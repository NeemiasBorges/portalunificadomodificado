﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class AutorizacoesGrupo
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SolCompra { get; set; }
        public string CriarSolCompra { get; set; }
        public string PedCompra { get; set; }
        public string CriarPedCompra { get; set; }
        public string ReqEstoque { get; set; }
        public string CriarReqEstoque { get; set; }
        public string Reembolso { get; set; }
        public string CriarReembolso { get; set; }
        public string CriarModeloEmail { get; set; }
        public string ModeloEmail { get; set; }
        public string AssistEnvio { get; set; }
        public string Clientes { get; set; }
        public string CriarGrpClientes { get; set; }
        public string GrpClientes { get; set; }
        public string RegistroEnvEmail { get; set; }
        public string RelatCobranças { get; set; }
    }
}