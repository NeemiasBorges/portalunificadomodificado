﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Estoque
{
    public class RequerimentoEstoque
    {
        [Display(Name = "Nº Interno")]
        public int Contagem { get; set; }

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }
        [Display(Name = "Nº do Documento")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Dt. Lançamento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataLancamento { get; set; }

        [Display(Name = "Requisitante")]
        public string Requisitante { get; set; }

        [Display(Name = "Itens")]
        public List<RequerimentoEstoqueLinhas> Itens { get; set; }

        [Display(Name = "Observação")]
        public string Observacao { get; set; }

    }
}