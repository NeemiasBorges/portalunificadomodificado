﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Estoque
{
    public class RequerimentoEstoqueLinhas
    {
        #region Lista de propriedades


        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Requisição")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Display(Name = "Dt. Requisição")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataRequisicao { get; set; }

        [Display(Name = "Nº Linha")]
        public int IdLinha { get; set; }

        [Display(Name = "Código")]
        public string CodigoItem { get; set; }

        [Display(Name = "Descrição")]
        public string DecricaoItem { get; set; }

        [Display(Name = "Quantidade")]
        public double Quantidade { get; set; }

        [Display(Name = "Status Linha")]
        public string StatusLinha { get; set; }

        [Display(Name = "Unidade de Medida")]
        public string UndMedida { get; set; }

        [Display(Name = "Observação")]
        public string Observacao { get; set; }

        [Display(Name = "Código Depósito")]
        public string CodigoDeposito { get; set; }

        [Display(Name = "Descrição Depósito")]
        public string DescricaoDeposito { get; set; }
        #endregion
    }
}