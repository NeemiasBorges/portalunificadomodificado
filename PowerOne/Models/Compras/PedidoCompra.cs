﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowerOne.Models.Compras
{
    public class PedidoCompra
    {
        [Display(Name = "Registros")]
        public int Contagem { get; set; }

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº do Documento")]
        public int DocNum { get; set; }

        [Display(Name = "Cod. Fornecedor")]
        public string CardCode { get; set; }

        [Display(Name = "Nome Fornecedor")]
        public string CardName { get; set; }

        [Display(Name = "Id Fornecedor")]
        public int BPL_IDAssignedToInvoice { get; set; }

        [Display(Name = "Nome Fornecedor")]
        public string BPLName { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Display(Name = "Data Documento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Data Entrega")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DocDueDate { get; set; }

        [Display(Name = "Anexos")]
        public int idAnexos { get; set; }

        [Display(Name = "Itens")]
        public List<PedidoCompraLinhas> Itens { get; set; }

        [Display(Name = "Lista de Anexos")]
        public List<Arquivo> ListaAnexos { get; set; }
    }
}