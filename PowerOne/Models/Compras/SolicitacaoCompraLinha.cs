﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Compras
{
    public class SolicitacaoCompraLinha
    {
        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Requisição")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Required(ErrorMessage = "{0} é obrigatória")]
        [Display(Name = "Data Necessária")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ReqDate { get; set; }

        [Display(Name = "Data Requerimento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ItemReqDate { get; set; }

        [Display(Name = "Nº Linha")]
        public int IdLinha { get; set; }

        public string LineStatus { get; set; }

        [Display(Name = "Código")]
        public string CodigoItem { get; set; }

        [Display(Name = "Descrição")]
        public string DecricaoItem { get; set; }

        [Display(Name = "Quantidade")]
        public double Quantidade { get; set; }

        [Display(Name = "Preço Unitário")]
        public double PriceBefDi { get; set; }

        [Display(Name = "Centro de Custo")]
        public string OcrCode { get; set; }
        [Display(Name = "Nome Ct. Custo")]
        public string OcrName { get; set; }

        [Display(Name = "Centro de Custo2")]
        public string OcrCode2 { get; set; }
        [Display(Name = "Nome Ct. Custo2")]
        public string OcrName2 { get; set; }

        [Display(Name = "Centro de Custo3")]
        public string OcrCode3 { get; set; }
        [Display(Name = "Nome Ct. Custo3")]
        public string OcrName3 { get; set; }

        [Display(Name = "Centro de Custo4")]
        public string OcrCode4 { get; set; }
        [Display(Name = "Nome Ct. Custo4")]
        public string OcrName4 { get; set; }

        [Display(Name = "Centro de Custo5")]
        public string OcrCode5 { get; set; }
        [Display(Name = "Nome Ct. Custo5")]
        public string OcrName5 { get; set; }


        [Display(Name = "Depósito")]
        public string Deposito { get; set; }

        [Display(Name = "UM")]
        public string UM { get; set; }

        [Display(Name = "Texto Livre")]
        public string TxtLivre { get; set; }
    }
}