﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowerOne.Models.Compras
{
    public class SolicitacaoCompra
    {
        [Display(Name = "Total doc")]
        public int Contagem { get; set; }

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº do Documento")]
        public int DocNum { get; set; }


        [Display(Name = "Status")]
        public string DocStatus { get; set; }


        [Display(Name = "Data Documento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Data de vencimento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DocDueDate { get; set; }

        [Display(Name = "Data Necessária")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ReqDate { get; set; }

        [Display(Name = "Nome solicitante ")]
        public string ReqName { get; set; }

        [Display(Name = "Itens")]
        public List<SolicitacaoCompraLinha> Itens { get; set; }

        [Display(Name = "Anexos")]
        public int idAnexos { get; set; }

        [Display(Name = "Comentario")]
        public string Comments { get; set; }


        [Display(Name = "Filial")]
        public int Filial { get; set; }

        [Display(Name = "Lista de Anexos")]
        public List<Arquivo> ListaAnexos { get; set; }
    }
}