﻿using System.ComponentModel.DataAnnotations;

namespace PowerOne.Models
{
    public class ListBP
    {
        [Display(Name = "Cod. PN")]
        public string CardCode { get; set; }

        [Display(Name = "Nome PN")]
        public string CardName { get; set; }
    }
}