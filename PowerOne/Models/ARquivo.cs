﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class Arquivo
    {
        public string Nome { get; set; }
        public string Extensao { get; set; }
        public string Diretorio { get; set; }
    }
}