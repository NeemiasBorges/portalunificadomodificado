﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class ConfiguracoesEmail
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        public string Email { get; set; }

        [Display(Name = "Senha")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        public string Senha { get; set; }
        
        [Display(Name = "Servidor SMTP")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        public string Servidor { get; set; }

        [Display(Name = "Porta")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        public int Porta { get; set; }

        [Display(Name = "Diretório de Notas Fiscais")]
        public string DirNotaFiscal { get; set; }

        [Display(Name = "Diretório de Boleto")]
        public string DirBoleto { get; set; }

        //[Display(Name = "Serviço")]
        //[Required(ErrorMessage = "{0} é obrigatório")]
        //public string Servico { get; set; }
    }
}