﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class MailModel
    {
        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Código")]
        public string Code { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Descrição")]
        public string Description { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Assunto")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Mensagem")]
        public string Message { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Tipo de Modelo")]
        public string TypeId { get; set; }

        public TypeModel Type { get; set; }

        [Display(Name ="Forma de Pagamento")]
        public string PayMethod { get; set; }
    }
}