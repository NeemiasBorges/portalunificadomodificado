﻿using PowerOne.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca.ViewModel
{
    public class SendingWizardViewModel
    {

        [Display(Name = "Código do PN")]
        public string CardCode { get; set; }
        [Display(Name = "Grupo do PN")]
        public string CodeGroup { get; set; }

        [Display(Name = "Tipo de Envio")]
        public string MailTypeCode { get; set; }

        public  TypeModel MailType { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data de Vencimento de")]
        public DateTime DueDateFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data de Vencimento até")]
        public DateTime DueDateTo { get; set; }


        
        [Display(Name = "Valor Total de")]
        public double TotalValueFrom { get; set; }
        
        [Display(Name = "Valor Total até")]
        public double TotalValueTo { get; set; }
        
        [Display(Name = "Dias Antes do Vencimento")]
        public int DaysBefore { get; set; }
        
        [Display(Name = "Dias Depois do Vencimento")]
        public int DaysAfter { get; set; }
    }
}