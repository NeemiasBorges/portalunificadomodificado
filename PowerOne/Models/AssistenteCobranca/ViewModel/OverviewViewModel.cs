﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca.ViewModel
{
    public class OverviewViewModel
    {
        public BPGroup Group { get; set; }
        //<formaDePagamento,MailModel>
        public Dictionary<string,MailModel> AlertaVencimento { get; set; }
        public Dictionary<string,MailModel> Cobranca { get; set; }
        public Dictionary<string,MailModel> PosFaturamento { get; set; }
    }
}