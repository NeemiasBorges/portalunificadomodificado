﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca.ViewModel
{
    public class PayMethodViewModel
    {
        public string BPGroupId { get; set; }
        public string TypeModelId { get; set; }

        public string GeralModelId { get; set; }
        public string BankTransferModelId { get; set; }
        public string BilletModelId { get; set; }

        public List<MailModel> GeralModels { get; set; }
        public List<MailModel> BankTransferModels { get; set; }
        public List<MailModel> BilletModels { get; set; }
    }
}