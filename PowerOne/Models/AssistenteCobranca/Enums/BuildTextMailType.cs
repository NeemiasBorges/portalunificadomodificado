﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca.Enums
{
    public enum BuildTextMailType
    {
        MessageBody,
        MessageSubject
    }
}