﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class BPGroup
    {
        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Código")]
        public string CodeGroup { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        [Display(Name = "Descrição")]
        public string DescGroup { get; set; }

        //[Required(ErrorMessage = "{0} é obrigatório.")]
        //[Display(Name = "Pós-Faturamento")]
        //public string DefaultModelId { get; set; }

        //[Required(ErrorMessage = "{0} é obrigatório.")]
        //[Display(Name = "Alerta de Vencimento")]
        //public string AlertModelId { get; set; }

        //[Required(ErrorMessage = "{0} é obrigatório.")]
        //[Display(Name = "Cobrança")]
        //public string BillingModelId { get; set; }
        //public MailModel DefaultModel { get; set; }

        //public MailModel AlertModel { get; set; }

        //public MailModel BillingModel { get; set; }
    }
}