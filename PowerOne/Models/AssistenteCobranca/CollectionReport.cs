﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class CollectionReport
    {
        [Display(Name = "Código do Título")]
        public string BillCode { get; set; }

        [Display(Name = "Data de Vencimento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DueDate { get; set; }

        [Display(Name = "Código do Parceiro")]
        public string CardCode { get; set; }

        [Display(Name = "Valor Total")]
        [DisplayFormat(DataFormatString = "R$ {0:F2}")]
        public double TotalValue { get; set; }

        [Display(Name = "Número de Envios")]
        public int NumberOfSubmitions { get; set; }
    }
}