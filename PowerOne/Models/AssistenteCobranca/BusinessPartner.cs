﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class BusinessPartner
    {

        [Display(Name = "Código do PN")]
        public string CardCode { get; set; }
        [Display(Name = "Razão Social")]
        public string CardName { get; set; }

        [Display(Name = "Código do Grupo")]
        public string CodeGroup { get; set; }
    }
}