﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class ContactEmployee
    {
        //T0.[FirstName], T0.[MiddleName], T0.[LastName], T0.[Position], T0.[E_MailL]

        [Display(Name = "Código do Contato")]
        public string Id { get; set; }

        [Display(Name = "Código do Parceiro de Negócios")]
        public string CardCode { get; set; }

        [Display(Name = "Nome")]
        public string FullName { get; set; }

        [Display(Name = "Posição")]
        public string Position { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Utilizar em Envio de Emails")]
        public string UseForSendingEmail { get; set; }

    }
}