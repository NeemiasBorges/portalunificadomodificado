﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class LogEmail
    {
        [Display(Name = "Código")]
        public string Code { get; set; }


        //Dados do Documento
        [Display(Name = "Código do Título")]
        public string BillCode { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data de Vencimento")]
        public DateTime DueDate { get; set; }

        [DisplayFormat(DataFormatString = "R$ {0:F2}")]
        [Display(Name = "Valor Total")]
        public double BillTotal { get; set; }

        [Display(Name = "Código do PN")]
        public string CardCode { get; set; }

        //Tipo de Email
        [Display(Name = "Tipo de Email")]
        public string TypeId { get; set; }
        public  TypeModel Type { get; set; }

        //Dados do Email
        [Display(Name = "Modelo de Email")]
        public string MailModelId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data de Envio")]
        public DateTime SendingDate { get; set; }

        [Display(Name = "Endereço de Email")]
        public string EmailAdress { get; set; }

        //Esse atributo irá armazenar os demais emails do PN
        [Display(Name = "Lista de Email de Contatos")]
        public  string[] EmailContactsCopyLists { get; set; }

        [Display(Name = "Assunto")]
        public string Subject { get; set; }
        [Display(Name = "Mensagem")]
        public string Message { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}