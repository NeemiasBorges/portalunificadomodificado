﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class BillMessage
    {
        [Display(Name = "Nº")]
        public string DocCode { get; set; }

        [Display(Name = "Nº de Série")]
        public string SerialNumber { get; set; }

        [Display(Name = "Modelo")]
        public string InvoiceModel { get; set; }

        [Display(Name = "Código do Cliente")]
        public string CardCode { get; set; }

        [Display(Name = "Razão Social")]
        public string CardName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data de Lançamento")]
        public DateTime DocDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data de Vencimento")]
        public DateTime DocDueDate { get; set; }

        [DisplayFormat(DataFormatString = "R$ {0:F2}")]
        [Display(Name = "Valor Total")]
        public double DocTotal { get; set; }

        [Display(Name = "Forma de Pagamento")]
        public string PayMethod { get; set; }

        public int DifferenceDueDateAndToday { get; set; }

        [Display(Name = "Endereço de Email")]
        public string EmailAdress { get; set; }
        //Esse atributo irá armazenar os demais emails do PN
        [Display(Name = "Lista de Email de Contatos")]
        public List<ContactEmployee> EmailContactsCopyLists { get; set; }

        public string GroupCode { get; set; }
        public string MailModelCode { get; set; }
        public MailModel MailModel { get; set; }


    }
}