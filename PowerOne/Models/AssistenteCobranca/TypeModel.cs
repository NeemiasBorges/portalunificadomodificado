﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.AssistenteCobranca
{
    public class TypeModel
    {
        [Display(Name = "Código")]
        public string Code { get; set; }
        [Display(Name = "Tipo de Modelo")]
        public string Name { get; set; }
    }
}