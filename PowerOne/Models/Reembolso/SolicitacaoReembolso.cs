﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Reembolso
{
    public class SolicitacaoReembolso
    {
        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Documento")]
        public int DocNum { get; set; }

        [Display(Name = "SuppCatNum")]
        public int SuppCatNum { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Display(Name = "TaxDate")]
        public string TaxDate { get; set; }

        [Display(Name = "Data Documento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Itens")]
        public List<SolicitacaoReembolsoLinhas> Itens { get; set; }

        [Display(Name = "Cód. Fornecedor")]
        public string CardCode { get; set; }

        [Display(Name = "Nome Fornecedor")]
        public string CardName { get; set; }

        [Display(Name = "VisOrder")]
        public int VisOrder { get; set; }

        [Display(Name = "Id Fornecedor")]
        public int BPL_IDAssignedToInvoice { get; set; }

        [Display(Name = "Data Necessária")]
        public float ItemCode { get; set; }

        [Display(Name = "Nome Item")]
        public string ItemName { get; set; }

        [Display(Name = "Requisitante")]
        public string Requisitante { get; set; }

        [Display(Name = "AgrNo")]
        public string AgrNo { get; set; }

        [Display(Name = "OcrCode")]
        public string OcrCode { get; set; }

        [Display(Name = "Quantidade")]
        public int Quantity { get; set; }

        [Display(Name = "LineStatus")]
        public string LineStatus { get; set; }

        [Display(Name = "Anexos")]
        public int idAnexos { get; set; }

        [Display(Name = "Filial")]
        public int Filial { get; set; }

        [Display(Name = "Comentario")]
        public string Comments { get; set; }

        [Display(Name = "TrgetEntry")]
        public int TrgetEntry { get; set; }

        [Display(Name = "Lista de Anexos")]
        public List<Arquivo> ListaAnexos { get; set; }
    }
}