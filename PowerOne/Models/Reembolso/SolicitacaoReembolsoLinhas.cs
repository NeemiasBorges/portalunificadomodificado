﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Reembolso
{
    public class SolicitacaoReembolsoLinhas
    {

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Requisição")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Display(Name = "Dt. Envio")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ShipDate { get; set; }

        [Display(Name = "Nº Linha")]
        public int VisOrder { get; set; }

        [Display(Name = "Código")]

        public string ItemCode { get; set; }

        [Display(Name = "Descrição")]
        public string ItemDesc { get; set; }

        [Display(Name = "Quantidade")]
        public double Quantity { get; set; }

        [Display(Name = "Projeto")]
        public string Projeto { get; set; }

        [Display(Name = "Preço Unitário")]
        public double PriceBefDi { get; set; }

        [Display(Name = "Status Linha")]
        public string LineStatus { get; set; }
        [Display(Name = "Centro de Custo")]
        public string OcrCode { get; set; }

        [Display(Name = "Nome Ct. Custo")]
        public string OcrName { get; set; }

        [Display(Name = "Centro de Custo2")]
        public string OcrCode2 { get; set; }
        [Display(Name = "Nome Ct. Custo2")]
        public string OcrName2 { get; set; }

        [Display(Name = "Centro de Custo3")]
        public string OcrCode3 { get; set; }
        [Display(Name = "Nome Ct. Custo3")]
        public string OcrName3 { get; set; }

        [Display(Name = "Centro de Custo4")]
        public string OcrCode4 { get; set; }
        [Display(Name = "Nome Ct. Custo4")]
        public string OcrName4 { get; set; }

        [Display(Name = "Centro de Custo5")]
        public string OcrCode5 { get; set; }
        [Display(Name = "Nome Ct. Custo5")]
        public string OcrName5 { get; set; }

        [Display(Name = "Codigo Whs")]
        public string WhsCode { get; set; }

        [Display(Name = "Unidade de compra")]
        public string BuyUnitMsr { get; set; }

        [Display(Name = "Target Entry")]
        public int EntradaCod { get; set; }

        [Display(Name = "Target Entry")]
        public string AgrNo { get; set; }
        [Display(Name = "Target Entry")]
        public string ItemName { get; set; }
        [Display(Name = "Target Entry")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Contrato")]
        public string Contrato { get; set; }

        [Display(Name = "FreeTxt")]
        public string FreeTxt { get; set; }
    }
}


