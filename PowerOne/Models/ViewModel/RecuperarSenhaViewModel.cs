﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.ViewModel
{
    public class RecuperarSenhaViewModel
    {
        string Id;

        [StringLength(60, MinimumLength = 3, ErrorMessage = "O tamanho do {0} deve estar entre {2} e {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Primeiro Nome")]
        public string FirstName { get; set; }

        [StringLength(60, MinimumLength = 3, ErrorMessage = "O tamanho do {0} deve estar entre {2} e {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Último Nome")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}