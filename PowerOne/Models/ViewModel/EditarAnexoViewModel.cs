﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.ViewModel
{
    public class EditarAnexoViewModel
    {
        public HttpPostedFileBase File { get; set; }
        public int DocEntry { get; set; }
    }
}