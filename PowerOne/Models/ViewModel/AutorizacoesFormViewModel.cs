﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.ViewModel
{
    public class AutorizacoesFormViewModel
    {
        public string sValor { get; set; }
        public string sDescricao { get; set; }
    }
}