﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PowerOne.Models.ViewModel
{
    public class AlterarSenhaViewModel
    {
        public int Id { get; set; }

        [StringLength(60, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Usuário ")]
        public string User { get; set; }

        [StringLength(60, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Senha Atual")]
        public string CurrentPassword { get; set; }

        [StringLength(60, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Senha Nova")]
        public string NewPassword { get; set; }
    }
}