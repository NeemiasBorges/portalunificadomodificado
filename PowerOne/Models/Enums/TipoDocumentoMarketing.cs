﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models.Enums
{
    public enum TipoDocumento
    {
        SolicitacaoCompra,
        PedidoCompra,
        Reembolso,
        RequerimentoEstoque
    }
}