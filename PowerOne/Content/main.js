﻿function toggleTableForm(element) {
	//verificar se os formularios estão escondidos
	var _tr = jQuery(element).parent().parent();

	var formVisivel = _tr.find("td:eq( 1 )").find(".input-table-form").first().hasClass('visible-false');
	if (formVisivel) {
		_tr.find("td").each(function () {
			jQuery(this).find('.input-table-form').each(function () {
				jQuery(this).removeClass('visible-false');
			});
			jQuery(this).find('.td-table-form').each(function () {
				jQuery(this).addClass('visible-false');
			});
		});

	} else {
		_tr.find("td").each(function () {
			jQuery(this).find('.input-table-form').each(function () {
				jQuery(this).addClass('visible-false');
			});
			jQuery(this).find('.td-table-form').each(function () {
				jQuery(this).removeClass('visible-false');
			});
		});
    }
}

function adicionarLinha() {
	var form = $('.formLinha');
	if (form.hasClass('form-hide')) {
		form.removeClass('form-hide');
	} else {
		form.addClass('form-hide');
    }
}


function escreverMensagemEmInput(id_span, msg) {
	$(id_span).text(msg);
	setTimeout(function () {
		$(id_span).text('');
	}, 4000);

}

function excluirLinha(_index) {
	linhas.splice(_index, 1);
	$('tbody tr').remove();
	if (linhas.length <= 0) {
		$('#botao').prop('disabled', true);
		$("#filial").prop('disabled', false);
		$("#bp").prop('disabled', false);
	}
	listar();
}

function isAnyEmpty(el) {
	var isEmpty = false, v;
	el.each(function (e) {
		v = $(this).val();
		if (v == "" || v == null || v == undefined) {
			isEmpty = true
		}
	});
	return isEmpty;
}

function escreverMensagemEmHead(msg) {

	$('#alert').text(msg);
	$('#alert').removeClass('alert-hide');
	$('#alert').addClass('alert-danger');
	window.scroll(0, 0);
}


function dataFormatada(dataInformada) {
	return (new Date(dataInformada + 'T00:00')).toLocaleDateString('pt-BR') ;
}

function mascara(o, f) {
	v_obj = o
	v_fun = f
	setTimeout("execmascara() ", 1)
}

function execmascara() {
	v_obj.value = v_fun(v_obj.value)
}

function moeda(v) {
	v = v.replace(/\D/g, "") // permite digitar apenas numero
	v = v.replace(/(\d{1})(\d{17})$/, "$1.$2") // coloca ponto antes dos ultimos digitos
	v = v.replace(/(\d{1})(\d{13})$/, "$1.$2") // coloca ponto antes dos ultimos 13 digitos
	v = v.replace(/(\d{1})(\d{10})$/, "$1.$2") // coloca ponto antes dos ultimos 10 digitos
	v = v.replace(/(\d{1})(\d{7})$/, "$1.$2") // coloca ponto antes dos ultimos 7 digitos
	v = v.replace(/(\d{1})(\d{1,2})$/, "$1,$2") // coloca virgula antes dos ultimos 2 digitos
	return v;
}



function soNumeros(v) {
	return v.replace(/\D/g, "")
}

function isNumber(n) {
	return $.isNumeric(n);
}

function SomenteNumero(e, numeros) {
	
	var v = numeros.value;
	var m = v.match(/,/g);
	if (m && m.length >= 1) {
		if (e.keyCode == "44") {
			return false
		}
	}
	var tecla = (window.event) ? event.keyCode : e.which;
	if ((tecla > 47 && tecla < 58)) return true;
	else {
		if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 13) return true;
		else return false;
	}
}