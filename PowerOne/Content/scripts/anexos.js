var anexos = []

function addAnexoForm(){
    $('#form_item').hide();
    $('#nav-anexos-tab').tab('show');
    $('#form_anexo').show();
}

function cancelarAnexo(){
    $('#form_item').show();
    $('#nav-itens-tab').tab('show');
    $('#form_anexo').hide();
}

function uploadAnexo(){
    var formData = new FormData();
    formData.append('file',$('#input_anexo')[0].files[0]);

    $.ajax({
        url: '/UploadArquivo/Upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success:function(result){
            addLinhasAnexos(result);
        },
        error:function(result){
            console.log('error:', result);
        }
    });
}

function excluirAnexo(index){
    anexos.splice(index,1);
    listarAnexos();
}

function addLinhasAnexos(anexo){
    anexos.push(anexo);
    listarAnexos();
}

function listarAnexos(){
    $('tbody#anexos tr').remove();
    anexos.forEach(function(linha){
        var row = "";                 
        row += "<td>" + linha.Nome + "</td>";
        row += "<td>" + linha.Extensao + "</td>";
        row += "<td> <button class='btn btn-outline-danger material-icons' onclick='excluirAnexo(linhas.length - 1)'>delete</button></td>";

        $('tbody#anexos').append("<tr class='tr'>" + row + "<tr>");
    });
}