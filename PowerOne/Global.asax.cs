using PowerOne.Database;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PowerOne
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Conexao.Connect();

            /**************************************
             * Alterações no Banco de Dados: UDF & UDT
             **************************************/
            CreateTablesAndFields.Main();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
    }
}
