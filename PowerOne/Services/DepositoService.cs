﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PowerOne.Database;
using PowerOne.Models;
using System.Text;


namespace PowerOne.Services
{
    public class DepositoService
    {
        #region List
        public List<Deposito> List(int filial)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Deposito> list = new List<Deposito>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"WhsCode\", \"WhsName\" FROM OWHS WHERE \"BPLid\" =" + filial + "");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Deposito item = new Deposito();
                item.WhsCode = oRecordset.Fields.Item("WhsCode").Value.ToString();
                item.WhsName = oRecordset.Fields.Item("WhsName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}