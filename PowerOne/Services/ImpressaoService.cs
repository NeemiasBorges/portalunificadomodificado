﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using PowerOne.Database;
using PowerOne.Models.Enums;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace PowerOne.Services
{
    public class ImpressaoService
    {
        #region Imprimir Requerimento de Estoque
        public string Imprimir(int DocEntry, string path, TipoDocumento tipoDocumento)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string outFile = null;
            try
            {
                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                CrystalDecisions.CrystalReports.Engine.ReportDocument obj = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

                string m_rptname;
                //ReqSaida(2).rpt
                m_rptname = path;
                obj.Load(m_rptname);
                obj.SetParameterValue("Dockey@", DocEntry);


                crConnectionInfo.ServerName = oCompany.Server;
                crConnectionInfo.DatabaseName = oCompany.CompanyDB;
                crConnectionInfo.UserID = oCompany.DbUserName;
                crConnectionInfo.Password = Constantes.SenhaSQL;

                CrTables = obj.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }

                string document = null;
                if (tipoDocumento == TipoDocumento.RequerimentoEstoque)
                {
                    document = "REQUERIMENTO_ESTOQUE";
                }
                else if (tipoDocumento == TipoDocumento.SolicitacaoCompra)
                {
                    document = "SOLICITACAO_COMPRA";
                }
                else if (tipoDocumento == TipoDocumento.PedidoCompra)
                {
                    document = "PEDIDO_COMPRA";
                }
                else if (tipoDocumento == TipoDocumento.Reembolso)
                {
                    document = "REEMBOLSO";
                }

                outFile = HostingEnvironment.MapPath($"~/Files/{document}_" + Convert.ToString(DocEntry) + ".pdf");

                obj.ExportToDisk(ExportFormatType.PortableDocFormat, outFile);
            }
            catch (Exception e)
            {

            }

            return outFile;
        }
        #endregion
    }
}