﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.Compras;
using PowerOne.Models.Compras.Enums;
using SAPbobsCOM;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Globalization;
using System.Text;
using PowerOne.Util;


namespace PowerOne.Services.Compras
{
    public class PedidoCompraService
    {
        #region Serviços
        AnexosService _anexoService = new AnexosService();
        #endregion

        #region contagem semanais
        public int contarRegistrosPedido()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(-7);

            PedidoCompra pedido = new PedidoCompra();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM OPOR " +
                "WHERE \"DocDate\" BETWEEN '"
                + fim.ToString("yyyyMMdd") +
                "' AND '"
                + inicio.ToString("yyyyMMdd") +
                "'");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                pedido.Contagem = int.Parse(oRecordSet.Fields.Item("Count").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return pedido.Contagem;
        }
        #endregion

        #region Vencimentos
        public int vencimentos()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(7);

            PedidoCompra pedido = new PedidoCompra();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM OPOR " +
                "WHERE \"DocDueDate\" BETWEEN '"
                + inicio.ToString("yyyyMMdd") +
                "' AND '"
                + fim.ToString("yyyyMMdd") +
                "'");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                pedido.Contagem = int.Parse(oRecordSet.Fields.Item("Count").Value.ToString());

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return pedido.Contagem;
        }

        #endregion

        #region Listar e pesquisar pedidos
        public List<PedidoCompra> SearchListPedidos(Login login, string DataInicio, string DataFim, string Status)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<PedidoCompra> listPedidoCompras = new List<PedidoCompra>();

            StringBuilder SQL = new StringBuilder();


            if ((login.Autorizacoes.PedCompra == "T") || (login.Autorizacoes.Grupo.PedCompra == "T"))
            {
                SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", T0.\"CardName\", ");
                SQL.AppendLine("CASE WHEN T0.\"DocStatus\" = 'O' THEN 'Aberta' WHEN T0.\"CANCELED\" = 'Y' THEN 'Cancelada' else 'Fechada' END \"DocStatus\", ");
                SQL.AppendLine("T0.\"BPLId\", T0.\"DocDate\", T0.\"DocDueDate\", T0.\"AtcEntry\"  ");
                SQL.AppendLine("FROM OPOR T0 ");
                SQL.AppendLine("WHERE 1 = 1 AND (\"U_P1_TYPE\" = 'P') ");
            }
            else
            {
                SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", T0.\"CardName\", ");
                SQL.AppendLine("CASE WHEN T0.\"DocStatus\" = 'O' THEN 'Aberta' WHEN T0.\"CANCELED\" = 'Y' THEN 'Cancelada' else 'Fechada' END \"DocStatus\", ");
                SQL.AppendLine("T0.\"BPLId\", T0.\"DocDate\", T0.\"DocDueDate\", T0.\"AtcEntry\"  ");
                SQL.AppendLine("FROM OPOR T0 ");
                SQL.AppendLine("WHERE 1 = 1 AND (\"U_P1_TYPE\" = 'P') ");

                SQL.AppendLine("AND (T0.\"U_P1_OWNER\" = '" + login.Id + "') ");
            }

            //pesquisas

            if (!String.IsNullOrEmpty(DataInicio))
            {
                SQL.AppendLine("AND T0.\"DocDate\" >= '" + (DateTime.ParseExact(DataInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");
            }

            if (!String.IsNullOrEmpty(DataFim))
            {
                SQL.AppendLine("AND T0.\"DocDate\" <='" + (DateTime.ParseExact(DataFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");
            }

            if (Status != "T")
            {
                SQL.AppendLine(" AND T0.\"DocStatus\" = '" + Status + "' ");
            }

            // ordena apartir do último documento
            SQL.AppendLine("ORDER BY T0.\"DocEntry\" DESC ");


            oRecordset.DoQuery(SQL.ToString());

            PedidoCompra pedido = new PedidoCompra();

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {

                listPedidoCompras.Add(new PedidoCompra()
                {
                    DocEntry = int.Parse(oRecordset.Fields.Item("DocEntry").Value.ToString()),
                    DocNum = int.Parse(oRecordset.Fields.Item("DocNum").Value.ToString()),
                    DocDueDate = DateTime.Parse(oRecordset.Fields.Item("DocDueDate").Value.ToString()),
                    DocDate = DateTime.Parse(oRecordset.Fields.Item("DocDate").Value.ToString()),
                    DocStatus = oRecordset.Fields.Item("DocStatus").Value.ToString(),
                    BPL_IDAssignedToInvoice = int.Parse(oRecordset.Fields.Item("BPLId").Value.ToString()),
                    idAnexos = int.Parse(oRecordset.Fields.Item("AtcEntry").Value.ToString())
                });

                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listPedidoCompras;
        }
        #endregion

        #region listar linhas
        public List<PedidoCompraLinhas> ListarLinhasPedido(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<PedidoCompraLinhas> pedidosLinhas = new List<PedidoCompraLinhas>();


            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", ");
            SQL.AppendLine("case when T0.\"DocStatus\" = 'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\",");
            SQL.AppendLine("T1.\"ShipDate\", T1.\"VisOrder\",T1.\"ItemCode\",T2.\"ItemName\",T1.\"Quantity\", T1.\"PriceBefDi\", T1.\"U_CONFPEDIDO\", T1.\"U_ObsConf\", ");
            SQL.AppendLine("case when T1.\"LineStatus\" = 'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"LineStatus\",");
            SQL.AppendLine("T1.\"OcrCode\", T1.\"OcrCode2\", T1.\"OcrCode3\", T1.\"OcrCode4\", T1.\"OcrCode5\",");
            SQL.AppendLine("T1.\"WhsCode\",T2.\"BuyUnitMsr\",coalesce(T1.\"TrgetEntry\",0),");
            SQL.AppendLine("C1.\"OcrName\", C2.\"OcrName\" \"OcrName2\", C3.\"OcrName\" \"OcrName3\", C4.\"OcrName\" \"OcrName4\", C5.\"OcrName\" \"OcrName5\"");
            SQL.AppendLine("FROM OPOR T0 inner Join POR1 T1 on T0.\"DocEntry\" = T1.\"DocEntry\"");
            SQL.AppendLine("inner Join oitm T2 ON T2.\"ItemCode\" = T1.\"ItemCode\" ");
            SQL.AppendLine("left join OOCR C1 on T1.\"OcrCode\" = C1.OcrCode ");
            SQL.AppendLine("left join OOCR C2 on T1.\"OcrCode2\" = C2.OcrCode ");
            SQL.AppendLine("left join OOCR C3 on T1.\"OcrCode3\" = C3.OcrCode ");
            SQL.AppendLine("left join OOCR C4 on T1.\"OcrCode4\" = C4.OcrCode ");
            SQL.AppendLine("left join OOCR C5 on T1.\"OcrCode5\" = C5.OcrCode ");
            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "'");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();

            while (!oRecordSet.EoF)
            {
                pedidosLinhas.Add(new PedidoCompraLinhas()
                {
                    DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString()),
                    DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString()),
                    DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString(),
                    ShipDate = DateTime.Parse(oRecordSet.Fields.Item("ShipDate").Value.ToString()),
                    VisOrder = int.Parse(oRecordSet.Fields.Item("VisOrder").Value.ToString()),
                    ItemCode = oRecordSet.Fields.Item("ItemCode").Value.ToString(),
                    ItemDesc = oRecordSet.Fields.Item("ItemName").Value.ToString(),
                    Quantity = double.Parse(oRecordSet.Fields.Item("Quantity").Value.ToString()),
                    U_CONFPEDIDO = double.Parse(oRecordSet.Fields.Item("U_CONFPEDIDO").Value.ToString()),
                    U_ObsConf = oRecordSet.Fields.Item("U_ObsConf").Value.ToString(),

                    PriceBefDi = double.Parse(oRecordSet.Fields.Item("PriceBefDi").Value.ToString()),

                    OcrCode = oRecordSet.Fields.Item("OcrCode").Value.ToString(),
                    OcrName = oRecordSet.Fields.Item("OcrName").Value.ToString(),
                    OcrCode2 = oRecordSet.Fields.Item("OcrCode2").Value.ToString(),
                    OcrName2 = oRecordSet.Fields.Item("OcrName2").Value.ToString(),
                    OcrCode3 = oRecordSet.Fields.Item("OcrCode3").Value.ToString(),
                    OcrName3 = oRecordSet.Fields.Item("OcrName3").Value.ToString(),
                    OcrCode4 = oRecordSet.Fields.Item("OcrCode4").Value.ToString(),
                    OcrName4 = oRecordSet.Fields.Item("OcrName4").Value.ToString(),
                    OcrCode5 = oRecordSet.Fields.Item("OcrCode5").Value.ToString(),
                    OcrName5 = oRecordSet.Fields.Item("OcrName5").Value.ToString(),

                    LineStatus = oRecordSet.Fields.Item("LineStatus").Value.ToString(),
                    WhsCode = oRecordSet.Fields.Item("WhsCode").Value.ToString(),
                    BuyUnitMsr = oRecordSet.Fields.Item("BuyUnitMsr").Value.ToString()
                    /* EntradaCod = int.Parse(oRecordSet.Fields.Item("TrgetEntry").Value.ToString())*/

                });

                oRecordSet.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return pedidosLinhas;
        }
        #endregion

        #region GetByKey
        public PedidoCompra GetByKey(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            PedidoCompra pedido = new PedidoCompra();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"DocEntry\", \"DocNum\", \"DocDate\", \"DocDueDate\", \"BPLName\",");
            SQL.AppendLine("case when \"DocStatus\" = 'O' then 'Aberta' when \"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", \"AtcEntry\" ");
            SQL.AppendLine("FROM OPOR WHERE \"DocEntry\" = " + DocEntry + "");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                pedido.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                pedido.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                pedido.DocDate = DateTime.Parse(oRecordSet.Fields.Item("DocDate").Value.ToString());
                pedido.DocDueDate = DateTime.Parse(oRecordSet.Fields.Item("DocDueDate").Value.ToString());
                pedido.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
                pedido.BPLName = oRecordSet.Fields.Item("BPLName").Value.ToString();
                pedido.idAnexos = int.Parse(oRecordSet.Fields.Item("AtcEntry").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            pedido.Itens = ListarLinhasPedido(DocEntry);

            return pedido;
        }
        #endregion

        #region Fechar Status Linha Pedido
        public string FecharStatusLinha(int DocEntry, int LineId)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                if (oPedidoCompra.GetByKey(DocEntry))
                {
                    oPedidoCompra.Lines.SetCurrentLine(LineId);

                    oPedidoCompra.Lines.LineStatus = SAPbobsCOM.BoStatus.bost_Close;

                    if (oPedidoCompra.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oPedidoCompra);
                oPedidoCompra = null;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Conferimento Linha
        public string AprovarLinha(double U_CONFPEDIDO, string U_ObsConf, int DocEntry, int VisOrder, Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            SAPbobsCOM.Documents oPurchaseOrders = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

            try
            {
                if (oPurchaseOrders.GetByKey(DocEntry))
                {
                    oPurchaseOrders.Lines.SetCurrentLine(VisOrder);

                    oPurchaseOrders.Lines.UserFields.Fields.Item("U_P1_CONFPEDIDO").Value = U_CONFPEDIDO;
                    oPurchaseOrders.Lines.UserFields.Fields.Item("U_P1_ObsConf").Value = U_ObsConf;
                    oPurchaseOrders.Lines.UserFields.Fields.Item("U_P1_Conferente").Value = login.User.ToString();
                    oPurchaseOrders.Lines.UserFields.Fields.Item("U_P1_CONFREC").Value = "Y";
                    oPurchaseOrders.Lines.UserFields.Fields.Item("U_P1_DataConf").Value = DateTime.Now;

                    if (oPurchaseOrders.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oPurchaseOrders);
                    oPurchaseOrders = null;
                }
                else
                {
                    msg = "Documento inexistente!";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Create
        public string Create(PedidoCompra pedido, Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                oPedidoCompra.CardCode = pedido.CardCode;
                oPedidoCompra.CardName = pedido.CardName;
                oPedidoCompra.BPL_IDAssignedToInvoice = pedido.BPL_IDAssignedToInvoice;
                oPedidoCompra.DocDate = DateTime.Now;
                oPedidoCompra.DocDueDate = pedido.DocDueDate;
                oPedidoCompra.TaxDate = DateTime.Now;

                oPedidoCompra.UserFields.Fields.Item("U_P1_OWNER").Value = login.Id;
                //oPedidoCompra.UserFields.Fields.Item("U_TYPE").Value = "P";

                foreach (var item in pedido.Itens)
                {

                    oPedidoCompra.Lines.ItemCode = item.ItemCode;
                    oPedidoCompra.Lines.ItemDescription = item.ItemDesc;
                    oPedidoCompra.Lines.ShipDate = item.ShipDate;
                    oPedidoCompra.Lines.Quantity = item.Quantity;
                    oPedidoCompra.Lines.UnitPrice = item.PriceBefDi;

                    oPedidoCompra.Lines.CostingCode = item.OcrCode;
                    oPedidoCompra.Lines.CostingCode2 = item.OcrCode2;
                    oPedidoCompra.Lines.CostingCode3 = item.OcrCode3;
                    oPedidoCompra.Lines.CostingCode4 = item.OcrCode4;
                    oPedidoCompra.Lines.CostingCode5 = item.OcrCode5;

                    oPedidoCompra.Lines.WarehouseCode = item.WhsCode;
                    oPedidoCompra.Lines.MeasureUnit = item.BuyUnitMsr;
                    oPedidoCompra.Lines.Add();
                }

                int iRetCode = oPedidoCompra.Add();

                if (iRetCode != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
                else
                {
                    int iDocEntry = int.Parse(oCompany.GetNewObjectKey());
                    msg = _anexoService.InserirAnexos(pedido.ListaAnexos, iDocEntry, Models.Enums.TipoDocumento.PedidoCompra);
                }


            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Fechar Status do Pedido
        public string FecharStatus(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                if (oPedidoCompra.GetByKey(DocEntry))
                {
                    int iRet = oPedidoCompra.Close();

                    if (iRet != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                        msg += " O Status Já está Fechado";
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Editar Linha
        public string AlterarLinha(PedidoCompraLinhas pedidoCompraLinhas, AddLinha type)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                if (oPedidoCompra.GetByKey(pedidoCompraLinhas.DocEntry))
                {
                    if (type == AddLinha.Add)
                    {
                        oPedidoCompra.Lines.Add();
                        oPedidoCompra.Lines.ItemCode = pedidoCompraLinhas.ItemCode;
                    }
                    else
                    {
                        oPedidoCompra.Lines.SetCurrentLine(pedidoCompraLinhas.VisOrder);
                    }

                    oPedidoCompra.Lines.ItemDescription = pedidoCompraLinhas.ItemDesc;
                    oPedidoCompra.Lines.ShipDate = pedidoCompraLinhas.ShipDate;
                    oPedidoCompra.Lines.Quantity = pedidoCompraLinhas.Quantity;

                    //oPedidoCompra.UserFields.Fields.Item("U_CONFPEDIDO").Value = pedidoCompraLinhas.U_P1_CONFPEDIDO;

                    oPedidoCompra.Lines.UnitPrice = pedidoCompraLinhas.PriceBefDi;
                    oPedidoCompra.Lines.CostingCode = pedidoCompraLinhas.OcrCode;
                    oPedidoCompra.Lines.CostingCode2 = pedidoCompraLinhas.OcrCode2;
                    oPedidoCompra.Lines.CostingCode3 = pedidoCompraLinhas.OcrCode3;
                    oPedidoCompra.Lines.CostingCode4 = pedidoCompraLinhas.OcrCode4;
                    oPedidoCompra.Lines.CostingCode5 = pedidoCompraLinhas.OcrCode5;
                    oPedidoCompra.Lines.WarehouseCode = pedidoCompraLinhas.WhsCode;
                    oPedidoCompra.Lines.MeasureUnit = pedidoCompraLinhas.BuyUnitMsr;

                    if (oPedidoCompra.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                        Log.Gravar(msg, Log.TipoLog.Erro);
                    }

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oPedidoCompra);
                    oPedidoCompra = null;
                }
                else
                {
                    msg = "Documento inexistente!";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion
    }
}
