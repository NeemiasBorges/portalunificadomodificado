﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.Compras;
using PowerOne.Models.Compras.Enums;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PowerOne.Services.Compras
{
    public class SolicitacaoCompraService
    {

        #region Serviços
        AnexosService _anexoService = new AnexosService();
        #endregion

        #region contagem semanais

        public int contarRegistrosSolicita()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(-7);

            PedidoCompra pedido = new PedidoCompra();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM OPRQ " +
                "WHERE \"DocDate\" BETWEEN '"
                + fim.ToString("yyyyMMdd") +
                "' AND '"
                + inicio.ToString("yyyyMMdd") +
                "'");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                pedido.Contagem = int.Parse(oRecordSet.Fields.Item("Count").Value.ToString());

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return pedido.Contagem;
        }

        public int vencimentos()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(7);

            PedidoCompra pedido = new PedidoCompra();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM OPRQ " +
                "WHERE \"DocDueDate\" BETWEEN '"
                + inicio.ToString("yyyyMMdd") +
                "' AND '"
                + fim.ToString("yyyyMMdd") +
                "'");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                pedido.Contagem = int.Parse(oRecordSet.Fields.Item("Count").Value.ToString());

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return pedido.Contagem;
        }

        #endregion

        #region Listar e Listar Com parametros
        public List<SolicitacaoCompra> SearchList(Login login, string DataInicio, string DataFim, string Status)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<SolicitacaoCompra> listSolicitacaoCompras = new List<SolicitacaoCompra>();

            StringBuilder SQL = new StringBuilder();

            if ((login.Autorizacoes.SolCompra == "T") || (login.Autorizacoes.Grupo.SolCompra == "T")) 
            {
                if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta'");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'C' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", T0.\"BPLId\",");
                    SQL.AppendLine(" T0.\"ReqDate\", T0.\"AtcEntry\"");
                    SQL.AppendLine("FROM OPRQ T0 LIMIT 50 ");
                    SQL.AppendLine(" WHERE 1 = 1 AND ");
                }
                else
                {
                    SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' ");
                    SQL.AppendLine(" when T0.\"CANCELED\" = 'C' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", T0.\"BPLId\", ");
                    SQL.AppendLine(" T0.\"ReqDate\", T0.\"AtcEntry\" ");
                    SQL.AppendLine(" FROM OPRQ T0 ");
                    SQL.AppendLine(" WHERE 1 = 1 AND ");
                }
            }
            else
            {
                if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta'");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'C' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", T0.\"BPLId\",");
                    SQL.AppendLine(" T0.\"ReqDate\", T0.\"AtcEntry\"");
                    SQL.AppendLine("FROM OPRQ T0 LIMIT 50");
                }
                else
                {
                    SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' ");
                    SQL.AppendLine(" when T0.\"CANCELED\" = 'C' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", T0.\"BPLId\", ");
                    SQL.AppendLine(" T0.\"ReqDate\", T0.\"AtcEntry\" ");
                    SQL.AppendLine(" FROM OPRQ T0 ");
                }

                //autorização restrita

                SQL.AppendLine("WHERE (T0.\"OwnerCode\" = '" + login.Id + "'");
                SQL.AppendLine(" or T0.\"UserSign\" = '" + login.UserId + "' or T0.\"ReqName\" = '" + login.User + "' ) AND ");
            }


            //pesquisas
            if (!String.IsNullOrEmpty(DataInicio))
            {
                SQL.AppendLine(" T0.\"DocDate\" >= '" + (DateTime.ParseExact(DataInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' AND ");
            }

            if (!String.IsNullOrEmpty(DataFim))
            {
                SQL.AppendLine("T0.\"DocDate\" <='" + (DateTime.ParseExact(DataFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") +"' AND ");
            }

            if (Status != "T")
            {
                SQL.AppendLine("T0.\"DocStatus\" = '" + Status + "' ");
            }

            string teste = (SQL.ToString()).Substring(SQL.Length - 6);
            if ((SQL.ToString()).Substring(SQL.Length - 6) == "AND \r\n")
            {
                StringBuilder aux = new StringBuilder(SQL.ToString().Substring(0, SQL.Length - 6));
                SQL = aux;
            }

            // ordena apartir do último documento
            SQL.AppendLine("ORDER BY T0.\"DocEntry\" DESC ");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                listSolicitacaoCompras.Add(new SolicitacaoCompra()
                {
                    DocEntry = int.Parse(oRecordset.Fields.Item("DocEntry").Value.ToString()),
                    DocNum = int.Parse(oRecordset.Fields.Item("DocNum").Value.ToString()),
                    ReqDate = DateTime.Parse(oRecordset.Fields.Item("ReqDate").Value.ToString()),
                    DocDate = DateTime.Parse(oRecordset.Fields.Item("DocDate").Value.ToString()),
                    DocStatus = oRecordset.Fields.Item("DocStatus").Value.ToString(),
                    idAnexos = int.Parse(oRecordset.Fields.Item("AtcEntry").Value.ToString()),
                    Filial = int.Parse(oRecordset.Fields.Item("BPLId").Value.ToString())
                });
                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listSolicitacaoCompras;
        }
        #endregion

        #region Pegar Solicitacao Específico
        public SolicitacaoCompra GetByKey(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            SolicitacaoCompra solicitacaoCompra = new SolicitacaoCompra();

            StringBuilder SQL = new StringBuilder();


            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", ");
            SQL.AppendLine("case when T0.\"DocStatus\" = 'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\",");
            SQL.AppendLine("T0.\"ReqDate\",T0.\"DocDate\",T0.\"ReqName\", T0.\"Comments\",T0.\"DocDueDate\", \"AtcEntry\" ");
            SQL.AppendLine("FROM OPRQ T0");

            SQL.AppendLine("WHERE T0.\"DocEntry\" = " + DocEntry);

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                solicitacaoCompra.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                solicitacaoCompra.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                solicitacaoCompra.DocDate = DateTime.Parse(oRecordSet.Fields.Item("DocDate").Value.ToString());
                solicitacaoCompra.ReqDate = DateTime.Parse(oRecordSet.Fields.Item("ReqDate").Value.ToString());
                solicitacaoCompra.ReqName = oRecordSet.Fields.Item("ReqName").Value.ToString();
                solicitacaoCompra.Comments = oRecordSet.Fields.Item("Comments").Value.ToString();
                solicitacaoCompra.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
                solicitacaoCompra.idAnexos = int.Parse(oRecordSet.Fields.Item("AtcEntry").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            solicitacaoCompra.Itens = ListarLinhas(DocEntry, null);

            return solicitacaoCompra;
        }
        #endregion

        #region Listar Linhas da solicitação
        public List<SolicitacaoCompraLinha> ListarLinhas(int DocEntry, string[] linhas)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<SolicitacaoCompraLinha> solicitacoesLinhas = new List<SolicitacaoCompraLinha>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", ");
            SQL.AppendLine("case when T0.\"DocStatus\" = 'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\",");
            SQL.AppendLine("T1.\"PQTReqDate\", T1.\"VisOrder\",T1.\"ItemCode\",T2.\"ItemName\", T1.\"LineStatus\", T1.\"PriceBefDi\",");
            SQL.AppendLine("case when T1.\"LineStatus\" = 'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"LineStatus\",");
            SQL.AppendLine("T1.\"Quantity\",T1.\"OcrCode4\", T1.\"WhsCode\",T2.\"BuyUnitMsr\", coalesce(T1.\"TrgetEntry\",0) as \"TrgetEntry\", t1.\"FreeTxt\", ");
            SQL.AppendLine("T1.\"OcrCode\", T1.\"OcrCode2\", T1.\"OcrCode3\", T1.\"OcrCode4\", T1.\"OcrCode5\",");
            SQL.AppendLine("C1.\"OcrName\", C2.\"OcrName\" \"OcrName2\", C3.\"OcrName\" \"OcrName3\", C4.\"OcrName\" \"OcrName4\", C5.\"OcrName\" \"OcrName5\"");
            SQL.AppendLine("FROM OPRQ T0 inner Join PRQ1 T1 on T0.\"DocEntry\" = T1.\"DocEntry\" ");
            SQL.AppendLine("inner Join oitm T2 on T2.\"ItemCode\" = T1.\"ItemCode\" ");
            SQL.AppendLine("left join OOCR C1 on T1.\"OcrCode\" = C1.OcrCode ");
            SQL.AppendLine("left join OOCR C2 on T1.\"OcrCode2\" = C2.OcrCode ");
            SQL.AppendLine("left join OOCR C3 on T1.\"OcrCode3\" = C3.OcrCode ");
            SQL.AppendLine("left join OOCR C4 on T1.\"OcrCode4\" = C4.OcrCode ");
            SQL.AppendLine("left join OOCR C5 on T1.\"OcrCode5\" = C5.OcrCode ");
            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "' ");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            while (!oRecordSet.EoF)
            {
                SolicitacaoCompraLinha solicitacao = new SolicitacaoCompraLinha();
                solicitacao.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                solicitacao.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                solicitacao.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
                solicitacao.ReqDate = DateTime.Parse(oRecordSet.Fields.Item("PQTReqDate").Value.ToString());
                solicitacao.IdLinha = int.Parse(oRecordSet.Fields.Item("VisOrder").Value.ToString());
                solicitacao.LineStatus = oRecordSet.Fields.Item("LineStatus").Value.ToString();
                solicitacao.CodigoItem = oRecordSet.Fields.Item("ItemCode").Value.ToString();
                solicitacao.DecricaoItem = oRecordSet.Fields.Item("ItemName").Value.ToString();
                solicitacao.Quantidade = double.Parse(oRecordSet.Fields.Item("Quantity").Value.ToString());
                solicitacao.PriceBefDi = double.Parse(oRecordSet.Fields.Item("PriceBefDi").Value.ToString());
                solicitacao.OcrCode = oRecordSet.Fields.Item("OcrCode").Value.ToString();
                solicitacao.OcrName = oRecordSet.Fields.Item("OcrName").Value.ToString();
                solicitacao.OcrCode2 = oRecordSet.Fields.Item("OcrCode2").Value.ToString();
                solicitacao.OcrName2 = oRecordSet.Fields.Item("OcrName2").Value.ToString();
                solicitacao.OcrCode3 = oRecordSet.Fields.Item("OcrCode3").Value.ToString();
                solicitacao.OcrName3 = oRecordSet.Fields.Item("OcrName3").Value.ToString();
                solicitacao.OcrCode4 = oRecordSet.Fields.Item("OcrCode4").Value.ToString();
                solicitacao.OcrName4 = oRecordSet.Fields.Item("OcrName4").Value.ToString();
                solicitacao.OcrCode5 = oRecordSet.Fields.Item("OcrCode5").Value.ToString();
                solicitacao.OcrName5 = oRecordSet.Fields.Item("OcrName5").Value.ToString();
                solicitacao.Deposito = oRecordSet.Fields.Item("WhsCode").Value.ToString();
                solicitacao.UM = oRecordSet.Fields.Item("BuyUnitMsr").Value.ToString();
                solicitacao.TxtLivre = oRecordSet.Fields.Item("FreeTxt").Value.ToString();

                solicitacoesLinhas.Add(solicitacao);
                oRecordSet.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return solicitacoesLinhas;
        }
        #endregion

        #region Editar Linha da Solicitação
        public string AlterarLinha(SolicitacaoCompraLinha solicitacaoCompraLinha, AddLinha type)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oSolicitacaoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);


                if (oSolicitacaoCompra.GetByKey(solicitacaoCompraLinha.DocEntry))
                {
                    if (type == AddLinha.Add)
                    {
                        oSolicitacaoCompra.Lines.Add();
                        oSolicitacaoCompra.Lines.ItemCode = solicitacaoCompraLinha.CodigoItem;
                    }
                    else
                    {
                        oSolicitacaoCompra.Lines.SetCurrentLine(solicitacaoCompraLinha.IdLinha);
                    }

                    oSolicitacaoCompra.Lines.Quantity = solicitacaoCompraLinha.Quantidade;
                    oSolicitacaoCompra.Lines.UnitPrice = solicitacaoCompraLinha.PriceBefDi;
                    oSolicitacaoCompra.Lines.CostingCode = solicitacaoCompraLinha.OcrCode;
                    oSolicitacaoCompra.Lines.CostingCode2 = solicitacaoCompraLinha.OcrCode2;
                    oSolicitacaoCompra.Lines.CostingCode3 = solicitacaoCompraLinha.OcrCode3;
                    oSolicitacaoCompra.Lines.CostingCode4 = solicitacaoCompraLinha.OcrCode4;
                    oSolicitacaoCompra.Lines.CostingCode5 = solicitacaoCompraLinha.OcrCode5;
                    oSolicitacaoCompra.Lines.WarehouseCode = solicitacaoCompraLinha.Deposito;
                    oSolicitacaoCompra.Lines.DiscountPercent = 0;
                    oSolicitacaoCompra.Lines.RequiredDate = solicitacaoCompraLinha.ReqDate;
                    oSolicitacaoCompra.Lines.FreeText = solicitacaoCompraLinha.TxtLivre;


                    if (oSolicitacaoCompra.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }


                System.Runtime.InteropServices.Marshal.ReleaseComObject(oSolicitacaoCompra);
                oSolicitacaoCompra = null;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Fechar Linha da Solicitação
        public string FecharStatusLinha(int DocEntry, int LineId)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oSolicitacaoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);

                if (oSolicitacaoCompra.GetByKey(DocEntry))
                {
                    oSolicitacaoCompra.Lines.SetCurrentLine(LineId);

                    oSolicitacaoCompra.Lines.LineStatus = SAPbobsCOM.BoStatus.bost_Close;

                    if (oSolicitacaoCompra.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oSolicitacaoCompra);
                oSolicitacaoCompra = null;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Fechar Status da Solicitação
        public string FecharStatus(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oSolicitacaoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);

                if (oSolicitacaoCompra.GetByKey(DocEntry))
                {
                    int iRet = oSolicitacaoCompra.Close();

                    if (iRet != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                        msg += " O Status Já está Fechado";
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oSolicitacaoCompra);
                oSolicitacaoCompra = null;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Create solicitação de compras
        public string Create(SolicitacaoCompra solicitacao, Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oRequest;

                oRequest = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);

                oRequest.DocDate = solicitacao.DocDate;
                oRequest.RequriedDate = solicitacao.ReqDate;
                oRequest.DocDueDate = solicitacao.DocDueDate;
                oRequest.RequesterName = login.User.ToString();
                oRequest.BPL_IDAssignedToInvoice = solicitacao.Filial;
                oRequest.Comments = solicitacao.Comments;

                foreach (var item in solicitacao.Itens)
                {

                    oRequest.Lines.ItemCode = item.CodigoItem;
                    oRequest.Lines.Quantity = item.Quantidade;
                    oRequest.Lines.UnitPrice = item.PriceBefDi;
                    oRequest.Lines.ShipDate = item.ItemReqDate;
                    oRequest.Lines.CostingCode = item.OcrCode;
                    oRequest.Lines.CostingCode2 = item.OcrCode2;
                    oRequest.Lines.CostingCode3 = item.OcrCode3;
                    oRequest.Lines.CostingCode4 = item.OcrCode4;
                    oRequest.Lines.CostingCode5 = item.OcrCode5;

                    oRequest.Lines.RequiredDate = item.ItemReqDate;
                    oRequest.Lines.WarehouseCode = item.Deposito;
                    oRequest.Lines.FreeText = item.TxtLivre;
                    oRequest.Lines.Add();
                }

                if (oRequest.Add() != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
                else
                {
                    int iDocEntry = int.Parse(oCompany.GetNewObjectKey());
                    msg = _anexoService.InserirAnexos(solicitacao.ListaAnexos, iDocEntry, Models.Enums.TipoDocumento.SolicitacaoCompra);
                }
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion
    }
}