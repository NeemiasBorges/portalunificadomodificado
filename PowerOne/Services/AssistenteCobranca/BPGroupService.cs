﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Util;
using System.Collections.Generic;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class BPGroupService
    {
        private GroupTypeMailService _groupTypeMailService = new GroupTypeMailService();

        #region Lista de Grupos de Clientes
        public List<BPGroup> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BPGroup> bPGroups = new List<BPGroup>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT * FROM \"@P1_BPGROUP\" WHERE \"U_P1_Ativado\" = 'Y'");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                bPGroups.Add(new BPGroup()
                {
                    CodeGroup = oRecordset.Fields.Item("Code").Value.ToString(),
                    DescGroup = oRecordset.Fields.Item("Name").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bPGroups;
        }
        #endregion

        #region Contar
        public int Contar()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            int contar = 0;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"Code\") \"Count\" FROM \"@P1_BPGROUP\"  WHERE \"U_P1_Ativado\" = 'Y'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                contar = int.Parse(oRecordset.Fields.Item("Count").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return contar;
        }
        #endregion

        #region Seleciona um grupo de clientes específico
        public BPGroup GetByKey(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            BPGroup bPGroups = null;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT * FROM \"@P1_BPGROUP\" WHERE \"Code\" = '" + code + "' AND \"U_P1_Ativado\" = 'Y'");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                bPGroups = new BPGroup()
                {
                    CodeGroup = oRecordset.Fields.Item("Code").Value.ToString(),
                    DescGroup = oRecordset.Fields.Item("Name").Value.ToString()
                };
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bPGroups;
        }
        #endregion

        #region Verifica a existência de um grupo de clientes
        public bool VerifyIfExist(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oBPGroup = oCompany.UserTables.Item("P1_BPGROUP");

            bool bExist = oBPGroup.GetByKey(code);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oBPGroup);
            oBPGroup = null;

            return bExist;
        }
        #endregion

        #region Verifica a existência de um grupo clientes pelo nome
        public bool VerifyByName(string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            bool bExiste = false;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT * FROM \"@P1_BPGROUP\" WHERE \"Name\" = '" + name + "' AND \"U_P1_Ativado\" = 'Y' ORDER BY \"Code\"");

            oRecordset.DoQuery(sql.ToString());

            if (oRecordset.RecordCount > 0)
            {
                bExiste = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bExiste;
        }
        #endregion

        #region Criar Grupo de clientes
        public void Create(BPGroup bPGroup)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oBPGroup = oCompany.UserTables.Item("P1_BPGROUP");

            int iRetCode = -1;

            oBPGroup.Code = bPGroup.CodeGroup;
            oBPGroup.Name = bPGroup.DescGroup;

            iRetCode = oBPGroup.Add();


            if (iRetCode != 0)
            {
                string sErrMessage = oCompany.GetLastErrorDescription();
                Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oBPGroup);
            oBPGroup = null;
        }
        #endregion

        #region Editar um grupo de clientes
        public void Edit(BPGroup bPGroup)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oBPGroup = oCompany.UserTables.Item("P1_BPGROUP");
            int iRetcode = -1;

            if (oBPGroup.GetByKey(bPGroup.CodeGroup))
            {
                oBPGroup.Code = bPGroup.CodeGroup;
                oBPGroup.Name = bPGroup.DescGroup;

                iRetcode = oBPGroup.Update();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oBPGroup);
            oBPGroup = null;
        }
        #endregion

        #region Deleta um grupo de clientes
        public void Delete(BPGroup bpGroup, List<string> modelLinks)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oBPGroup = oCompany.UserTables.Item("P1_BPGROUP");
            int iRetcode = -1;

            if (oBPGroup.GetByKey(bpGroup.CodeGroup))
            {
                try
                {
                    //Excluindo vínculos
                    foreach (string item in modelLinks)
                    {
                        _groupTypeMailService.Delete(item);
                    }

                    oBPGroup.UserFields.Fields.Item("U_P1_Ativado").Value = "N";

                    iRetcode = oBPGroup.Update();

                    if (iRetcode != 0)
                    {
                        string sErrMessage = oCompany.GetLastErrorDescription();
                        Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                    }
                }
                catch { }

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oBPGroup);
            oBPGroup = null;
        }
        #endregion

        #region Pega o código do link
        public List<string> GetLinkCodes(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<string> LinkedCodes = new List<string>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"Code\"  FROM \"@P1_GRTPMAIL\" WHERE \"U_P1_GroupCode\" = '" + code + "'");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();

            while (!oRecordset.EoF)
            {
                LinkedCodes.Add(oRecordset.Fields.Item("Code").Value.ToString());
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return LinkedCodes;
        }
        #endregion

        //pegar todos os grupos que estão linkados aos parceiros de negócio
        #region GetGropLikedBP
        public List<BPGroup> GetGropLikedBP()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BPGroup> groups = new List<BPGroup>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"@P1_BPGROUP\".* FROM \"@P1_BPGROUP\" ");
            SQL.AppendLine("INNER JOIN OCRD ");
            SQL.AppendLine("ON OCRD.\"U_P1_BPGroup\" = \"@P1_BPGROUP\".Code ");
            SQL.AppendLine("GROUP BY \"Code\",\"Name\",\"U_P1_Ativado\"");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();

            while (!oRecordset.EoF)
            {
                groups.Add(new BPGroup() {
                    CodeGroup = oRecordset.Fields.Item("Code").Value.ToString(),
                    DescGroup = oRecordset.Fields.Item("Name").Value.ToString()
                });
                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return groups;
        }
        #endregion

        #region Verifica se o grupo está linkado
        public bool VerifyIfIsLinked(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            bool exist = false;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"U_P1_BPGroup\" FROM OCRD WHERE \"U_P1_BPGroup\" = '" + code + "'");

            oRecordset.DoQuery(sql.ToString());

            if(oRecordset.RecordCount > 0)
            {
                exist = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return exist;
        }
        #endregion
    }

}