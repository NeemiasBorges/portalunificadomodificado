﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Util;
using System.Collections.Generic;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class TypeModelService
    {
        #region Criar tipo de modelo
        public void Create(string code, string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable typeModel = oCompany.UserTables.Item("P1_TYPEMAIL");

            int iRetCode = -1;

            typeModel.Code = code;
            typeModel.Name = name;

            iRetCode = typeModel.Add();

            if (iRetCode != 0)
            {
                string sErrMessage = oCompany.GetLastErrorDescription();
                Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(typeModel);
            typeModel = null;

        }
        #endregion

        #region Verifica se existe o modelo
        public bool VerifyIfExist(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable typeModel = oCompany.UserTables.Item("P1_TYPEMAIL");

            bool bExiste = typeModel.GetByKey(code);


            System.Runtime.InteropServices.Marshal.ReleaseComObject(typeModel);
            typeModel = null;

            return bExiste;
        }
        #endregion

        #region Listar modelos
        public List<TypeModel> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<TypeModel> listTypeModels = new List<TypeModel>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT * FROM \"@P1_TYPEMAIL\" ORDER BY \"Code\"");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                listTypeModels.Add(new TypeModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Name = oRecordset.Fields.Item("Name").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listTypeModels;
        }
        #endregion

        #region Selecionar Modelo Específico
        public TypeModel GetByKey(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT * FROM \"@P1_TYPEMAIL\" where \"Code\" = '" + code + "'");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();

            TypeModel typeModel = new TypeModel()
            {
                Code = oRecordset.Fields.Item("Code").Value.ToString(),
                Name = oRecordset.Fields.Item("Name").Value.ToString()
            };

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return typeModel;
        }
        #endregion
    }
}