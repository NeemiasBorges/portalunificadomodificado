﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class BusinessPartnerService
    {
        #region Lista de clientes
        public List<BusinessPartner> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BusinessPartner> businessPartners = new List<BusinessPartner>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"CardCode\", \"CardName\", \"U_P1_BPGroup\" FROM OCRD WHERE \"CardType\" = 'C'");

            oRecordset.DoQuery(sql.ToString());

            while (!oRecordset.EoF)
            {
                businessPartners.Add(new BusinessPartner()
                {
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    CardName = oRecordset.Fields.Item("CardName").Value.ToString(),
                    CodeGroup = oRecordset.Fields.Item("U_P1_BPGroup").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return businessPartners;
        }
        #endregion

        #region Pesquisar cliente
        public List<BusinessPartner> Search(BusinessPartner obp)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BusinessPartner> businessPartners = new List<BusinessPartner>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"CardCode\", \"CardName\", \"U_P1_BPGroup\" FROM \"OCRD\" ");
            sql.AppendLine("WHERE \"CardType\" = 'C' ");
            sql.AppendLine("AND \"CardCode\" LIKE '%" + obp.CardCode + "%' ");
            sql.AppendLine("AND \"CardName\" LIKE '%" + obp.CardName + "%' ");

            if (obp.CodeGroup != null && obp.CodeGroup != "")
            {
                sql.AppendLine("AND \"U_P1_BPGroup\" = '" + obp.CodeGroup + "'");
            }
            
            if(obp.CodeGroup == null)
            {
                sql.AppendLine("AND \"U_P1_BPGroup\" IS NULL");
            }


            oRecordset.DoQuery(sql.ToString());

            while (!oRecordset.EoF)
            {
                businessPartners.Add(new BusinessPartner()
                {
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    CardName = oRecordset.Fields.Item("CardName").Value.ToString(),
                    CodeGroup = oRecordset.Fields.Item("U_P1_BPGroup").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return businessPartners;
        }
        #endregion

        #region Editar cliente
        public void Edit(BusinessPartner bp)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.BusinessPartners oBusinessPartner = (SAPbobsCOM.BusinessPartners)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            int iRetCode = -1;

            if (oBusinessPartner.GetByKey(bp.CardCode))
            {
                if (bp.CodeGroup != null)
                {
                    oBusinessPartner.UserFields.Fields.Item("U_P1_BPGroup").Value = bp.CodeGroup;
                }
                else
                {
                    oBusinessPartner.UserFields.Fields.Item("U_P1_BPGroup").Value = "";
                }

                iRetCode = oBusinessPartner.Update();

                if (iRetCode != 0)
                {
                    string sErrMsg = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMsg, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oBusinessPartner);
            oBusinessPartner = null;
        }
        #endregion

        #region CountClientsWithouGroup 
        public int CountClientsWithouGroup()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT COUNT(\"CardCode\") AS \"Count\" FROM OCRD ");
            sql.AppendLine("WHERE \"CardType\" = 'C' AND \"U_P1_BPGroup\" IS NULL");

            oRecordset.DoQuery(sql.ToString());

            int count = Convert.ToInt32(oRecordset.Fields.Item("Count").Value);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return count;
        }
        #endregion
    }
}