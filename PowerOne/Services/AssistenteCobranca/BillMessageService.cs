﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class BillMessageService
    {
        #region Lista de Contas a Receber
        public List<BillMessage> List(SendingWizardViewModel searchParams)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BillMessage> billMessages = new List<BillMessage>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"Contas\".*, SUBSTRING(\"PN\".\"CardName\",1, 30)  AS \"CardName\", \"PN\".\"U_P1_BPGroup\",\"PN\".\"E_Mail\",\"PN\".\"U_P1_EmailList\"  FROM ");

            sql.AppendLine(SQLComand());

            //Verificando se há parâmetros de pesquisa
            if (searchParams != null)
            {
                if (searchParams.MailTypeCode == "T0003")
                {
                    //Hana: sql += "WHERE DAYS_BETWEEN(CURRENT_DATE,\"Contas\".\"DueDate\") < 0 AND "
                    if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        sql.AppendLine("WHERE DAYS_BETWEEN(CURRENT_DATE,\"Contas\".\"DueDate\") < 0 AND ");
                    }
                    else
                    {
                        sql.AppendLine("WHERE DATEDIFF(DAY,CONVERT (date, SYSDATETIME()),CONVERT (date, \"Contas\".\"DueDate\")) < 0 AND ");
                    }
                }
                else
                {
                    if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        sql.AppendLine("WHERE DAYS_BETWEEN(CURRENT_DATE,\"Contas\".\"DueDate\") >= 0 AND ");
                    }
                    else
                    {
                        sql.AppendLine("WHERE DATEDIFF(DAY,CONVERT (date, SYSDATETIME()),CONVERT (date, \"Contas\".\"DueDate\")) >= 0 AND ");
                    }   
                }

                //Código do PN
                if (searchParams.CardCode != null)
                {
                    sql.AppendLine("\"Contas\".\"CardCode\" = '" + searchParams.CardCode + "'  AND ");
                }

                //Código do Grupo
                if (searchParams.CodeGroup != null)
                {
                    sql.AppendLine("\"PN\".\"U_P1_BPGroup\" = '" + searchParams.CodeGroup + "' AND ");
                }

                //Intervalo de datas
                if (searchParams.DueDateFrom != default(DateTime))
                {
                    sql.AppendLine("\"Contas\".\"DueDate\" >= '" + searchParams.DueDateFrom.ToString("yyyyMMdd") + "' AND "); 
                }
                if (searchParams.DueDateTo != default(DateTime))
                {
                    sql.AppendLine("\"Contas\".\"DueDate\" <= '" + searchParams.DueDateTo.ToString("yyyyMMdd") + "' AND ");
                }


                //if (searchParams.DaysBefore != 0)
                //{
                //Hana: sql += "WHERE DAYS_BETWEEN(CURRENT_DATE,\"Contas\".\"DueDate\") = " + searchParams.DaysBefore + " OR ";
                //Hana: sql += "WHERE DAYS_BETWEEN(CURRENT_DATE,\"Contas\".\"DueDate\") = " + (-1 * searchParams.DaysAfter) + " AND ";
                //    sql += "DATEDIFF(DAY, CONVERT(date, SYSDATETIME()), CONVERT(date, \"Contas\".\"DueDate\")) = " + searchParams.DaysBefore + " OR ";
                //    sql += "DATEDIFF(DAY, CONVERT(date, SYSDATETIME()), CONVERT(date, \"Contas\".\"DueDate\")) = " + (-1 * searchParams.DaysAfter) + " AND ";
                //}

                //Valor intervalo de valores do documento
                if (searchParams.TotalValueFrom != 0 )
                {
                    sql.AppendLine("\"Contas\".\"InsTotal\" >= " + searchParams.TotalValueFrom + " AND ");
                }
                if (searchParams.TotalValueTo != 0)
                {
                    sql.AppendLine("\"Contas\".\"InsTotal\" <= " + searchParams.TotalValueTo);
                }
            }

            //Verificar se há algum 'AND' no final da string SQL e remover para não dar erro na consulta
            if (sql.ToString().Substring(sql.Length - 6) == "AND \r\n")
            {
                StringBuilder aux = new StringBuilder(sql.ToString().Substring(0, sql.Length - 6));
                sql = aux;
            }

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                billMessages.Add(new BillMessage()
                {
                    DocCode = oRecordset.Fields.Item("Code").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    CardName = oRecordset.Fields.Item("CardName").Value.ToString(),
                    DocDate = (DateTime)oRecordset.Fields.Item("DocDate").Value,
                    DocDueDate = (DateTime)oRecordset.Fields.Item("DueDate").Value,
                    DocTotal = double.Parse(oRecordset.Fields.Item("InsTotal").Value.ToString()),
                    GroupCode = oRecordset.Fields.Item("U_P1_BPGroup").Value.ToString(),
                    EmailAdress = oRecordset.Fields.Item("E_Mail").Value.ToString(),
                    InvoiceModel = oRecordset.Fields.Item("NfmName").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("PeyMethod").Value.ToString(),
                    SerialNumber = oRecordset.Fields.Item("Serial").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return billMessages;
        }
        #endregion

        //Utilzado para envio de email
        #region Selecionar contas a receber específicas
        public List<BillMessage> GetByCollectionKeys(string[] code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BillMessage> billMessages = new List<BillMessage>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("Select \"Contas\".*, \"PN\".\"CardName\", \"PN\".\"U_P1_BPGroup\",\"PN\".\"E_Mail\",\"PN\".\"U_P1_EmailList\"  FROM ");
            sql.AppendLine(SQLComand());

            sql.AppendLine("WHERE \"Contas\".\"Code\" in ('" + string.Join("','", code) + "')");

            oRecordset.DoQuery(sql.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                billMessages.Add(new BillMessage()
                {
                    DocCode = oRecordset.Fields.Item("Code").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    CardName = oRecordset.Fields.Item("CardName").Value.ToString(),
                    DocDate = (DateTime)oRecordset.Fields.Item("DocDate").Value,
                    DocDueDate = (DateTime)oRecordset.Fields.Item("DueDate").Value,
                    DocTotal = double.Parse(oRecordset.Fields.Item("InsTotal").Value.ToString()),
                    GroupCode = oRecordset.Fields.Item("U_P1_BPGroup").Value.ToString(),
                    EmailAdress = oRecordset.Fields.Item("E_Mail").Value.ToString(),
                    InvoiceModel = oRecordset.Fields.Item("NfmName").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("PeyMethod").Value.ToString(),
                    SerialNumber = oRecordset.Fields.Item("Serial").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return billMessages;
        }
        #endregion

        #region Conta todas contas a receber
        public int CountBills(string where)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int count = 0;
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT Count(*) as \"Count\" FROM");

            sql.AppendLine(SQLComand());

            if (where != null)
            {
                sql.AppendLine(" " + where);
            }

            oRecordset.DoQuery(sql.ToString());

            count = int.Parse(oRecordset.Fields.Item("Count").Value.ToString());

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return count;
        }
        #endregion

        #region SQLComand
        public string SQLComand()
        {
            string sql = "(";
            sql += "SELECT CONCAT(CONCAT(CONCAT('NFS', \"T0\".\"DocNum\"),'-PR'),\"T1\".\"InstlmntID\") as \"Code\", \"T4\".\"NfmName\", \"T0\".\"CardCode\", \"T0\".\"DocDate\", \"T1\".\"DueDate\", (\"T1\".\"InsTotal\" - \"T1\".\"PaidToDate\") as \"InsTotal\", CASE WHEN \"T0\".\"PeyMethod\" = '' THEN 'G' ELSE \"T3\".\"BankTransf\" END AS \"PeyMethod\",CAST(\"T0\".\"Serial\" AS VARCHAR) \"Serial\"";
            sql += "FROM \"OINV\" \"T0\" ";
            sql += "INNER JOIN \"INV6\" \"T1\" ON ";
            sql += "\"T0\".\"DocEntry\" = \"T1\".\"DocEntry\" ";
            sql += "LEFT JOIN \"OPYM\" \"T3\" ON \"T3\".\"PayMethCod\" = \"T0\".\"PeyMethod\" ";
            sql += "INNER JOIN ONFM \"T4\" ON \"T0\".\"Model\" = \"T4\".\"AbsEntry\" ";
            sql += "WHERE  \"T1\".\"Status\" = 'O' ";

            sql += "UNION ALL ";

            sql += "SELECT CONCAT(CONCAT(CONCAT('ADT', \"T1\".\"DocEntry\"),'-PR'),\"T1\".\"InstlmntID\") as \"Code\", '-',\"T0\".\"CardCode\", \"T0\".\"DocDate\",\"T1\".\"DueDate\", (\"T1\".\"InsTotal\" - \"T1\".\"PaidToDate\") as \"InsTotal\", CASE WHEN \"T0\".\"PeyMethod\" = '' THEN 'G' ELSE \"T3\".\"BankTransf\" END AS \"PeyMethod\",''  ";
            sql += "FROM \"ODPI\" \"T0\" ";
            sql += "INNER JOIN \"DPI6\" \"T1\" ON \"T0\".\"DocEntry\" = \"T1\".\"DocEntry\" ";
            sql += "LEFT JOIN \"OPYM\" \"T3\" ON \"T3\".\"PayMethCod\" = \"T0\".\"PeyMethod\" ";
            sql += "WHERE \"T1\".\"Status\" = 'O' ";

            sql += "UNION ALL ";

            sql += "SELECT CONCAT('LCM',\"T1\".\"TransId\") as \"Code\",'-',\"T1\".\"ShortName\", \"T1\".\"RefDate\",\"T1\".\"DueDate\",\"T1\".\"Debit\", 'G','' ";
            sql += "FROM \"OJDT\" \"T0\" ";
            sql += " INNER JOIN \"JDT1\" \"T1\" ";
            sql += "ON \"T0\".\"TransId\" = \"T1\".\"TransId\" ";
            sql += "WHERE \"T0\".\"TransId\" = \"T0\".\"BaseRef\" ";
            sql += "AND (\"T1\".\"MthDate\" is null or \"T1\".\"MthDate\" = '') AND \"T1\".\"Credit\" = 0";

            sql += ") \"Contas\" ";


            sql += "INNER JOIN \"OCRD\" \"PN\" ON ";
            sql += "\"PN\".\"CardCode\" =  \"Contas\".\"CardCode\" ";

            return sql;
        }
        #endregion
    }
}