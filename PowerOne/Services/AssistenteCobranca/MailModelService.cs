﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.DynamicData;
using System.Data.SqlClient;
using PowerOne.Util;

namespace PowerOne.Services.AssistenteCobranca
{
    public class MailModelService
    {
        #region create
        public void Create(MailModel mailModel)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oMailModel = oCompany.UserTables.Item("P1_MAILMODEL");

            int iRetCode = -1;

            oMailModel.Code = mailModel.Code;
            oMailModel.Name = mailModel.Description;
            oMailModel.UserFields.Fields.Item("U_P1_CodeType").Value = mailModel.TypeId;
            oMailModel.UserFields.Fields.Item("U_P1_Message").Value = mailModel.Message;
            oMailModel.UserFields.Fields.Item("U_P1_Subject").Value = mailModel.Subject;
            oMailModel.UserFields.Fields.Item("U_P1_PayMethod").Value = mailModel.PayMethod;

            iRetCode = oMailModel.Add();

            if (iRetCode != 0)
            {
                string sErrMessage = oCompany.GetLastErrorDescription();
                Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oMailModel);
            oMailModel = null;

        }
        #endregion

        #region Contar
        public int Contar()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            int consultaModelos = 0;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"Code\") \"Count\" FROM \"@P1_MAILMODEL\"");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                consultaModelos = int.Parse(oRecordset.Fields.Item("Count").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return consultaModelos;
        }
        #endregion

        #region verefica se existe
        public bool VerifyIfExist(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oMailModel = oCompany.UserTables.Item("P1_MAILMODEL");

            bool bExist = oMailModel.GetByKey(code);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oMailModel);
            oMailModel = null;

            return bExist;
        }
#endregion

        #region verifica se possui grupo
        public bool VerifyIfIsLinkedGroup(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            bool isLinked = false;
            StringBuilder SQL = new StringBuilder();

             SQL.AppendLine("SELECT * FROM \"@P1_GRTPMAIL\" WHERE \"U_P1_CodeMail\" = '" + code + "'");

            oRecordset.DoQuery(SQL.ToString());

            int count = oRecordset.RecordCount;

            if (count != 0)
            {
                isLinked = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return isLinked;
        }
        #endregion

        #region verifica usado
        public bool VerifyIfAlreadyIsUsed(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            bool isLinked = false;
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM \"@P1_LOGEMAIL\" WHERE \"U_P1_ModelMail\" = '" + code + "'");

            oRecordset.DoQuery(SQL.ToString());

            int count = oRecordset.RecordCount;

            if (count != 0)
            {
                isLinked = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return isLinked;
        }
        #endregion

        #region verifica enviado
        public bool VerifyIfWasSended(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            bool isLinked = false;

            string sql = "SELECT * FROM \"@P1_LOGEMAIL\" WHERE \"U_P1_ModelMail\" = '" + code + "'";

            oRecordset.DoQuery(sql);

            int count = oRecordset.RecordCount;

            if (count != 0)
            {
                isLinked = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return isLinked;
        }
        #endregion

        #region list
        public List<MailModel> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<MailModel> mailModel = new List<MailModel>();
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM \"@P1_MAILMODEL\"");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                mailModel.Add(new MailModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Description = oRecordset.Fields.Item("Name").Value.ToString(),
                    TypeId = oRecordset.Fields.Item("U_P1_CodeType").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_Message").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("U_P1_PayMethod").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return mailModel;
        }
#endregion

        #region select via tipo
        public List<MailModel> FindByType(string codeType)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<MailModel> mailModel = new List<MailModel>();
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM \"@P1_MAILMODEL\" WHERE \"U_P1_CodeType\" = '" + codeType + "' ORDER BY \"Code\"");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                mailModel.Add(new MailModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Description = oRecordset.Fields.Item("Name").Value.ToString(),
                    TypeId = oRecordset.Fields.Item("U_P1_CodeType").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_Message").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("U_P1_PayMethod").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return mailModel;
        }
#endregion

        #region select id

        public MailModel GetById(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM \"@P1_MAILMODEL\" where \"Code\" = '" + code + "' ORDER BY \"Code\"");
            MailModel model = null;

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();

            if (!oRecordset.EoF)
            {
                model = new MailModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Description = oRecordset.Fields.Item("Name").Value.ToString(),
                    TypeId = oRecordset.Fields.Item("U_P1_CodeType").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_Message").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("U_P1_PayMethod").Value.ToString()
                };
            }
            

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return model;
        }
#endregion

        #region verifica nome
        public bool VerifyByName(string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            bool bExiste = false;
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM \"@P1_MAILMODEL\" where \"Name\" = '" + name + "' ORDER BY \"Code\"");

            oRecordset.DoQuery(SQL.ToString());

            if (oRecordset.RecordCount > 0)
            {
                bExiste = true;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bExiste;
        }
#endregion

        #region select via grupo e tipo
        public MailModel GetByGroupAndType(string groupCode, string typeCode, string payMethod)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            MailModel mailModel = new MailModel();
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT \"@P1_MAILMODEL\".* FROM \"@P1_GRTPMAIL\" ");
            SQL.AppendLine("INNER JOIN \"@P1_MAILMODEL\"  ON ");
            SQL.AppendLine("\"@P1_GRTPMAIL\".\"U_P1_CodeMail\" = \"@P1_MAILMODEL\".\"Code\" ");
            SQL.AppendLine("WHERE \"@P1_GRTPMAIL\".\"U_P1_GroupCode\" = '" + groupCode + "'");
            SQL.AppendLine("AND \"@P1_GRTPMAIL\".\"U_P1_CodeType\" = '" + typeCode + "'");
            SQL.AppendLine("AND \"@P1_GRTPMAIL\".\"U_P1_PayMethod\" = '" + payMethod + "'");
            
                
            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                mailModel = new MailModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Description = oRecordset.Fields.Item("Name").Value.ToString(),
                    TypeId = oRecordset.Fields.Item("U_P1_CodeType").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_Message").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("U_P1_PayMethod").Value.ToString()
                };
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return mailModel;
        }
#endregion

        #region list Select via tipo e metodo
        public List<MailModel> GetByTypepAndPayMethod(string typeCode, string payMethod)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<MailModel> mailModel = new List<MailModel>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_MAILMODEL\" ");
            SQL.AppendLine("WHERE \"U_P1_CodeType\" = '" + typeCode + "'");
            SQL.AppendLine("AND \"U_P1_PayMethod\" = '" + payMethod + "'");


            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                mailModel.Add(new MailModel()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    Description = oRecordset.Fields.Item("Name").Value.ToString(),
                    TypeId = oRecordset.Fields.Item("U_P1_CodeType").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_Message").Value.ToString(),
                    PayMethod = oRecordset.Fields.Item("U_P1_PayMethod").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return mailModel;
        }
#endregion

        #region edit
        public void Edit(MailModel mailModel)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oMailModel = oCompany.UserTables.Item("P1_MAILMODEL");
            int iRetcode = -1;

            if (oMailModel.GetByKey(mailModel.Code))
            {
                oMailModel.Code = mailModel.Code;
                oMailModel.Name = mailModel.Description;
                oMailModel.UserFields.Fields.Item("U_P1_CodeType").Value = mailModel.TypeId;
                oMailModel.UserFields.Fields.Item("U_P1_Message").Value = mailModel.Message;
                oMailModel.UserFields.Fields.Item("U_P1_Subject").Value = mailModel.Subject;
                oMailModel.UserFields.Fields.Item("U_P1_PayMethod").Value = mailModel.PayMethod;

                iRetcode = oMailModel.Update();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oMailModel);
            oMailModel = null;
        }
#endregion

        #region delete

        public void Delete(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oMailModel = oCompany.UserTables.Item("P1_MAILMODEL");

            if (oMailModel.GetByKey(code))
            {
                int iRetcode = oMailModel.Remove();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oMailModel);
            oMailModel = null;
        }
        #endregion
    }
}