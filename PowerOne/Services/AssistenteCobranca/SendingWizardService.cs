﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.DynamicData;

namespace PowerOne.Services.AssistenteCobranca
{
    public class SendingWizardService
    {
        #region Verificar se todos os grupos possuem Modelos
        public bool VerificarGruposPossuiTodosModelos(BPGroup group)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //Inicia como verdadeiro
            bool response = true;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Count(\"U_P1_GroupCode\") as \"Quantidade\" FROM \"@P1_GRTPMAIL\" ");
            SQL.AppendLine("WHERE \"U_P1_GroupCode\" = '" + group.CodeGroup + "' ");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                /*
                 * Cada grupo precisa possuir 9 modelos cadastrados,pois:
                 * São 3 categorias de Modelo e 3 Formas de Pagamento para cada uma
                 */
                if (Convert.ToInt32(oRecordset.Fields.Item("Quantidade").Value) != 9)
                {
                    response = false;
                }
                oRecordset.MoveNext();
            }

            return response;
        }
        #endregion
    }
}