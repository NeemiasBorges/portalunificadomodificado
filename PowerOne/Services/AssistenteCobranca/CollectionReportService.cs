﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class CollectionReportService
    {
        #region List
        public List<CollectionReport> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<CollectionReport> collectionReports = new List<CollectionReport>();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT Count(\"U_P1_CRCode\") as \"Cobrancas\",\"U_P1_CRCode\", \"U_P1_DueDate\", \"U_P1_CardCode\", \"U_P1_CrTotal\" ");
            sql.AppendLine("FROM \"@LOGEMAIL\"");
            sql.AppendLine("GROUP BY \"U_P1_DueDate\", \"U_P1_CardCode\", \"U_P1_CrTotal\",\"U_P1_CRCode\"");

            oRecordset.DoQuery(sql.ToString());

            while (!oRecordset.EoF)
            {
                collectionReports.Add(new CollectionReport()
                {
                    BillCode= oRecordset.Fields.Item("U_P1_CRCode").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("U_P1_CardCode").Value.ToString(),
                    DueDate = (DateTime)oRecordset.Fields.Item("U_P1_DueDate").Value,
                    NumberOfSubmitions = int.Parse(oRecordset.Fields.Item("Cobrancas").Value.ToString()),
                    TotalValue = double.Parse(oRecordset.Fields.Item("U_P1_CrTotal").Value.ToString(), CultureInfo.InvariantCulture)
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return collectionReports;
        }
        #endregion

        #region Pesquisar Cobranças
        public List<CollectionReport> Search(string TitleCode, string CardCode, string DueDate, double TotalFrom, double TotalTo)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<CollectionReport> collectionReports = new List<CollectionReport>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Count(\"U_P1_CRCode\") as \"Cobrancas\",\"U_P1_CRCode\", \"U_P1_DueDate\", \"U_P1_CardCode\", \"U_P1_CrTotal\" ");
            SQL.AppendLine("FROM \"@P1_LOGEMAIL\"");
            SQL.AppendLine("WHERE \"U_P1_CRCode\" like '%" + TitleCode + "%' AND ");
            SQL.AppendLine("\"U_P1_CardCode\" like '%" + CardCode + "%' AND ");

            if (!String.IsNullOrEmpty(DueDate))
            {
                SQL.AppendLine("\"U_P1_DueDate\" = '" + (DateTime.ParseExact(DueDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "'AND ");
            }

            if (TotalFrom != 0)
            {
                SQL.AppendLine("\"U_P1_CrTotal\" >= " + TotalFrom + " AND ");
            }

            if (TotalTo != 0)
            {
                SQL.AppendLine("\"U_P1_CrTotal\" <= " + TotalTo + " AND ");
            }

            //Verificar se há algum 'AND' no final da string SQL e remover para não dar erro na consulta
            string teste = (SQL.ToString()).Substring(SQL.Length - 6);
            if ((SQL.ToString()).Substring(SQL.Length - 6) == "AND \r\n")
            {
                StringBuilder aux = new StringBuilder(SQL.ToString().Substring(0, SQL.Length - 6));
                SQL = aux;
            }


            SQL.AppendLine("GROUP BY \"U_P1_DueDate\", \"U_P1_CardCode\", \"U_P1_CrTotal\",\"U_P1_CRCode\"");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                collectionReports.Add(new CollectionReport()
                {
                    BillCode = oRecordset.Fields.Item("U_P1_CRCode").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("U_P1_CardCode").Value.ToString(),
                    DueDate = (DateTime)oRecordset.Fields.Item("U_P1_DueDate").Value,
                    NumberOfSubmitions = int.Parse(oRecordset.Fields.Item("Cobrancas").Value.ToString()),
                    TotalValue = double.Parse(oRecordset.Fields.Item("U_P1_CrTotal").Value.ToString(), CultureInfo.InvariantCulture)
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return collectionReports;
        }

        #endregion
    }
}