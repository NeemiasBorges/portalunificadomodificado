﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.DynamicData;
using PowerOne.Util;

namespace PowerOne.Services.AssistenteCobranca
{
    public class GroupTypeMailService
    {
        #region create
        public void Create(string code, string name, string uGroupCode, string uCodeType, string uCodeMail, string PayMethod)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oGrTpMail = oCompany.UserTables.Item("P1_GRTPMAIL");
            int iRetcode = -1;


            oGrTpMail.Code = code;
            oGrTpMail.Name = name;
            oGrTpMail.UserFields.Fields.Item("U_P1_GroupCode").Value = uGroupCode;
            oGrTpMail.UserFields.Fields.Item("U_P1_CodeType").Value = uCodeType;
            oGrTpMail.UserFields.Fields.Item("U_P1_CodeMail").Value = uCodeMail;
            oGrTpMail.UserFields.Fields.Item("U_P1_PayMethod").Value = PayMethod;

            iRetcode = oGrTpMail.Add();

            if (iRetcode != 0)
            {
                string sErrMessage = oCompany.GetLastErrorDescription();
                Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oGrTpMail);
            oGrTpMail = null;
        }
        #endregion

        #region verifica existe
        public bool VerifyIfExist(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oGroupTypeMail = oCompany.UserTables.Item("P1_GRTPMAIL");

            bool response = oGroupTypeMail.GetByKey(code);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oGroupTypeMail);
            oGroupTypeMail = null;

            return response;
        }
        #endregion

        #region delete
        public void Delete(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oGroupTypeMail = oCompany.UserTables.Item("P1_GRTPMAIL");

            if (oGroupTypeMail.GetByKey(code))
            {
                int iRetcode = oGroupTypeMail.Remove();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oGroupTypeMail);
            oGroupTypeMail = null;
        }
        #endregion

        #region select via tipo e metodoPagamento
        public string GetByGroupTypePayMethod(string group, string type, string payMethod)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string code = null;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"Code\" FROM \"@P1_GRTPMAIL\" ");
            SQL.AppendLine("WHERE \"U_P1_GroupCode\" = '" + group + "'");
            SQL.AppendLine("AND \"U_P1_CodeType\"  = '" + type + "'");
            SQL.AppendLine("AND \"U_P1_PayMethod\" = '" + payMethod + "'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                code = oRecordset.Fields.Item("Code").Value.ToString();
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return code;

        }
        #endregion
    }
}