﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class LogEmailService
    {
        #region Create
        public void Create(LogEmail logEmail)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oLogEMail = oCompany.UserTables.Item("P1_LOGEMAIL");

            int iRetCode = -1;
            oLogEMail.Code = GetLastCode("@P1_LOGEMAIL");
            oLogEMail.Name = GetLastCode("@P1_LOGEMAIL");
            oLogEMail.UserFields.Fields.Item("U_P1_CRCode").Value = logEmail.BillCode;
            oLogEMail.UserFields.Fields.Item("U_P1_Date").Value = logEmail.SendingDate;
            oLogEMail.UserFields.Fields.Item("U_P1_DueDate").Value = logEmail.DueDate;
            oLogEMail.UserFields.Fields.Item("U_P1_TpMailCode").Value = logEmail.TypeId;
            oLogEMail.UserFields.Fields.Item("U_P1_ModelMail").Value = logEmail.MailModelId;
            oLogEMail.UserFields.Fields.Item("U_P1_EmailTxt").Value = logEmail.Message;
            oLogEMail.UserFields.Fields.Item("U_P1_Subject").Value = logEmail.Subject;
            oLogEMail.UserFields.Fields.Item("U_P1_EmlAdress").Value = logEmail.EmailAdress;
            oLogEMail.UserFields.Fields.Item("U_P1_CrTotal").Value = logEmail.BillTotal;
            oLogEMail.UserFields.Fields.Item("U_P1_Status").Value = logEmail.Status;
            oLogEMail.UserFields.Fields.Item("U_P1_CardCode").Value = logEmail.CardCode;
            oLogEMail.UserFields.Fields.Item("U_P1_ListEmails").Value = String.Join(";", logEmail.EmailContactsCopyLists);
            iRetCode = oLogEMail.Add();

            if (iRetCode != 0)
            {
                string sErrMessage = oCompany.GetLastErrorDescription();
                Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oLogEMail);
            oLogEMail = null;
        }
        #endregion

        #region Contar Registros por Mês
        public int ContarRegistroMes()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(-30);

            int contarRegistroMes = 0;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"Code\") \"Count\" FROM \"@P1_LOGEMAIL\" ");
            SQL.AppendLine("WHERE \"U_P1_DueDate\" BETWEEN '" + fim.ToString("yyyyMMdd") + "' AND '" + inicio.ToString("yyyyMMdd") + "' ");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                contarRegistroMes = int.Parse(oRecordset.Fields.Item("Count").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return contarRegistroMes;
        }
        #endregion

        #region list
        public List<LogEmail> List()
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<LogEmail> listLogModel = new List<LogEmail>();

            StringBuilder SQL = new StringBuilder(); 
            SQL.AppendLine("SELECT * FROM \"@P1_LOGEMAIL\" ORDER BY CAST (\"Code\" AS BIGINT) DESC");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                //T0.[U_CardCode]
                listLogModel.Add(new LogEmail()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    BillCode = oRecordset.Fields.Item("U_P1_CRCode").Value.ToString(),
                    DueDate = (DateTime)oRecordset.Fields.Item("U_P1_DueDate").Value,
                    BillTotal = double.Parse(oRecordset.Fields.Item("U_P1_CrTotal").Value.ToString(), CultureInfo.InvariantCulture),
                    TypeId = oRecordset.Fields.Item("U_P1_TpMailCode").Value.ToString(),
                    MailModelId = oRecordset.Fields.Item("U_P1_ModelMail").Value.ToString(),
                    SendingDate = (DateTime)oRecordset.Fields.Item("U_P1_Date").Value,
                    EmailAdress = oRecordset.Fields.Item("U_P1_EmlAdress").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_EmailTxt").Value.ToString(),
                    Status = oRecordset.Fields.Item("U_P1_Status").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("U_P1_CardCode").Value.ToString(),
                    EmailContactsCopyLists = oRecordset.Fields.Item("U_P1_ListEmails").Value.ToString().Split(';')
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listLogModel;
        }
        #endregion

        #region Últimos 20 registros
        public List<LogEmail> Top20List()
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<LogEmail> listLogModel = new List<LogEmail>();

            StringBuilder SQL = new StringBuilder();
            if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
            {
                SQL.AppendLine("SELECT * FROM \"@P1_LOGEMAIL\" ORDER BY CAST(\"Code\" AS NUMERIC) DESC LIMIT 20");
            }
            else
            {
                SQL.AppendLine("SELECT TOP 20 * FROM \"@P1_LOGEMAIL\" ORDER BY CONVERT ( BIGINT ,\"Code\") DESC");
            }

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                listLogModel.Add(new LogEmail()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    BillCode = oRecordset.Fields.Item("U_P1_CRCode").Value.ToString(),
                    DueDate = (DateTime)oRecordset.Fields.Item("U_P1_DueDate").Value,
                    BillTotal = double.Parse(oRecordset.Fields.Item("U_P1_CrTotal").Value.ToString(), CultureInfo.InvariantCulture),
                    TypeId = oRecordset.Fields.Item("U_P1_TpMailCode").Value.ToString(),
                    MailModelId = oRecordset.Fields.Item("U_P1_ModelMail").Value.ToString(),
                    SendingDate = (DateTime)oRecordset.Fields.Item("U_P1_Date").Value,
                    EmailAdress = oRecordset.Fields.Item("U_P1_EmlAdress").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_EmailTxt").Value.ToString(),
                    Status = oRecordset.Fields.Item("U_P1_Status").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("U_P1_CardCode").Value.ToString(),
                    EmailContactsCopyLists = oRecordset.Fields.Item("U_P1_ListEmails").Value.ToString().Split(';')
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listLogModel;
        }
        #endregion

        #region Pesquisar Log de Emails
        public List<LogEmail> SearchList(string TitleCode, string CardCode, string DueDateFrom, string DueDateTo, double TotalFrom, double TotalTo)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<LogEmail> listLogModel = new List<LogEmail>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_LOGEMAIL\" ");
            SQL.AppendLine("WHERE \"U_P1_CRCode\" like '%" + TitleCode + "%' AND ");
            SQL.AppendLine("\"U_P1_CardCode\" like '%" + CardCode + "%' AND ");

            if (!String.IsNullOrEmpty(DueDateFrom))
            {
                SQL.AppendLine("\"U_P1_DueDate\" >= '" + (DateTime.ParseExact(DueDateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "'AND ");
            }

            if (!String.IsNullOrEmpty(DueDateTo))
            {
                SQL.AppendLine("\"U_P1_DueDate\" <= '" + (DateTime.ParseExact(DueDateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' AND ");
            }

            if (TotalFrom != 0)
            {
                SQL.AppendLine("\"U_P1_CrTotal\" >= " + TotalFrom + " AND ");
            }

            if (TotalTo != 0)
            {
                SQL.AppendLine("\"U_P1_CrTotal\" <= " + TotalTo + " AND ");
            }

            //Verificar se há algum 'AND' no final da string SQL e remover para não dar erro na consulta
            string teste = (SQL.ToString()).Substring(SQL.Length - 6);
            if ((SQL.ToString()).Substring(SQL.Length - 6) == "AND \r\n")
            {
                StringBuilder aux = new StringBuilder(SQL.ToString().Substring(0, SQL.Length - 6));
                SQL = aux;
            }

            if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
            {
                SQL.AppendLine(" ORDER BY CAST(\"Code\" AS NUMERIC) DESC ");
            }
            else
            {
                SQL.AppendLine(" ORDER BY CONVERT ( BIGINT ,\"Code\") DESC ");
            }
                

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                //T0.[U_CardCode]
                listLogModel.Add(new LogEmail()
                {
                    Code = oRecordset.Fields.Item("Code").Value.ToString(),
                    BillCode = oRecordset.Fields.Item("U_P1_CRCode").Value.ToString(),
                    DueDate = (DateTime)oRecordset.Fields.Item("U_P1_DueDate").Value,
                    BillTotal = double.Parse(oRecordset.Fields.Item("U_P1_CrTotal").Value.ToString(), CultureInfo.InvariantCulture),
                    TypeId = oRecordset.Fields.Item("U_P1_TpMailCode").Value.ToString(),
                    MailModelId = oRecordset.Fields.Item("U_P1_ModelMail").Value.ToString(),
                    SendingDate = (DateTime)oRecordset.Fields.Item("U_P1_Date").Value,
                    EmailAdress = oRecordset.Fields.Item("U_P1_EmlAdress").Value.ToString(),
                    Subject = oRecordset.Fields.Item("U_P1_Subject").Value.ToString(),
                    Message = oRecordset.Fields.Item("U_P1_EmailTxt").Value.ToString(),
                    Status = oRecordset.Fields.Item("U_P1_Status").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("U_P1_CardCode").Value.ToString(),
                    EmailContactsCopyLists = oRecordset.Fields.Item("U_P1_ListEmails").Value.ToString().Split(';')
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listLogModel;
        }
        #endregion

        #region Pegar Último Código
        public string GetLastCode(string sTabela)
        {

            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string sCode = "";

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Max(CAST(\"Code\" as NUMERIC)) as \"Code\" ");
            SQL.AppendLine("FROM " + "\"" + sTabela + "\"");

            //Realiza a consulta
            oRecordSet.DoQuery(SQL.ToString());

            if (oRecordSet.RecordCount > 0)
            {
                oRecordSet.MoveLast();
                while (!oRecordSet.EoF)
                {
                    sCode = oRecordSet.Fields.Item("Code").Value.ToString();
                    if (sCode == "")
                    {
                        sCode = "1";
                    }
                    else
                    {
                        sCode = (Convert.ToInt32(sCode) + 1).ToString();
                    }
                    oRecordSet.MoveNext();
                }
            }
            else
            {
                sCode = "1";
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return sCode;
        }
        #endregion
    }
}