﻿using PowerOne.Database;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Util;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerOne.Services.AssistenteCobranca
{
    public class ContactEmployeeService
    {
        #region Listar contatos dos funcionários
        public List<ContactEmployee> List(string CardCode)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<ContactEmployee> contactEmployees = new List<ContactEmployee>();

            int iRetcode = -1;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"Name\",\"CardCode\", CONCAT(CONCAT(CONCAT(CONCAT(\"FirstName\", ' '), \"MiddleName\"),' '), \"LastName\") as \"FullName\", \"Position\", \"E_MailL\",\"U_P1_UseForMail\" FROM OCPR ");
            sql.AppendLine("WHERE \"CardCode\" = '" + CardCode + "'");

            oRecordset.DoQuery(sql.ToString());

            while (!oRecordset.EoF)
            {
                contactEmployees.Add(new ContactEmployee()
                {
                    Id = oRecordset.Fields.Item("Name").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    FullName = oRecordset.Fields.Item("FullName").Value.ToString(),
                    Position = oRecordset.Fields.Item("Position").Value.ToString(),
                    Email = oRecordset.Fields.Item("E_MailL").Value.ToString(),
                    UseForSendingEmail = oRecordset.Fields.Item("U_P1_UseForMail").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return contactEmployees;
        }
        #endregion

        #region Listar funcionários ativos
        public List<ContactEmployee> ListAllActivated(string CardCode)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<ContactEmployee> contactEmployees = new List<ContactEmployee>();

            int iRetcode = -1;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT \"Name\",\"CardCode\", CONCAT(CONCAT(CONCAT(CONCAT(\"FirstName\", ' '), \"MiddleName\"),' '), \"LastName\") as \"FullName\", \"Position\", \"E_MailL\",\"U_P1_UseForMail\" FROM OCPR ");
            sql.AppendLine("WHERE \"CardCode\" = '" + CardCode + "' AND \"U_UseForMail\" = 'Y'");

            oRecordset.DoQuery(sql.ToString());

            while (!oRecordset.EoF)
            {
                contactEmployees.Add(new ContactEmployee()
                {
                    Id = oRecordset.Fields.Item("Name").Value.ToString(),
                    CardCode = oRecordset.Fields.Item("CardCode").Value.ToString(),
                    FullName = oRecordset.Fields.Item("FullName").Value.ToString(),
                    Position = oRecordset.Fields.Item("Position").Value.ToString(),
                    Email = oRecordset.Fields.Item("E_MailL").Value.ToString(),
                    UseForSendingEmail = oRecordset.Fields.Item("U_P1_UseForMail").Value.ToString()
                });
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return contactEmployees;
        }
        #endregion

        #region SetForUse
        public void SetForUse(string[] codes, string CardCode)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.BusinessPartners oBusinessPartner = (SAPbobsCOM.BusinessPartners)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            
            //pegando a lista de todos os contatos
            List<ContactEmployee> allContacts = List(CardCode);

            int iRetCode = -1;

            if (oBusinessPartner.GetByKey(CardCode))
            {
                SAPbobsCOM.ContactEmployees oContact = oBusinessPartner.ContactEmployees;

                for (int i = 0; i < oContact.Count; i++)
                {
                    oContact.SetCurrentLine(i);
                    if (codes != null)
                    {
                        //Verificando se o contato está na lista dos que vão ser utilizados
                        if (codes.Contains(oContact.Name))
                        {
                            oContact.UserFields.Fields.Item("U_P1_UseForMail").Value = "Y";
                        }
                        else
                        {
                            oContact.UserFields.Fields.Item("U_P1_UseForMail").Value = "N";
                        }
                    }
                    else//Se for vazio, todos os contatos serão desativados
                    {
                        oContact.UserFields.Fields.Item("U_P1_UseForMail").Value = "N";
                    }

                }
                iRetCode = oBusinessPartner.Update();

                if (iRetCode != 0)
                {
                    string sErrMsg = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMsg, Log.TipoLog.Erro);
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oBusinessPartner);
                oBusinessPartner = null;
            }
        }
        #endregion
    }
}