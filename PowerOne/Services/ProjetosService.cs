﻿using PowerOne.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PowerOne.Models;


namespace PowerOne.Services
{
    public class ProjetosService
    {
        #region List
        public List<Projetos> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Projetos> list = new List<Projetos>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"PrjCode\",\"PrjName\" from OPRJ where \"Active\" =  'Y'");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Projetos item = new Projetos();
                item.PrjCode = oRecordset.Fields.Item("PrjCode").Value.ToString();
                item.PrjName = oRecordset.Fields.Item("PrjName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}