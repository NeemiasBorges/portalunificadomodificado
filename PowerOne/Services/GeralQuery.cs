﻿using PowerOne.Database;
using System.Text;


namespace PowerOne.Services
{
    public class GeralQuery
    {

        #region verificar existencia de campos
        public bool VerifyField(string sCampo, string sTabela)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset;
            oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string sql;
            bool bExiste = false;

            StringBuilder SQL = new StringBuilder();
            SQL.Append("SELECT \"FieldID\" FROM CUFD WHERE \"TableID\"='" + sTabela + "' AND \"AliasID\"='" + sCampo + "'");

            //Realiza a consulta
            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                bExiste = true;
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bExiste;
        }
        #endregion

        #region verificar existencia de tabelas
        public bool VerifyTable(string sTabela)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset;
            oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            bool bExiste = false;

            StringBuilder SQL = new StringBuilder();
            SQL.Append("SELECT \"TableName\" FROM OUTB WHERE \"TableName\"='" + sTabela + "'");

            oRecordset.DoQuery(SQL.ToString());

            if (oRecordset.RecordCount > 0)
            {
                oRecordset.MoveFirst();
                while (!oRecordset.EoF)
                {
                    bExiste = true;
                    oRecordset.MoveNext();
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bExiste;
        }
        #endregion

        #region Verificar autorização do usuário
        public string VerificarAutorizacao(string sEmpId, string sMenu)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string sAutorizacao = "N";
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT \"" + sMenu + "\" as \"P1_Autorizacao\" FROM \"@P1_ACCESS\" ");
            SQL.AppendLine("WHERE \"U_EB_EMPId\" = '" + sEmpId + "' ");

            oRecordSet.DoQuery(SQL.ToString());

            if (oRecordSet.RecordCount > 0)
            {
                sAutorizacao = oRecordSet.Fields.Item("P1_Autorizacao").Value.ToString();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return sAutorizacao;
        }
        #endregion
    }
}