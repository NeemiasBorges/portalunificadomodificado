﻿using PowerOne.Database;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Services
{
    public class ItemEstoqueService
    {
        #region List
        public List<ItemEstoque> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<ItemEstoque> list = new List<ItemEstoque>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"ItemCode\",\"ItemName\",\"InvntryUom\" FROM OITM");
            SQL.AppendLine("ORDER BY \"ItemName\" ASC");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                ItemEstoque item = new ItemEstoque();
                item.ItemCode = oRecordset.Fields.Item("ItemCode").Value.ToString();
                item.ItemName = oRecordset.Fields.Item("ItemName").Value.ToString();
                item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion

        #region Pesquisar item
        public List<ItemEstoque> Search(string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<ItemEstoque> list = new List<ItemEstoque>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"ItemCode\",\"ItemName\",\"InvntryUom\" FROM OITM");
            SQL.AppendLine("WHERE \"ItemName\" like '%" + name +"%'");
            SQL.AppendLine("ORDER BY \"ItemName\" ASC");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                ItemEstoque item = new ItemEstoque();
                item.ItemCode = oRecordset.Fields.Item("ItemCode").Value.ToString();
                item.ItemName = oRecordset.Fields.Item("ItemName").Value.ToString();
                item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}
