﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.Reembolso;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using PowerOne.Util;

namespace PowerOne.Services.Reembolso
{
    public class SolicitacaoReembolsoService
    {
        #region Serviços
        AnexosService _anexoService = new AnexosService();
        #endregion

        #region Pegar Solicitacao Específico
        public SolicitacaoReembolso GetByKey(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            SolicitacaoReembolso solicitacaoReembolso = new SolicitacaoReembolso();

            StringBuilder SQL = new StringBuilder();


            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"CardCode\", T0.\"CardName\", T0.\"DocNum\",T0.\"DocStatus\", ");
            SQL.AppendLine("T0.\"ReqDate\",T0.\"DocDate\",T0.\"ReqName\", T0.\"Comments\",T0.\"DocDueDate\",T0.\"AtcEntry\" ");
            SQL.AppendLine("FROM OPOR T0");

            SQL.AppendLine("WHERE T0.\"DocEntry\" = " + DocEntry);

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                solicitacaoReembolso.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                solicitacaoReembolso.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                solicitacaoReembolso.DocDate = DateTime.Parse(oRecordSet.Fields.Item("DocDate").Value.ToString());
                solicitacaoReembolso.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
                solicitacaoReembolso.CardName = oRecordSet.Fields.Item("CardName").Value.ToString();
                solicitacaoReembolso.CardCode = oRecordSet.Fields.Item("CardCode").Value.ToString();
                solicitacaoReembolso.Requisitante = oRecordSet.Fields.Item("ReqName").Value.ToString();
                solicitacaoReembolso.Comments = oRecordSet.Fields.Item("Comments").Value.ToString();
                solicitacaoReembolso.idAnexos = int.Parse(oRecordSet.Fields.Item("AtcEntry").Value.ToString());

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            solicitacaoReembolso.Itens = ListarLinhas(DocEntry, null);

            return solicitacaoReembolso;
        }

        #endregion

        #region Listar linhas reembolso
        public List<SolicitacaoReembolsoLinhas> ListarLinhas(int DocEntry, string[] linhas)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<SolicitacaoReembolsoLinhas> reembolsoLinhas = new List<SolicitacaoReembolsoLinhas>();

            StringBuilder SQL = new StringBuilder();


            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", case when T0.\"DocStatus\" = " + " ");
            SQL.AppendLine("'O' then 'Aberta' when T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", ");
            SQL.AppendLine("T1.\"DocDate\", T1.\"VisOrder\",T1.\"ItemCode\",T2.\"ItemName\",T1.\"Quantity\", ");
            SQL.AppendLine("T1.\"LineStatus\",T1.\"AgrNo\",T1.\"Project\",coalesce(T1.\"TrgetEntry\",0), t1.\"FreeTxt\", ");
            SQL.AppendLine("T1.\"OcrCode\", T1.\"OcrCode2\", T1.\"OcrCode3\", T1.\"OcrCode4\", T1.\"OcrCode5\",");
            SQL.AppendLine("C1.\"OcrName\", C2.\"OcrName\" \"OcrName2\", C3.\"OcrName\" \"OcrName3\", C4.\"OcrName\" \"OcrName4\", C5.\"OcrName\" \"OcrName5\" ");
            SQL.AppendLine("FROM OPOR T0 inner Join POR1 T1 on T0.\"DocEntry\" = T1.\"DocEntry\" ");
            SQL.AppendLine("inner Join oitm T2 on T2.\"ItemCode\" = T1.\"ItemCode\" ");
            SQL.AppendLine("left join OOCR C1 on T1.\"OcrCode\" = C1.OcrCode ");
            SQL.AppendLine("left join OOCR C2 on T1.\"OcrCode2\" = C2.OcrCode ");
            SQL.AppendLine("left join OOCR C3 on T1.\"OcrCode3\" = C3.OcrCode ");
            SQL.AppendLine("left join OOCR C4 on T1.\"OcrCode4\" = C4.OcrCode ");
            SQL.AppendLine("left join OOCR C5 on T1.\"OcrCode5\" = C5.OcrCode ");
            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "' ");
            //SQL.AppendLine("ON T2.\"ItemCode\" = T1.\"ItemCode\"  WHERE T0.\"DocEntry\" = '" + DocEntry + "' ");


            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                reembolsoLinhas.Add(new SolicitacaoReembolsoLinhas()
                {
                    DocEntry = int.Parse(oRecordset.Fields.Item("DocEntry").Value.ToString()),
                    DocNum = int.Parse(oRecordset.Fields.Item("DocNum").Value.ToString()),
                    DocDate = DateTime.Parse(oRecordset.Fields.Item("DocDate").Value.ToString()),
                    DocStatus = oRecordset.Fields.Item("DocStatus").Value.ToString(),
                    VisOrder = int.Parse(oRecordset.Fields.Item("VisOrder").Value.ToString()),
                    Projeto = oRecordset.Fields.Item("Project").Value.ToString(),
                    ItemCode = oRecordset.Fields.Item("ItemCode").Value.ToString(),
                    FreeTxt = oRecordset.Fields.Item("FreeTxt").Value.ToString(),
                    ItemName = oRecordset.Fields.Item("ItemName").Value.ToString(),

                    OcrCode = oRecordset.Fields.Item("OcrCode").Value.ToString(),
                    OcrName = oRecordset.Fields.Item("OcrName").Value.ToString(),
                    OcrCode2 = oRecordset.Fields.Item("OcrCode2").Value.ToString(),
                    OcrName2 = oRecordset.Fields.Item("OcrName2").Value.ToString(),
                    OcrCode3 = oRecordset.Fields.Item("OcrCode3").Value.ToString(),
                    OcrName3 = oRecordset.Fields.Item("OcrName3").Value.ToString(),
                    OcrCode4 = oRecordset.Fields.Item("OcrCode4").Value.ToString(),
                    OcrName4 = oRecordset.Fields.Item("OcrName4").Value.ToString(),
                    OcrCode5 = oRecordset.Fields.Item("OcrCode5").Value.ToString(),
                    OcrName5 = oRecordset.Fields.Item("OcrName5").Value.ToString(),

                    AgrNo = oRecordset.Fields.Item("AgrNo").Value.ToString(),
                    Quantity = int.Parse(oRecordset.Fields.Item("Quantity").Value.ToString()),
                });
                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return reembolsoLinhas;
        }


        #endregion

        #region select geral e pesquisa
        public List<SolicitacaoReembolso> SearchList(Login login, string DataInicio, string DataFim, string Status)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<SolicitacaoReembolso> listSolicitacaoCompras = new List<SolicitacaoReembolso>();

            StringBuilder SQL = new StringBuilder();

            if (login.Autorizacoes.Reembolso == "T" || login.Autorizacoes.Grupo.Reembolso == "T")
            {
                if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"ReqName\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' '");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", '");
                    SQL.AppendLine("T0.\"TaxDate\",  STRING_AGG(Tc1.\"FileName\", ' / ' order by Tc1.\"FileName\") \"Anexos\", T0.\"AtcEntry\" '");
                    SQL.AppendLine(" FROM OPOR T0  left JOIN atc1 Tc1 ON T0.\"AtcEntry\" = Tc1.\"AbsEntry\" ");
                    SQL.AppendLine("WHERE 1 = 1 ");
                }
                else
                {
                    SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"ReqName\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' ");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\",");
                    SQL.AppendLine("T0.\"TaxDate\",  STUFF(Tc1.\"FileName\", 1,1,' / ') \"Anexos\", T0.\"AtcEntry\"  ");
                    SQL.AppendLine(" FROM OPOR T0  left JOIN atc1 Tc1 ON T0.\"AtcEntry\" = Tc1.\"AbsEntry\" ");
                    SQL.AppendLine("WHERE 1 = 1 ");
                    SQL.AppendLine("AND (\"U_P1_TYPE\" = 'R') ");
                }
            }
            else
            {
                if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"ReqName\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' '");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\", '");
                    SQL.AppendLine("T0.\"TaxDate\",  STRING_AGG(Tc1.\"FileName\", ' / ' order by Tc1.\"FileName\") \"Anexos\", T0.\"AtcEntry\" '");
                    SQL.AppendLine(" FROM OPOR T0  left JOIN atc1 Tc1 ON T0.\"AtcEntry\" = Tc1.\"AbsEntry\" ");
                    SQL.AppendLine("WHERE 1 = 1 AND ");
                }
                else
                {
                    SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"ReqName\", T0.\"DocNum\", case when T0.\"DocStatus\" = 'O' then 'Aberta' ");
                    SQL.AppendLine("WHEN T0.\"CANCELED\" = 'Y' then 'Cancelada' else 'Fechada' end \"DocStatus\", T0.\"DocDate\",");
                    SQL.AppendLine("T0.\"TaxDate\",  STUFF(Tc1.\"FileName\", 1,1,' / ') \"Anexos\", T0.\"AtcEntry\"  ");
                    SQL.AppendLine(" FROM OPOR T0  left JOIN atc1 Tc1 ON T0.\"AtcEntry\" = Tc1.\"AbsEntry\" ");
                    SQL.AppendLine("WHERE (\"U_P1_TYPE\" = 'R') ");
                }

                //autorização restrita
                SQL.AppendLine("AND (T0.\"U_P1_OWNER\" = '" + login.Id + "') ");
                //SQL.AppendLine("AND (T0.\"OwnerCode\" = '" + login.UserId + "' ");
                //SQL.AppendLine("OR T0.\"UserSign\" = '" + login.UserId + "') ");
            }

            //pesquisas

            if (!String.IsNullOrEmpty(DataInicio))
            {
                SQL.AppendLine(" AND T0.\"DocDate\" >= '" + (DateTime.ParseExact(DataInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "'");
            }

            if (!String.IsNullOrEmpty(DataFim))
            {
                SQL.AppendLine(" AND T0.\"DocDate\" <='" + (DateTime.ParseExact(DataFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "'");
            }

            if (Status != "T")
            {
                SQL.AppendLine(" and T0.\"DocStatus\" = '" + Status + "' ");
            }

            // ordena apartir do último documento
            SQL.AppendLine(" ORDER BY T0.\"DocEntry\" DESC ");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                listSolicitacaoCompras.Add(new SolicitacaoReembolso()
                {
                    DocEntry = int.Parse(oRecordset.Fields.Item("DocEntry").Value.ToString()),
                    Requisitante = oRecordset.Fields.Item("ReqName").Value.ToString(),
                    DocNum = int.Parse(oRecordset.Fields.Item("DocNum").Value.ToString()),
                    DocStatus = oRecordset.Fields.Item("DocStatus").Value.ToString(),
                    DocDate = DateTime.Parse(oRecordset.Fields.Item("DocDate").Value.ToString()),
                    TaxDate = oRecordset.Fields.Item("TaxDate").Value.ToString(),
                    idAnexos = int.Parse(oRecordset.Fields.Item("AtcEntry").Value.ToString())
                });
                oRecordset.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return listSolicitacaoCompras;
        }

        #endregion + pesquisa

        #region Create
        public string Create(SolicitacaoReembolso reembolso, Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oReembolso;
                oReembolso = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                oReembolso.DocDate = DateTime.Now;
                oReembolso.BPL_IDAssignedToInvoice = reembolso.BPL_IDAssignedToInvoice;
                //oReembolso.RequesterName = login.User;
                //oReembolso.DocumentsOwner = login.UserId;
                oReembolso.CardCode = reembolso.CardCode;
                oReembolso.CardName = reembolso.CardName;
                oReembolso.Comments = reembolso.Comments;

                oReembolso.UserFields.Fields.Item("U_P1_OWNER").Value = login.Id;
                oReembolso.UserFields.Fields.Item("U_P1_TYPE").Value = "R";

                foreach (var item in reembolso.Itens)
                {
                    oReembolso.Lines.ItemCode = item.ItemCode;
                    oReembolso.Lines.ItemDescription = item.ItemDesc;
                    oReembolso.Lines.ShipDate = item.ShipDate;
                    oReembolso.Lines.Quantity = item.Quantity;
                    oReembolso.Lines.UnitPrice = item.PriceBefDi;

                    oReembolso.Lines.CostingCode = item.OcrCode;
                    oReembolso.Lines.CostingCode2 = item.OcrCode2;
                    oReembolso.Lines.CostingCode3 = item.OcrCode3;
                    oReembolso.Lines.CostingCode4 = item.OcrCode4;
                    oReembolso.Lines.CostingCode5 = item.OcrCode5;

                    oReembolso.Lines.WarehouseCode = item.WhsCode;
                    oReembolso.Lines.MeasureUnit = item.BuyUnitMsr;
                    oReembolso.Lines.ProjectCode = item.Projeto;
                    oReembolso.Lines.FreeText = item.FreeTxt;
                    //oReembolso.Lines.TaxCode = "Reembols";

                    oReembolso.Lines.Add();
                }

                int iRetCode = oReembolso.Add();

                if (iRetCode != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
                else
                {
                    int iDocEntry = int.Parse(oCompany.GetNewObjectKey());
                    msg = _anexoService.InserirAnexos(reembolso.ListaAnexos, iDocEntry, Models.Enums.TipoDocumento.PedidoCompra);
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }

        #endregion

        #region Add Anexo 

        public bool AddAnexo(string observacoes, int DocEntry, Login login)
        {

            return true;
        }


        #endregion

        #region Deletar Linha Reembolso
        public string DeletarLinha(int DocEntry, int LineId)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                if (oPedidoCompra.GetByKey(DocEntry))
                {
                    oPedidoCompra.Lines.SetCurrentLine(LineId);

                    oPedidoCompra.Lines.Delete();

                    if (oPedidoCompra.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oPedidoCompra);
                oPedidoCompra = null;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Fechar Status do Pedido
        public string FecharStatus(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msg = null;

            try
            {
                SAPbobsCOM.Documents oPedidoCompra = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                if (oPedidoCompra.GetByKey(DocEntry))
                {
                    int iRet = oPedidoCompra.Close();

                    if (iRet != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                        msg += " O Status Já está Fechado";
                    }
                }
                else
                {
                    msg = "Documento inexistente!";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion
    }
}