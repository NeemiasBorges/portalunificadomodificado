﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace PowerOne.Services
{
    public class ListBPService
    {
        #region List
        public List<ListBP> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<ListBP> list = new List<ListBP>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"CardCode\", \"CardName\" FROM OCRD");
            SQL.AppendLine("WHERE CardType = 'S'");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                ListBP bp = new ListBP();
                bp.CardCode = oRecordset.Fields.Item("CardCode").Value.ToString();
                bp.CardName = oRecordset.Fields.Item("CardName").Value.ToString();

                list.Add(bp);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}