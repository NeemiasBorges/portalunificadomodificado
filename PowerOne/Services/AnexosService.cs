﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.Compras;
using PowerOne.Models.Enums;
using PowerOne.Util;

namespace PowerOne.Services
{
    public class AnexosService
    {
        #region Inserir Anexos (Arquivos, DocEntry)
        public string InserirAnexos(List<Arquivo> arquivos, int DocEntry, TipoDocumento tipoDocumentoMarketing)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Documents doc;
            if (tipoDocumentoMarketing == TipoDocumento.SolicitacaoCompra)
            {
                doc = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);
            }
            else //Reembolso e Pedido de Compra utilizam o mesmo documento no SAP Business One
            {
                doc = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);
            }


            string msg = null;

            try
            {
                doc.GetByKey(DocEntry);
                SAPbobsCOM.Attachments2 oATT = (SAPbobsCOM.Attachments2)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2);

                if (arquivos != null)
                {
                    foreach (var arquivo in arquivos)
                    {
                        oATT.Lines.Add();
                        oATT.Lines.FileName = arquivo.Nome;
                        oATT.Lines.FileExtension = arquivo.Extensao;
                        oATT.Lines.SourcePath = System.IO.Path.GetDirectoryName(arquivo.Diretorio);
                        oATT.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES;

                        int iAttEntry = -1;
                        if (oATT.Add() == 0)
                        {
                            iAttEntry = int.Parse(oCompany.GetNewObjectKey());
                            doc.AttachmentEntry = iAttEntry;

                            if (doc.Update() != 0)
                            {
                                msg = oCompany.GetLastErrorDescription();
                            }
                        }
                        else
                        {
                            msg = oCompany.GetLastErrorDescription();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Editar Documentos adicionando novos Anexos (Arquivos, DocEntry, IdAnexo)
        public string EditarAdicionandoAnexos(List<Arquivo> arquivos, int DocEntry, int idAnexo, TipoDocumento tipoDocumentoMarketing)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Documents doc;
            if (tipoDocumentoMarketing == TipoDocumento.SolicitacaoCompra)
            {
                doc = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest);
            }
            else //Reembolso e Pedido de Compra utilizam o mesmo documento no SAP Business One
            {
                doc = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);
            }


            string msg = null;

            try
            {
                doc.GetByKey(DocEntry);
                SAPbobsCOM.Attachments2 oATT = (SAPbobsCOM.Attachments2)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2);

                if (oATT.GetByKey(idAnexo))//Pegando o objeto de anexo
                {
                    foreach (var arquivo in arquivos)
                    {
                        oATT.Lines.Add();
                        oATT.Lines.FileName = arquivo.Nome;
                        oATT.Lines.FileExtension = arquivo.Extensao;
                        oATT.Lines.SourcePath = System.IO.Path.GetDirectoryName(arquivo.Diretorio);
                        oATT.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES;

                        int iAttEntry = -1;
                        if (oATT.Update() == 0)
                        {
                            iAttEntry = int.Parse(oCompany.GetNewObjectKey());
                            doc.AttachmentEntry = iAttEntry;

                            if (doc.Update() != 0)
                            {
                                msg = oCompany.GetLastErrorDescription();
                            }
                        }
                        else
                        {
                            msg = oCompany.GetLastErrorDescription();
                        }
                    }
                }
                else
                {
                    msg = "Erro ao adicionar anexo.";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Anexos
        public List<Arquivo> GetAnexo(int DocEntry, TipoDocumento TipoDocumento)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Arquivo> anexos = new List<Arquivo>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Tc1.\"trgtPath\", Tc1.\"FileName\", Tc1.\"FileExt\" FROM");

            if (TipoDocumento == TipoDocumento.SolicitacaoCompra)
            {
                SQL.AppendLine("OPRQ T0");
            }
            else 
            {
                SQL.AppendLine("OPOR T0");
            }
            
            
            SQL.AppendLine("LEFT JOIN atc1 Tc1 ON T0.\"AtcEntry\" = Tc1.\"AbsEntry\"");
            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "'");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Arquivo anexo = new Arquivo();
                anexo.Diretorio = oRecordset.Fields.Item("trgtPath").Value.ToString();
                anexo.Nome = oRecordset.Fields.Item("FileName").Value.ToString();
                anexo.Extensao = oRecordset.Fields.Item("FileExt").Value.ToString();

                anexos.Add(anexo);
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return anexos;
        }

        #endregion
    }
}