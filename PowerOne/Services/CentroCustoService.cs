﻿using PowerOne.Database;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Services
{
    public class CentroCustoService
    {
        #region List Centros de Custo
        public List<CentroCusto> List(int DimCode)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<CentroCusto> list = new List<CentroCusto>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"OcrCode\",\"OcrName\" FROM OOCR WHERE \"DimCode\" =" + DimCode + "");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                CentroCusto ctCusto = new CentroCusto();
                ctCusto.OcrCode = oRecordset.Fields.Item("OcrCode").Value.ToString();
                ctCusto.OcrName = oRecordset.Fields.Item("OcrName").Value.ToString();

                list.Add(ctCusto);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}