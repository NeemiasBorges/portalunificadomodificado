﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Services
{
    public class AutorizacoesService
    {
        #region Serviços 
        LoginService _loginService = new LoginService();
        #endregion

        #region Criar Autorizações para Todos os Usuários
        public string CriarAutorizacoes()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_Access");

            string msg = null;

            List<Login> idusers = _loginService.GetAllIdUsers();

            try
            {
                foreach (Login user in idusers)
                {
                    if (!oAutorizacoes.GetByKey(user.Id.ToString()))
                    {
                        oAutorizacoes.Code = user.Id.ToString();
                        oAutorizacoes.Name = user.User;
                        oAutorizacoes.UserFields.Fields.Item("U_P1_EMPId").Value = user.Id.ToString();
                        if (oAutorizacoes.Add() != 0)
                        {
                            msg = oCompany.GetLastErrorDescription();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);


            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Criar Autorizacao 
        public string CriarAutorizacoes(Login user)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_Access");

            string msg = null;

            if (!oAutorizacoes.GetByKey(user.Id.ToString()))
            {
                oAutorizacoes.Code = user.Id.ToString();
                oAutorizacoes.Name = user.User;
                oAutorizacoes.UserFields.Fields.Item("U_P1_EMPId").Value = user.Id.ToString();
                if (oAutorizacoes.Add() != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pesquisar Autorizações do Usuário
        public Autorizacoes PegarAutorizacoes(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_ACCESS\"");
            SQL.AppendLine("WHERE \"Code\" = '" + code + "'");
            Autorizacoes autorizacao = new Autorizacoes();

            oRecordSet.DoQuery(SQL.ToString());
            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                autorizacao.UserId = int.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                autorizacao.UserName = oRecordSet.Fields.Item("Name").Value.ToString();
                autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
                autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
                autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
                autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
                autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
                autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
                autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
                autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
                autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
                autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
                autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
                autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
                autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
                autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
                autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
                autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();
                autorizacao.IdGrupo = oRecordSet.Fields.Item("U_P1_Grupo").Value.ToString();
                autorizacao.Tipo = oRecordSet.Fields.Item("U_P1_Type").Value.ToString();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return autorizacao;
        }
        #endregion

        #region Pegar Autorizações
        public List<Autorizacoes> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            List<Autorizacoes> autorizacoes = new List<Autorizacoes>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_ACCESS\"");

            oRecordSet.DoQuery(SQL.ToString());
            while (!oRecordSet.EoF)
            {
                Autorizacoes autorizacao = new Autorizacoes();
                autorizacao.UserId = int.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                autorizacao.UserName = oRecordSet.Fields.Item("Name").Value.ToString();
                autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
                autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
                autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
                autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
                autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
                autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
                autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
                autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
                autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
                autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
                autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
                autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
                autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
                autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
                autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
                autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();
                autorizacao.IdGrupo = oRecordSet.Fields.Item("U_P1_Grupo").Value.ToString();
                autorizacao.Tipo = oRecordSet.Fields.Item("U_P1_Type").Value.ToString();

                autorizacoes.Add(autorizacao);

                oRecordSet.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return autorizacoes;
        }
        #endregion

        #region Pegar Autorizações do Grupo
        public List<AutorizacoesGrupo> ListGrupo()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            List<AutorizacoesGrupo> autorizacoes = new List<AutorizacoesGrupo>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_AccGrp\"");

            oRecordSet.DoQuery(SQL.ToString());
            while (!oRecordSet.EoF)
            {
                AutorizacoesGrupo autorizacao = new AutorizacoesGrupo();
                autorizacao.Code = oRecordSet.Fields.Item("Code").Value.ToString();
                autorizacao.Name = oRecordSet.Fields.Item("Name").Value.ToString();
                autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
                autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
                autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
                autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
                autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
                autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
                autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
                autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
                autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
                autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
                autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
                autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
                autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
                autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
                autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
                autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();

                autorizacoes.Add(autorizacao);

                oRecordSet.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return autorizacoes;
        }
        #endregion

        #region Atualizar Autorizações
        public string Update(Autorizacoes autorizacao)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_Access");

            string msg = null;
            if (oAutorizacoes.GetByKey(autorizacao.UserId.ToString()))
            {
                oAutorizacoes.UserFields.Fields.Item("U_P1_ConsSol").Value = autorizacao.SolCompra;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewSol").Value = autorizacao.CriarSolCompra;
                oAutorizacoes.UserFields.Fields.Item("U_P1_PedComp").Value = autorizacao.PedCompra;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewPedC").Value = autorizacao.CriarPedCompra;
                oAutorizacoes.UserFields.Fields.Item("U_P1_ReqS").Value = autorizacao.ReqEstoque;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewReqS").Value = autorizacao.CriarReqEstoque;
                oAutorizacoes.UserFields.Fields.Item("U_P1_Reemb").Value = autorizacao.Reembolso;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewReem").Value = autorizacao.CriarReembolso;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewMailM").Value = autorizacao.CriarModeloEmail;
                oAutorizacoes.UserFields.Fields.Item("U_P1_MailMod").Value = autorizacao.ModeloEmail;
                oAutorizacoes.UserFields.Fields.Item("U_P1_AstEnvio").Value = autorizacao.AssistEnvio;
                oAutorizacoes.UserFields.Fields.Item("U_P1_Client").Value = autorizacao.Clientes;
                oAutorizacoes.UserFields.Fields.Item("U_P1_NewGPCnt").Value = autorizacao.CriarGrpClientes;
                oAutorizacoes.UserFields.Fields.Item("U_P1_GPCnt").Value = autorizacao.GrpClientes;
                oAutorizacoes.UserFields.Fields.Item("U_P1_LogEmail").Value = autorizacao.RegistroEnvEmail;
                oAutorizacoes.UserFields.Fields.Item("U_P1_RTCobran").Value = autorizacao.RelatCobranças;

                if (oAutorizacoes.Update() != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
            }
            else
            {
                msg = "Usuário não encontrado";
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Criar Grupos
        public string CriarGrupos(string code, string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_AccGrp");

            string msg = null;
            oAutorizacoes.Code = code;
            oAutorizacoes.Name = name;
            if (oAutorizacoes.Add() != 0)
            {
                msg = oCompany.GetLastErrorDescription();
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Atualizar Autorizações de Grupos
        public string AtualizarAutorizacoesGrupos(AutorizacoesGrupo autorizacoes)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_AccGrp");

            string msg = null;

            try
            {
                if (oAutorizacoes.GetByKey(autorizacoes.Code))
                {
                    oAutorizacoes.UserFields.Fields.Item("U_P1_ConsSol").Value = autorizacoes.SolCompra;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewSol").Value = autorizacoes.CriarSolCompra;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_PedComp").Value = autorizacoes.PedCompra;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewPedC").Value = autorizacoes.CriarPedCompra;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_ReqS").Value = autorizacoes.ReqEstoque;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewReqS").Value = autorizacoes.CriarReqEstoque;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_Reemb").Value = autorizacoes.Reembolso;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewReem").Value = autorizacoes.CriarReembolso;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewMailM").Value = autorizacoes.CriarModeloEmail;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_MailMod").Value = autorizacoes.ModeloEmail;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_AstEnvio").Value = autorizacoes.AssistEnvio;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_Client").Value = autorizacoes.Clientes;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_NewGPCnt").Value = autorizacoes.CriarGrpClientes;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_GPCnt").Value = autorizacoes.GrpClientes;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_LogEmail").Value = autorizacoes.RegistroEnvEmail;
                    oAutorizacoes.UserFields.Fields.Item("U_P1_RTCobran").Value = autorizacoes.RelatCobranças;

                    if (oAutorizacoes.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                msg = oCompany.GetLastErrorDescription();
                Log.Gravar(msg, Log.TipoLog.Erro);
            }


            return msg;
        }

        #endregion

        #region Atualizar Grupo de Usuário
        public string AtualizarGrupoDoUsuario(string idUser, string idGrupo)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAccess = (SAPbobsCOM.UserTable)oCompany.UserTables.Item("P1_ACCESS");
            string msg = null;

            try
            {
                if (oAccess.GetByKey(idUser))
                {
                    oAccess.UserFields.Fields.Item("U_P1_Grupo").Value = idGrupo;

                    if (oAccess.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }
                else
                {
                    msg = "Usuário não encontrado na tabela de autorizações.";
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }


            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Autorizações de Usuário
        public AutorizacoesGrupo PegarGrupoAutorizacoes(Autorizacoes autorizacoes)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_AccGrp\"");
            SQL.AppendLine("WHERE \"Code\" = '" + autorizacoes.IdGrupo + "'");

            AutorizacoesGrupo autorizacao = new AutorizacoesGrupo();

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();

            if (!oRecordSet.EoF)
            {
                autorizacao.Code = oRecordSet.Fields.Item("Code").Value.ToString();
                autorizacao.Name = oRecordSet.Fields.Item("Name").Value.ToString();
                autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
                autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
                autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
                autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
                autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
                autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
                autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
                autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
                autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
                autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
                autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
                autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
                autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
                autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
                autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
                autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();
            }
            else
            {
                autorizacao.Code = "N";
                autorizacao.Name = "N";
                autorizacao.SolCompra = "N";
                autorizacao.CriarSolCompra = "N";
                autorizacao.PedCompra = "N";
                autorizacao.CriarPedCompra = "N";
                autorizacao.ReqEstoque = "N";
                autorizacao.CriarReqEstoque = "N";
                autorizacao.Reembolso = "N";
                autorizacao.CriarReembolso = "N";
                autorizacao.CriarModeloEmail = "N";
                autorizacao.ModeloEmail = "N";
                autorizacao.AssistEnvio = "N";
                autorizacao.Clientes = "N";
                autorizacao.CriarGrpClientes = "N";
                autorizacao.GrpClientes = "N";
                autorizacao.RegistroEnvEmail = "N";
                autorizacao.RelatCobranças = "N";
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return autorizacao;
        }
        #endregion
    }
}