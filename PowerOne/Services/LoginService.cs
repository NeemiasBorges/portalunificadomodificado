﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.ViewModel;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerOne.Services
{
    public class LoginService
    {
        #region Consulta valor log login
        public bool ConsultaLogLogin(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            bool aux = true;
       
            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"U_P1_Status\" FROM \"OHEM\" WHERE \"empId\" = " + login.Id + " ");

            oRecordSet.DoQuery(SQL.ToString());

            if (!oRecordSet.EoF)
            {
                oRecordSet.MoveFirst();

                if (oRecordSet.Fields.Item("U_P1_Status").Value.ToString() == "D")
                {
                    aux = false;
                }
                else
                {
                    aux = true;
                }

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return aux;

        }
        #endregion

        #region Logout update login
        public string logoutLogLogin(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.EmployeesInfo oEmployee = (SAPbobsCOM.EmployeesInfo)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo);
            //SAPbobsCOM.UserTable oLogin = oCompany.UserTables.Item("EB_LogLogin");

            string msg = null;

            try
            {
                if (oEmployee.GetByKey(login.Id))
                {
                    oEmployee.UserFields.Fields.Item("U_P1_Status").Value = "D";

                    if (oEmployee.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oEmployee);


            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region add valor logado
        public string AddValorLogin(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.EmployeesInfo oEmployee = (SAPbobsCOM.EmployeesInfo)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo);
            //SAPbobsCOM.UserTable oLogin = oCompany.UserTables.Item("EB_LogLogin");

            string msg = null;

            try
            {
                if (oEmployee.GetByKey(login.Id))
                {
                    //oLogin.Code = login.UserId.ToString();
                    //oLogin.Name = login.User;
                    //oLogin.UserFields.Fields.Item("P1_EB_IdLogado").Value = login.UserId;
                    oEmployee.UserFields.Fields.Item("U_P1_Status").Value = "L";

                    if (oEmployee.Update() != 0)
                    {
                        msg = oCompany.GetLastErrorDescription();
                    }
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }
          
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oEmployee);


            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Consulta de Usuario para Login
        public Login Login(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            Login user;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"empID\", concat(concat(\"firstName\", '.'), \"lastName\") AS \"Name\",");
            SQL.AppendLine("coalesce(\"BPLId\",1) \"BPLId\", coalesce(\"email\",'') \"email\" ,coalesce(\"userId\",-1) \"userId\"");
            SQL.AppendLine("FROM OHEM ");
            SQL.AppendLine("WHERE upper(concat(concat(\"firstName\", '.'), \"lastName\")) = upper('" + login.User + "') ");
            SQL.AppendLine("AND (\"U_P1_Password\" = '" + Criptografia.CalculateSHA1(login.Password) + "' OR ");
            SQL.AppendLine("(\"U_P1_Password\" = 'NOVO' and \"U_P1_Password\" = '" + login.Password + "'))");

            oRecordSet.DoQuery(SQL.ToString());

            if (!oRecordSet.EoF)
            {
                oRecordSet.MoveFirst();
                user = new Login()
                {
                    Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
                    User = oRecordSet.Fields.Item("Name").Value.ToString(),
                    IdFilial = int.Parse(oRecordSet.Fields.Item("BPLId").Value.ToString()),
                    UserId = int.Parse(oRecordSet.Fields.Item("UserId").Value.ToString())
                };
            }
            else
            {
                user = null;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return user;
        }
        #endregion

        #region Alterar Senha
        public void EditarSenha(AlterarSenhaViewModel alterarSenha)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.EmployeesInfo oEmployee = (SAPbobsCOM.EmployeesInfo)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo);

            if (oEmployee.GetByKey(alterarSenha.Id))
            {
                oEmployee.UserFields.Fields.Item("U_P1_Password").Value = Criptografia.CalculateSHA1(alterarSenha.NewPassword);

                int iRetcode = oEmployee.Update();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oEmployee);
            oEmployee = null;
        }
        #endregion

        #region Pegar Usuários por: Primeiro Nome, Últim Nome e Email
        public string GetIdUsuarioPorNomeEmail(string firstName, string lastName, string email)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string Id = null;

            string SQL = "SELECT \"empID\" FROM OHEM ";
            SQL += "WHERE \"email\" = '" + email + "' ";
            SQL += "AND \"firstName\" = '" + firstName + "' ";
            SQL += "AND \"lastName\" = '" + lastName + "'";

            oRecordset.DoQuery(SQL);

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                Id = oRecordset.Fields.Item("empID").Value.ToString();
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return Id;
        }
        #endregion

        #region Pegar Id Todos os Usuários 
        public List<Login> GetAllIdUsers()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            List<Login> usersId = new List<Login>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"EmpId\", \"FirstName\" FROM \"OHEM\"");

            oRecordSet.DoQuery(SQL.ToString());
            while (!oRecordSet.EoF)
            {
                usersId.Add(
                    new Models.Login()
                    {
                        Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
                        User = oRecordSet.Fields.Item("FirstName").Value.ToString()
                    });
                oRecordSet.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return usersId;
        }
        #endregion

        #region Atualizar Senha de Usuários já cadastrados
        public void ToFillTheEmptyPassword()
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.EmployeesInfo oEmployee = (SAPbobsCOM.EmployeesInfo)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo);

            List<Models.Login> users = GetUserEmptyPasswords();

            if (users != null)
            {
                foreach (var user in users)
                {
                    if (oEmployee.GetByKey(user.Id))
                    {
                        oEmployee.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";

                        int iRetcode = oEmployee.Update();

                        if (iRetcode != 0)
                        {
                            string sErrMessage = oCompany.GetLastErrorDescription();
                        }
                    }

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oEmployee);
                    oEmployee = null;
                    oEmployee = (SAPbobsCOM.EmployeesInfo)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oEmployee);
            oEmployee = null;

        }

        public List<Models.Login> GetUserEmptyPasswords()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Models.Login> userLogin = new List<Models.Login>();

            string SQL = "SELECT \"empID\", CONCAT(CONCAT(\"firstName\",'.'),\"lastName\") FROM OHEM WHERE \"U_P1_Password\" IS NULL";

            oRecordset.DoQuery(SQL);

            while (!oRecordset.EoF)
            {
                //oRecordset.MoveFirst();
                userLogin.Add(new Models.Login()
                {
                    Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString()),
                    User = oRecordset.Fields.Item(1).Value.ToString()
                });

                oRecordset.MoveNext();
            }

            //if (!oRecordset.EoF)
            //{
            //    oRecordset.MoveFirst();
            //    userLogin.Add(new Models.Login()
            //    {
            //        Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString()),
            //        User = oRecordset.Fields.Item(1).Value.ToString()
            //    });
            //}
            //else
            //{
            //    userLogin = null;
            //}

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return userLogin;
        }

        #endregion

    }
}