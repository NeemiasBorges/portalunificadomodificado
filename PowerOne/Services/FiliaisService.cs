﻿using System.Collections.Generic;
using PowerOne.Database;
using PowerOne.Models;
using System.Text;

namespace PowerOne.Services
{
    public class FiliaisService
    {
        #region List
        public List<Filiais> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Filiais> list = new List<Filiais>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"BPLId\", \"BPLName\" FROM OBPL WHERE NOT \"BPLId\" = 2 AND \"Disabled\" = 'N'");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Filiais item = new Filiais();
                item.BplId = int.Parse(oRecordset.Fields.Item("BPLId").Value.ToString());
                item.BplName = oRecordset.Fields.Item("BPLName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}