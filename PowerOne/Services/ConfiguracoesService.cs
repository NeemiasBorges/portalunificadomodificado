﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Services
{
    public class ConfiguracoesService
    {
        #region Configurar Email
        public string ConfigurarEmail(ConfiguracoesEmail configuracoesEmail)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_EMAILCONF");

            string msg = null;
            int iRet = -1;
            bool bExiste = oConfig.GetByKey("1"); //verificando se já existe


            oConfig.UserFields.Fields.Item("U_P1_Email").Value = configuracoesEmail.Email;
            oConfig.UserFields.Fields.Item("U_P1_Senha").Value = configuracoesEmail.Senha;
            oConfig.UserFields.Fields.Item("U_P1_Servidor").Value = configuracoesEmail.Servidor;
            oConfig.UserFields.Fields.Item("U_P1_Porta").Value = configuracoesEmail.Porta;

            if (!bExiste)//se já existir
            {
                oConfig.Code = "1";
                oConfig.Name = "1";

                iRet = oConfig.Add();
            }
            else
            {
                iRet = oConfig.Update();
            }

            if (iRet != 0)
            {
                msg = oCompany.GetLastErrorDescription();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
            oConfig = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Configurar Anexo de Email
        public string ConfiguraAnexo(ConfiguracoesEmail configuracoesAnexo)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_EMAILCONF");

            string msg = null;
            int iRet = -1;
            bool bExiste = oConfig.GetByKey("1"); //verificando se já existe

            oConfig.UserFields.Fields.Item("U_P1_DrBoleto").Value = configuracoesAnexo.DirBoleto;
            oConfig.UserFields.Fields.Item("U_P1_DrNtFisc").Value = configuracoesAnexo.DirNotaFiscal;

            if (!bExiste)//se já existir
            {
                oConfig.Code = "1";
                oConfig.Name = "1";

                iRet = oConfig.Add();
            }
            else
            {
                iRet = oConfig.Update();
            }

            if (iRet != 0)
            {
                msg = oCompany.GetLastErrorDescription();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
            oConfig = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Configuracoes de Email
        public ConfiguracoesEmail GetConfiguracoesEmail()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            ConfiguracoesEmail configEmail = null;

            StringBuilder SQl = new StringBuilder();

            SQl.AppendLine("SELECT * FROM \"@P1_EMAILCONF\"");

            oRecordset.DoQuery(SQl.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                configEmail = new ConfiguracoesEmail(); //ele só não será null se existir configurações
                configEmail.Email = oRecordset.Fields.Item("U_P1_Email").Value.ToString();
                configEmail.Senha = oRecordset.Fields.Item("U_P1_Senha").Value.ToString();
                configEmail.Servidor = oRecordset.Fields.Item("U_P1_Servidor").Value.ToString();
                configEmail.Porta = int.Parse(oRecordset.Fields.Item("U_P1_Porta").Value.ToString());

                configEmail.DirBoleto = oRecordset.Fields.Item("U_P1_DrBoleto").Value.ToString();
                configEmail.DirNotaFiscal = oRecordset.Fields.Item("U_P1_DrNtFisc").Value.ToString();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return configEmail;
        }
        #endregion

        #region Configurar Arquivos para Impressão
        public string ConfigurarModeloImpressao(string code, string name, string path)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_RPTDOC");

            string msg = null;
            int iRet = -1;

            bool bExiste = oConfig.GetByKey(code); //verificando se já existe

            oConfig.UserFields.Fields.Item("U_P1_Diretorio").Value = path;

            if (!bExiste)//se já existir
            {
                oConfig.Code = code;
                oConfig.Name = name;

                iRet = oConfig.Add();
            }
            else
            {
                iRet = oConfig.Update();
            }

            if (iRet != 0)
            {
                msg = oCompany.GetLastErrorDescription();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
            oConfig = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Diretorio de Modelo para Impressao
        public string GetDiretorioModeloImpressao(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string path = null;

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT \"U_P1_Diretorio\" FROM \"@P1_RPTDOC\"");
            SQL.AppendLine("WHERE \"Code\" = '" + code + "'");

            oRecordset.DoQuery(SQL.ToString());

            if (!oRecordset.EoF)
            {
                path = oRecordset.Fields.Item("U_P1_Diretorio").Value.ToString();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return path;
        }
        #endregion

        #region Configurações de Tema
        public string ConfigurarTema(string code, string cor, string icone)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_TEMA");

            string msg = null;
            int iRet = -1;

            //O código do usuário é utilizado como chave primária
            bool bExiste = oConfig.GetByKey(code); //verificando se já existe

            if (cor != null)
            {
                oConfig.UserFields.Fields.Item("U_P1_Color").Value = cor;
            }

            if (icone != null)
            {
                oConfig.UserFields.Fields.Item("U_P1_Icones").Value = icone;
            }

            if (!bExiste)//se já existir
            {
                oConfig.Code = code;
                oConfig.Name = code;
                iRet = oConfig.Add();
            }
            else
            {
                iRet = oConfig.Update();
            }

            if (iRet != 0)
            {
                msg = oCompany.GetLastErrorDescription();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
            oConfig = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Configuracoes de Tema
        public Tema GetTema(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            Tema configTema = null;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_TEMA\"");
            SQL.AppendLine("WHERE Code = '" + code + "'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                configTema = new Tema(); //ele só não será null se existir configurações
                configTema.UserId = oRecordset.Fields.Item("Code").Value.ToString();
                configTema.Icon = oRecordset.Fields.Item("U_P1_Icones").Value.ToString();
                configTema.Color = oRecordset.Fields.Item("U_P1_Color").Value.ToString();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return configTema;

        }
        #endregion

        #region Configurar Centros de Custo
        public string ConfigurarCentroCusto(int[] dimencoes)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.CompanyService oCompanyService = oCompany.GetCompanyService();

            SAPbobsCOM.DimensionsService oDIMService =
                (SAPbobsCOM.DimensionsService)
                oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.DimensionsService);

            string msg = null;
            try
            {
                for (int i = 1; i <= 5; i++)
                {
                    SAPbobsCOM.DimensionParams oDIMParams =
                        (SAPbobsCOM.DimensionParams)
                        oDIMService.GetDataInterface
                        (SAPbobsCOM.DimensionsServiceDataInterfaces.dsDimensionParams);

                    oDIMParams.DimensionCode = i;
                    SAPbobsCOM.Dimension oDIM = oDIMService.GetDimension(oDIMParams);
                    if (dimencoes.Contains(i))
                    {
                        oDIM.UserFields.Item("U_P1_Ativado").Value = "Y";
                    }
                    else
                    {
                        oDIM.UserFields.Item("U_P1_Ativado").Value = "N";
                    }
                    oDIMService.UpdateDimension(oDIM);
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Configurações de Centro de Custo
        public int[] PegarConfiguracoesCentroCusto()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            int[] ctCusto = new int[5];

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT T0.\"DimCode\" FROM ODIM T0");
            SQL.AppendLine("WHERE T0.\"U_P1_Ativado\" = 'Y'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            int i = 0;
            while (!oRecordset.EoF)
            {
                ctCusto[i] = int.Parse(oRecordset.Fields.Item("DimCode").Value.ToString());
                oRecordset.MoveNext();
                i++;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return ctCusto;
        }
        #endregion

    }
}