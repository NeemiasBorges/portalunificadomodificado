﻿using Antlr.Runtime;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.Estoque;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.Hosting;

namespace PowerOne.Services.Estoque
{
    public class RequerimentoEstoqueService
    {
        #region Serviços
        private GeralQuery geralQUery = new GeralQuery();
        #endregion

        #region contagem de documentos

        public int contEstoque()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            RequerimentoEstoque estoque = new RequerimentoEstoque();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM  \"@P1_OIGE\" ");
            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                estoque.Contagem = int.Parse(oRecordSet.Fields.Item("Count").Value.ToString());

            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return estoque.Contagem;
        }

        #endregion

        #region Pesquisar Requerimentos e Listar
        public List<RequerimentoEstoque> List(Login login, string dtInicio, string dtFim, string status)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<RequerimentoEstoque> requerimentos = new List<RequerimentoEstoque>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", ");
            SQL.AppendLine(" CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN'Aberta' ELSE 'Fechada' END \"DocStatus\",");
            SQL.AppendLine(" T0.\"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"Requisitante\"");
            SQL.AppendLine(" FROM \"@P1_OIGE\" T0 INNER JOIN OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

            if (geralQUery.VerificarAutorizacao(login.Id.ToString(), "U_P1_ReqS") == "R")
            {
                SQL.AppendLine(" WHERE (T0.\"U_G2_Owner\" = '" + login.Id.ToString() + "') AND ");
            }
            else
            {
                SQL.AppendLine(" WHERE 1 = 1 AND ");
            }



            //if ((dtInicio != null) && (dtFim != null))
            //{
            //    SQL.AppendLine(" AND T0.\"U_G2_DTLANC\" BETWEEN '" +
            //       (DateTime.ParseExact(dtInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd")
            //       + "' AND '" +
            //       (DateTime.ParseExact(dtFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd")
            //       + "' ");
            //}

            if (!String.IsNullOrEmpty(dtInicio))
            {
                SQL.AppendLine(" T0.\"U_P1_DTLANC\" >= '" + (DateTime.ParseExact(dtInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' AND ");
            }

            if (!String.IsNullOrEmpty(dtFim))
            {
                SQL.AppendLine("T0.\"U_P1_DTLANC\" <='" + (DateTime.ParseExact(dtFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' AND ");
            }


            if (status != "T")
            {
                SQL.AppendLine(" T0.\"U_P1_STATUS\" = '" + status + "' ");
            }

            string teste = (SQL.ToString()).Substring(SQL.Length - 6);
            if ((SQL.ToString()).Substring(SQL.Length - 6) == "AND \r\n")
            {
                StringBuilder aux = new StringBuilder(SQL.ToString().Substring(0, SQL.Length - 6));
                SQL = aux;
            }


            // ordena apartir do último documento
            SQL.AppendLine("ORDER BY T0.\"DocEntry\" DESC ");

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            while (!oRecordSet.EoF)
            {
                requerimentos.Add(new RequerimentoEstoque()
                {
                    DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString()),
                    DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString()),
                    DataLancamento = DateTime.Parse(oRecordSet.Fields.Item("U_P1_DTLANC").Value.ToString()),
                    Requisitante = oRecordSet.Fields.Item("Requisitante").Value.ToString(),
                    Status = oRecordSet.Fields.Item("DocStatus").Value.ToString()
                });
                oRecordSet.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return requerimentos;
        }
        #endregion

        #region Pegar Requerimento Específico
        public RequerimentoEstoque GetByKey(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            RequerimentoEstoque requerimentoEstoque = new RequerimentoEstoque();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\", ");
            SQL.AppendLine(" CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN'Aberta' ELSE 'Fechada' END \"DocStatus\",");
            SQL.AppendLine(" T0.\"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"Requisitante\"");
            SQL.AppendLine(" FROM \"@P1_OIGE\" T0 INNER JOIN OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

            SQL.AppendLine("WHERE T0.\"DocEntry\" = " + DocEntry);

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            if (!oRecordSet.EoF)
            {
                requerimentoEstoque.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                requerimentoEstoque.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                requerimentoEstoque.DataLancamento = DateTime.Parse(oRecordSet.Fields.Item("U_P1_DTLANC").Value.ToString());
                requerimentoEstoque.Requisitante = oRecordSet.Fields.Item("Requisitante").Value.ToString();
                requerimentoEstoque.Status = oRecordSet.Fields.Item("DocStatus").Value.ToString();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            requerimentoEstoque.Itens = ListarLinhas(DocEntry, null);

            return requerimentoEstoque;
        }
        #endregion

        #region Create
        public string Create(RequerimentoEstoque requerimento, Login user)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;

            string msg = null;

            try
            {
                SAPbobsCOM.GeneralService oGeneralService;
                SAPbobsCOM.GeneralData oGeneralData;
                SAPbobsCOM.GeneralDataCollection oSons;
                SAPbobsCOM.GeneralData oSon;
                SAPbobsCOM.CompanyService sCmp;

                sCmp = oCompany.GetCompanyService();

                // Pegando o Servico que vai tratar o UDO
                oGeneralService = sCmp.GetGeneralService("P1_OIGE");

                // Inserindo os dados do UDO
                oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralData.SetProperty("U_P1_STATUS", "O");
                oGeneralData.SetProperty("U_P1_DTLANC", DateTime.Now);
                oGeneralData.SetProperty("U_P1_Owner", user.Id.ToString());
                if (!String.IsNullOrEmpty(requerimento.Observacao))
                {
                    oGeneralData.SetProperty("U_P1_Comments", requerimento.Observacao);
                }

                oSons = oGeneralData.Child("P1_IGE1");

                foreach (var item in requerimento.Itens)
                {
                    oSon = oSons.Add();
                    oSon.SetProperty("U_P1_ItemCode", item.CodigoItem);
                    oSon.SetProperty("U_P1_Quantity", item.Quantidade);
                    if (!String.IsNullOrEmpty(item.Observacao))
                    {
                        oSon.SetProperty("U_P1_Comments", item.Observacao);
                    }
                    oSon.SetProperty("U_P1_WhsCode", item.CodigoDeposito);
                    oSon.SetProperty("U_P1_LinStatus", "O");
                }

                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;

        }
        #endregion

        #region Listar Linhas do Requerimento
        public List<RequerimentoEstoqueLinhas> ListarLinhas(int DocEntry, string[] linhas)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<RequerimentoEstoqueLinhas> requerimentosLinhas = new List<RequerimentoEstoqueLinhas>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\",");
            SQL.AppendLine("case when T0.\"U_G2_STATUS\" = 'O' then 'Aberta' else 'Fechada' end \"DocStatus\", ");
            SQL.AppendLine(" T0.\"U_P1_DTLANC\", T1.\"LineId\", T1.\"U_P1_ItemCode\",i.\"ItemName\", T1.\"U_P1_Quantity\", o.\"WhsCode\",o.\"WhsName\",");
            SQL.AppendLine(" case when T1.\"U_P1_LinStatus\" = 'O' then 'Aberta' ");
            SQL.AppendLine("when T1.\"U_P1_LinStatus\" = 'C'");
            SQL.AppendLine("then 'Cancelada' else 'Fechada' end \"LinStatus\",");
            SQL.AppendLine("i.\"InvntryUom\"");
            SQL.AppendLine(" FROM \"@P1_OIGE\"  T0 inner join \"@P1_IGE1\"  T1 on T0.\"DocEntry\" = T1.\"DocEntry\" ");
            SQL.AppendLine(" inner join oitm i on t1.\"U_P1_ItemCode\" = i.\"ItemCode\"");
            SQL.AppendLine("  left join OWHS o on t1.\"U_P1_WhsCode\" = o.\"WhsCode\"");
            SQL.AppendLine(" WHERE T0.\"DocEntry\" = '" + DocEntry + "' ");

            if (linhas != null)
            {
                SQL.AppendLine("AND T1.\"LineId\" in (" + string.Join(",", linhas) + ")");
            }

            oRecordSet.DoQuery(SQL.ToString());

            oRecordSet.MoveFirst();
            while (!oRecordSet.EoF)
            {
                RequerimentoEstoqueLinhas linha = new RequerimentoEstoqueLinhas();
                linha.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
                linha.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
                linha.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
                linha.DataRequisicao = DateTime.Parse(oRecordSet.Fields.Item("U_P1_DTLANC").Value.ToString());
                linha.IdLinha = int.Parse(oRecordSet.Fields.Item("LineId").Value.ToString());
                linha.CodigoItem = oRecordSet.Fields.Item("U_P1_ItemCode").Value.ToString();
                linha.DecricaoItem = oRecordSet.Fields.Item("ItemName").Value.ToString();
                linha.Quantidade = int.Parse(oRecordSet.Fields.Item("U_P1_Quantity").Value.ToString());
                linha.StatusLinha = oRecordSet.Fields.Item("LinStatus").Value.ToString();
                linha.UndMedida = oRecordSet.Fields.Item("InvntryUom").Value.ToString();
                linha.CodigoDeposito = oRecordSet.Fields.Item("WhsCode").Value.ToString();
                linha.DescricaoDeposito = oRecordSet.Fields.Item("WhsName").Value.ToString();

                requerimentosLinhas.Add(linha);
                oRecordSet.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return requerimentosLinhas;
        }
        #endregion

        #region Aprovar Requerimento de Estoque(Gerar Saída de Estoque)
        public string AprovarRequerimento(Login login, string[] linesChecked, string observacoes, int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            string msgErro = null;

            if (oCompany.InTransaction)
            {
                oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }

            oCompany.StartTransaction();

            try
            {
                ///* (Documento, lista de ids que se deseja pegar)*/
                List<RequerimentoEstoqueLinhas> linhas = ListarLinhas(DocEntry, linesChecked);
                msgErro = InserirSaidaMercadoria(login, observacoes, DocEntry, linhas);

                if (msgErro != null)
                {
                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
                else
                {
                    FecharRequerimento(DocEntry, linhas);

                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                }
            }
            catch (Exception e)
            {
                if (oCompany.InTransaction)
                {
                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
                if (e.Message != "")
                {
                    msgErro = e.Message;
                }
            }

            if (msgErro != null)
            {
                Log.Gravar(msgErro, Log.TipoLog.Erro);
            }

            return msgErro;
        }

        #region Inserir Requisição
        /*
         * Gerar um documento de saída de estoque a partir da requisição
         */
        public string InserirSaidaMercadoria(Login login, string observacoes, int docEntry, List<RequerimentoEstoqueLinhas> linhas)
        {
            string erroMsg = null;
            //bool sCamposObrigatorio = true;
            SAPbobsCOM.Company oCompany = Conexao.Company;
            try
            {
                SAPbobsCOM.Documents oSaidaEstoque;
                oSaidaEstoque = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);

                oSaidaEstoque.DocDate = DateTime.Now;
                oSaidaEstoque.BPL_IDAssignedToInvoice = 1;

                foreach (var linha in linhas)
                {
                    if (linha.DocStatus == "Aberta")
                    {
                        oSaidaEstoque.Lines.ItemCode = linha.CodigoItem;
                        SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                        string SQL = "SELECT T0.\"UomEntry\" FROM OUOM T0 where T0.\"UomCode\"  = '" + linha.UndMedida + "' ";
                        oRecordset.DoQuery(SQL);

                        if (!oRecordset.EoF)
                        {
                            oSaidaEstoque.Lines.UoMEntry = Convert.ToInt32(oRecordset.Fields.Item("UomEntry").Value);
                        }
                        oSaidaEstoque.Lines.Quantity = Convert.ToDouble(linha.Quantidade);
                        oSaidaEstoque.Lines.WarehouseCode = linha.CodigoDeposito;
                        oSaidaEstoque.Lines.Add();
                    }
                }

                oSaidaEstoque.Comments = observacoes;
                oSaidaEstoque.UserFields.Fields.Item("U_G2_Conferente").Value = login.User.ToString();
                oSaidaEstoque.UserFields.Fields.Item("U_G2_ObsConf").Value = docEntry.ToString();

                if (oSaidaEstoque.Add() != 0)
                {
                    erroMsg = oCompany.GetLastErrorDescription();
                }
            }
            catch (Exception ex)
            {
                erroMsg = ex.Message;
            }

            return erroMsg;
        }
        #endregion

        #endregion

        #region Fechar Requerimento
        public string FecharRequerimento(int DocEntry, List<RequerimentoEstoqueLinhas> linhas)
        {
            SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Conexao.Company;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralData oGeneralData;

            SAPbobsCOM.CompanyService sCmp;
            SAPbobsCOM.GeneralDataParams oGeneralParams;

            SAPbobsCOM.GeneralDataCollection oSons;
            SAPbobsCOM.GeneralData oSon;

            sCmp = oCompany.GetCompanyService();
            oGeneralService = sCmp.GetGeneralService("P1_OIGE");

            string msg = null;

            if (VerificarRequisicao(DocEntry))
            {
                oGeneralService = sCmp.GetGeneralService("P1_OIGE");

                oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralParams = (SAPbobsCOM.GeneralDataParams)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                try
                {
                    oGeneralParams.SetProperty("DocEntry", DocEntry);
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                    oSons = oGeneralData.Child("P1_IGE1");

                    //Se linhas for igual a Null, é o usuário clicou em fechar documento
                    if (linhas == null)
                    {
                        linhas = ListarLinhas(DocEntry, null);
                        oGeneralData.SetProperty("U_P1_STATUS", "C");
                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
                    }// Se todas as linhas forem fechadas
                    else if (CountOpenedLines(DocEntry) == linhas.Count)
                    {
                        oGeneralData.SetProperty("U_P1_STATUS", "C");
                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
                    }


                    foreach (RequerimentoEstoqueLinhas linha in linhas)
                    {
                        if (linha.DocStatus == "Aberta")
                        {
                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_LinStatus", "F");
                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_DTEFET", DateTime.Now);
                        }
                    }

                    oGeneralService.Update(oGeneralData);
                }
                catch (Exception e)
                {
                    msg = e.Message;
                }
            }
            else
            {
                msg = "Erro: requerimento de Estoque não encontrado!";
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Número de Linhas com status em aberto
        public int CountOpenedLines(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Count(*) as \"NumLine\" FROM \"@P1_IGE1\"");
            SQL.AppendLine("WHERE \"U_P1_LinStatus\"='O' AND \"DocEntry\"='" + DocEntry + "'");

            int countLines = 0;

            oRecordset.DoQuery(SQL.ToString());
            if (oRecordset.RecordCount > 0)
            {
                countLines = int.Parse(oRecordset.Fields.Item("NumLine").Value.ToString());
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return countLines;
        }
        #endregion

        #region Verifica se o Documento de Requisição existe
        public bool VerificarRequisicao(int DocEntry)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            bool bExiste = false;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT Count(*) as \"NumLine\" FROM \"@P1_IGE1\"");
            SQL.AppendLine("WHERE \"U_P1_LinStatus\"='O' AND \"DocEntry\"='" + DocEntry + "'");

            oRecordset.DoQuery(SQL.ToString());

            if (oRecordset.RecordCount > 0)
            {
                if (oRecordset.Fields.Item("NumLine").Value.ToString() != "0")
                {
                    bExiste = true;
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return bExiste;
        }
        #endregion
    }
}