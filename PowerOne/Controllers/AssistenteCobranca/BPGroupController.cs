﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Services.AssistenteCobranca;
using PowerOne.Util;
using System.Collections.Generic;
using System.Web.Mvc;


namespace PowerOne.Controllers.AssistenteCobranca
{
    #region Grupo de Parceiro de negocios
    public class BPGroupController : Controller
    {
        #region Atributos
        private MailModelService _mailModelService = new MailModelService();
        private BPGroupService _bPGroupService = new BPGroupService();
        private GroupTypeMailService _groupTypeMailService = new GroupTypeMailService();
        #endregion

        #region Index
        public ActionResult Index()
        {
            if ( ((Login)Session["auth"]).Autorizacoes.Clientes == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.Clientes == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }
            List<BPGroup> bpGroups = _bPGroupService.List();

            //Pegando os modelos de Email para Cada Grupo
            //foreach (var item in bpGroups)
            //{
            //    MailModel DefaultModel = _mailModelService.GetByGroupAndType(item.CodeGroup, "T0001",item.);
            //    MailModel AlertModel = _mailModelService.GetByGroupAndType(item.CodeGroup, "T0002");
            //    MailModel BillingModel = _mailModelService.GetByGroupAndType(item.CodeGroup, "T0003");

            //    item.DefaultModelId = DefaultModel.Code;
            //    item.DefaultModel = DefaultModel;
            //    item.AlertModelId = AlertModel.Code;
            //    item.AlertModel = AlertModel;
            //    item.BillingModelId = BillingModel.Code;
            //    item.BillingModel = BillingModel;
            //}

            return View(bpGroups);
        }
        #endregion

        #region Details
        public ActionResult Details(string code)
        {
            if (code == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }

            BPGroup bpGroup = GetSpecificGroup(code);

            if (bpGroup == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de Grupo não existe!" });
            }

            return View(bpGroup);
        }
        #endregion

        #region Create GET
        public ActionResult Create()
        {
            if (((Login)Session["auth"]).Autorizacoes.CriarGrpClientes == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.CriarGrpClientes == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }
            return View();
        }
        #endregion

        #region Create POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BPGroup bpGroup)
        {
            if ( ((Login)Session["auth"]).Autorizacoes.CriarGrpClientes == "N" || ((Login)Session["auth"]).Autorizacoes.Grupo.CriarGrpClientes == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(bpGroup);
            }

            if (_bPGroupService.VerifyIfExist(bpGroup.CodeGroup))
            {
                string msg = "Código pertencente a um objeto já cadastrado.";
                Log.Gravar(msg, Log.TipoLog.Erro);
                ModelState.AddModelError("CodeGroup", msg);
                return View(bpGroup);
            }

            if (_bPGroupService.VerifyByName(bpGroup.DescGroup))
            {
                string msg = "Descrição pertencente a um objeto já cadastrado.";
                Log.Gravar(msg, Log.TipoLog.Erro);
                ModelState.AddModelError("DescGroup", msg);
                return View(bpGroup);
            }

            _bPGroupService.Create(bpGroup);
            TempData["Success"] = $"O grupo de Parceiros de Negócio {bpGroup.CodeGroup} foi cadastrado com sucesso!";

            return RedirectToAction("Index");
        }
        #endregion

        #region PayMethod GET
        public ActionResult PayMethod(string typeModel, string group)
        {
            PayMethodViewModel payMethodModels = new PayMethodViewModel();

            payMethodModels.BPGroupId = group;
            payMethodModels.TypeModelId = typeModel;

            //pegando a lista de modelos
            //Geral
            payMethodModels.GeralModels = _mailModelService.GetByTypepAndPayMethod(typeModel, "G");
            payMethodModels.GeralModels.Insert(0, null);
            payMethodModels.GeralModelId = _mailModelService.GetByGroupAndType(group, typeModel, "G").Code;

            //Boleto
            payMethodModels.BilletModels = _mailModelService.GetByTypepAndPayMethod(typeModel, "B");
            payMethodModels.BilletModels.Insert(0, null);
            payMethodModels.BilletModelId = _mailModelService.GetByGroupAndType(group, typeModel, "B").Code;

            //Transferencia Bancária

            payMethodModels.BankTransferModels = _mailModelService.GetByTypepAndPayMethod(typeModel, "T");
            payMethodModels.BankTransferModels.Insert(0, null);
            payMethodModels.BankTransferModelId = _mailModelService.GetByGroupAndType(group, typeModel, "T").Code;


            TempData["TypeModel"] = typeModel;
            return View(payMethodModels);
        }
        #endregion

        #region PayMethod POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayMethod(PayMethodViewModel data)
        {
            //Geral
            if (data.GeralModelId != null)
            {
                string code = data.BPGroupId + data.GeralModelId;
                if (!_groupTypeMailService.VerifyIfExist(code))
                {
                    string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "G");
                    if (codigoJaVinculado != null)
                    {
                        _groupTypeMailService.Delete(codigoJaVinculado);
                    }
                    _groupTypeMailService.Create(code, code, data.BPGroupId, data.TypeModelId, data.GeralModelId, "G");
                }
            }
            else
            {
                string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "G");
                if (codigoJaVinculado != null)
                {
                    _groupTypeMailService.Delete(codigoJaVinculado);
                }
            }


            // Boleto
            if (data.BilletModelId != null)
            {
                string code = data.BPGroupId + data.BilletModelId;
                if (!_groupTypeMailService.VerifyIfExist(code))
                {
                    string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "B");
                    if (codigoJaVinculado != null)
                    {
                        _groupTypeMailService.Delete(codigoJaVinculado);
                    }
                    _groupTypeMailService.Create(code, code, data.BPGroupId, data.TypeModelId, data.BilletModelId, "B");
                }
            }
            else
            {
                string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "B");
                if (codigoJaVinculado != null)
                {
                    _groupTypeMailService.Delete(codigoJaVinculado);
                }
            }

            //Transferencia Bancária
            if (data.BankTransferModelId != null)
            {
                string code = data.BPGroupId + data.BankTransferModelId;
                if (!_groupTypeMailService.VerifyIfExist(code))
                {
                    string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "T");
                    if (codigoJaVinculado != null)
                    {
                        _groupTypeMailService.Delete(codigoJaVinculado);
                    }
                    _groupTypeMailService.Create(code, code, data.BPGroupId, data.TypeModelId, data.BankTransferModelId, "T");
                }
            }
            else
            {
                string codigoJaVinculado = _groupTypeMailService.GetByGroupTypePayMethod(data.BPGroupId, data.TypeModelId, "T");
                if (codigoJaVinculado != null)
                {
                    _groupTypeMailService.Delete(codigoJaVinculado);
                }
            }

            TempData["Success"] = "Alteração dos modelos finalizada com sucesso!";
            return RedirectToAction(nameof(PayMethod), new { typeModel = data.TypeModelId, group = data.BPGroupId });
        }
        #endregion

        #region Edit GET
        public ActionResult Edit(string code)
        {
            if (code == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }

            BPGroup bpGroup = GetSpecificGroup(code);
            if (bpGroup == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de Grupo não existe!" });
            }

            return View(bpGroup);
        }
        #endregion

        #region Edit POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BPGroup model)
        {
            if (!ModelState.IsValid)
            {
                //Pegando dados para preencher os dropdows do formulário
                ViewData["Default"] = _mailModelService.FindByType("T0001");
                ViewData["Alert"] = _mailModelService.FindByType("T0002");
                ViewData["Billing"] = _mailModelService.FindByType("T0003");

                return View(model);
            }

            _bPGroupService.Edit(model);
            TempData["Success"] = $"O grupo de Parceiros de Negócio {model.CodeGroup} foi editado com sucesso!";

            return RedirectToAction("Index");
        }
        #endregion

        #region Delete
        public ActionResult Delete(string code)
        {
            if (_bPGroupService.VerifyIfIsLinked(code))
            {
                TempData["Error"] = "Erro: Grupo vinculado a um Parceiro de Negócios!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }

            if (code == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }

            BPGroup bpGroup = GetSpecificGroup(code);

            if (bpGroup == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de Grupo não existe!" });
            }

            return View(bpGroup);
        }
        #endregion

        #region DeleteGroup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGroup(BPGroup group)
        {

            List<string> linkedCodes = _bPGroupService.GetLinkCodes(group.CodeGroup);
            _bPGroupService.Delete(group, linkedCodes);

            TempData["Success"] = $"Grupo de Parceiros de Negócio {group.CodeGroup} foi excluído com sucesso!";

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region GetSpecificGroup
        public BPGroup GetSpecificGroup(string code)
        {
            BPGroup bpGroup = _bPGroupService.GetByKey(code);

            //if (bpGroup != null) {
            //    //Pegando os dados do Modelo
            //    //MailModel DefaultModel = _mailModelService.GetByGroupAndType(bpGroup.CodeGroup, "T0001");
            //    //MailModel AlertModel = _mailModelService.GetByGroupAndType(bpGroup.CodeGroup, "T0002");
            //    //MailModel BillingModel = _mailModelService.GetByGroupAndType(bpGroup.CodeGroup, "T0003");

            //    bpGroup.DefaultModelId = DefaultModel.Code;
            //    bpGroup.DefaultModel = DefaultModel;
            //    bpGroup.AlertModelId = AlertModel.Code;
            //    bpGroup.AlertModel = AlertModel;
            //    bpGroup.BillingModelId = BillingModel.Code;
            //    bpGroup.BillingModel = BillingModel;
            //}

            return bpGroup;
        }
        #endregion

        #region Error
        public ActionResult Error(string message)
        {
            ErrorViewModel viewModel = new ErrorViewModel
            {
                Message = message
            };

            return View(viewModel);
        }
        #endregion
    }
    #endregion
}