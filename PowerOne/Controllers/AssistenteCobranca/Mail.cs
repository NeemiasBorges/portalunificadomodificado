﻿using PowerOne.Models.AssistenteCobranca;
using PowerOne.Services.AssistenteCobranca;
using System;
using System.IO;
using System.Net.Mail;
using PowerOne.Util;
using SAPbouiCOM;
using System.Security.Policy;
using System.Web.Hosting;
using PowerOne.Database;
using PowerOne.Services;
using PowerOne.Models;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class Email
    {
        #region
        ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        #endregion

        #region envia email
        //Para enviar emails de cobranca
        public bool EnviaEmail(BillMessage billMessage, ConfiguracoesEmail configuracoesEmail)
        {
            bool sEnviou = false;


            MailMessage objEmail = new MailMessage();

            //'Define o Campo From e ReplyTo do e-mail.
            objEmail.From = new System.Net.Mail.MailAddress(Conexao.Company.CompanyName + "< " + configuracoesEmail.Email + " >");


            objEmail.To.Add("<" + billMessage.EmailAdress + ">");

            //Adicionar outros email que irão receber a mensagem
            if (billMessage.EmailContactsCopyLists != null)
            {
                foreach (var email in billMessage.EmailContactsCopyLists)
                {
                    objEmail.CC.Add("<" + email.Email + ">");
                }
            }
            objEmail.Priority = System.Net.Mail.MailPriority.Normal;

            //  'Define o formato do e-mail HTML (caso não queira HTML alocar valor false)
            objEmail.IsBodyHtml = true;

            // 'Define o título do e-mail.
            objEmail.Subject = billMessage.MailModel.Subject;

            //Construindo o Body do Email

            //adicionando a logo do email
            Attachment logo = new Attachment(HostingEnvironment.MapPath(Constantes.DiretorioLogoLayoutEmail));

            objEmail.Attachments.Add(logo);

            string prefixo = "<!DOCTYPE html><html><head><title></title></head><body><div style=\"background-color:rgb(241, 241, 241); width: 100%; min-width:620px; table-layout:fixed; border-collapse:collapse; border-spacing:0px;\"> <table style=\"width: 60%; margin: auto; background-color:#ffffff\"> <tr> <td style=\"width:100%\"> <img src=\"cid:" + logo.ContentId + "\" style =\"margin:auto;display:block;width:50%;\"> </td></tr><tr> <td style=\"padding:50px;\">";
            string sufixo = " </td></tr><tr> <td style=\"padding: 50px; font-size:0.7em\"> <center> Rua Santos Dumont, 225 | Centro09715-120 | São Bernardo do Campo – SPPABX – (11) 4121-7200<br><br>ACR, segurança que você espera, qualidade que você confia. </center> </td></tr></table></div></body></html>";

            objEmail.Body = prefixo + billMessage.MailModel.Message + sufixo;

            //   'Para evitar problemas com caracteres "estranhos", configuramos o Charset para "ISO-8859-1"
            objEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            objEmail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            /*
             * Adicionando Anexos ao Email
             */

            //Notas Fiscais
            if (billMessage.InvoiceModel != "-")//se existir serial number é uma cobrança de Nota Fiscal
            {
                string nfs_path = @Constantes.DiretorioNotasFiscais;
                //Montando o nome do anexo pdf com o serial code da NFS
                string nfs_name = "NFS" + billMessage.SerialNumber + ".pdf";

                if (File.Exists(nfs_path + nfs_name))
                {
                    objEmail.Attachments.Add(new Attachment(nfs_path + nfs_name));
                }
            }


            //Boletos
            if (billMessage.PayMethod == "B")
            {
                string boleto_path = @Constantes.DiretorioBoletos;
                string boleto_name = "";
                if (billMessage.InvoiceModel != "-")
                {
                    boleto_name = "BOLETO-" + "NFS" + billMessage.SerialNumber + "-" + billMessage.DocCode.Split('-')[1] + ".pdf";
                }
                else
                {
                    boleto_name = "BOLETO-" + billMessage.DocCode + ".pdf";
                }


                if (File.Exists(boleto_path + boleto_name))
                {
                    objEmail.Attachments.Add(new Attachment(boleto_path + boleto_name));
                }
            }

            //   'Cria objeto com os dados do SMTP
            System.Net.Mail.SmtpClient objSmtp = new System.Net.Mail.SmtpClient();

            // 'Alocamos o endereço do host para enviar os e-mails  

            objSmtp.Host = configuracoesEmail.Servidor;
            objSmtp.Port = configuracoesEmail.Porta;
            objSmtp.EnableSsl = true;

            objSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            objSmtp.UseDefaultCredentials = true;
            objSmtp.Credentials = new System.Net.NetworkCredential(configuracoesEmail.Email, configuracoesEmail.Senha);

            try
            {
                if(configuracoesEmail.Servidor == "smtp.office365.com")
                {
                    objSmtp.TargetName = "STARTTLS/smtp.office365.com";
                }
                objSmtp.Send(objEmail);

                sEnviou = true;
            }
            catch (Exception ex)
            {
                Log.Gravar(ex.Message, Log.TipoLog.Erro);
                sEnviou = false;
            }
            finally
            {
                //excluímos o objeto de e-mail da memória
                objEmail.Dispose();
            }

            return sEnviou;
        }
        #endregion

        #region recuperar login
        //Para enviar emails de recuperação de senhas
        public bool RecoverPasswordEmail(string password, string email, ConfiguracoesEmail configuracoesEmail)
        {
            bool sEnviou = false;


            MailMessage objEmail = new MailMessage();

            //'Define o Campo From e ReplyTo do e-mail.
            objEmail.From = new System.Net.Mail.MailAddress(Conexao.Company.CompanyName + "< " + configuracoesEmail.Email + " >");
            objEmail.To.Add("<" + email + ">");
            objEmail.Priority = System.Net.Mail.MailPriority.Normal;
            //  'Define o formato do e-mail HTML (caso não queira HTML alocar valor false)
            objEmail.IsBodyHtml = true;
            // 'Define o título do e-mail.
            objEmail.Subject = "Recuperação de Senha";

            //Construindo o Body do Email
            //adicionando a logo do email
            Attachment logo = new Attachment(HostingEnvironment.MapPath(Constantes.DiretorioLogoLayoutEmail));

            objEmail.Attachments.Add(logo);

            string prefixo = "<!DOCTYPE html><html><head><title></title></head><body><div style=\"background-color:rgb(241, 241, 241); width: 100%; min-width:620px; table-layout:fixed; border-collapse:collapse; border-spacing:0px;\"> <table style=\"width: 60%; margin: auto; background-color:#ffffff\"> <tr> <td style=\"width:100%\"> <img src=\"cid:" + logo.ContentId + "\" style =\"margin:auto;display:block;width:50%;\"> </td></tr><tr> <td style=\"padding:50px;\">";
            string sufixo = " </td></tr><tr> <td style=\"padding: 50px; font-size:0.7em\"> <center> Rua Santos Dumont, 225 | Centro09715-120 | São Bernardo do Campo – SPPABX – (11) 4121-7200<br><br>ACR, segurança que você espera, qualidade que você confia. </center> </td></tr></table></div></body></html>";
            string message = " <h3 style=\"text-align:center\">Recuperação de Senha</h3> <p>Conforme o solicitado, segue uma nova senha para você acessar sua conta no Assistente de Cobranças:</p><br><p><strong>" + password + "</strong></p>";

            objEmail.Body = prefixo + message + sufixo;

            //'Para evitar problemas com caracteres "estranhos", configuramos o Charset para "ISO-8859-1"
            objEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            objEmail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            // 'Cria objeto com os dados do SMTP
            System.Net.Mail.SmtpClient objSmtp = new System.Net.Mail.SmtpClient();
            // 'Alocamos o endereço do host para enviar os e-mails  
            objSmtp.Host = configuracoesEmail.Servidor;
            objSmtp.Port = configuracoesEmail.Porta;
            objSmtp.EnableSsl = true;

            objSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            objSmtp.UseDefaultCredentials = true;
            objSmtp.Credentials = new System.Net.NetworkCredential(configuracoesEmail.Email, configuracoesEmail.Senha);

            // 'Enviamos o e-mail através do método .send()
            try
            {
                if (configuracoesEmail.Servidor == "smtp.office365.com")
                {
                    objSmtp.TargetName = "STARTTLS/smtp.office365.com";
                }
                objSmtp.Send(objEmail);

                sEnviou = true;
            }
            catch (Exception ex)
            {
                Log.Gravar(ex.Message, Log.TipoLog.Erro);
                sEnviou = false;
            }
            finally
            {
                //excluímos o objeto de e-mail da memória
                objEmail.Dispose();
            }


            return sEnviou;
        }
        #endregion
    }
}
