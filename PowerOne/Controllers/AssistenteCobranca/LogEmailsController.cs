﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Services.AssistenteCobranca;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class LogEmailsController : Controller
    {

        #region Serviços para consultas ao banco de dados
        private LogEmailService _logEmailService = new LogEmailService();

        private TypeModelService _typeModelService = new TypeModelService();

        private CollectionReportService _collectionReportService = new CollectionReportService();
        #endregion

        #region Index
        public ActionResult Index(string TitleCode, string CardCode, string DueDateFrom, string DueDateTo, string TotalFrom, string TotalTo)
        {
            if (((Login)Session["auth"]).Autorizacoes.RegistroEnvEmail == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.RegistroEnvEmail == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            List<LogEmail> listLogEmail = null;

            if (String.IsNullOrEmpty(TitleCode) && String.IsNullOrEmpty(CardCode)
                && String.IsNullOrEmpty(DueDateFrom) && String.IsNullOrEmpty(DueDateTo)
                && String.IsNullOrEmpty(TotalFrom) && String.IsNullOrEmpty(TotalTo))
            {
                listLogEmail = _logEmailService.List();
            }
            else
            {
                //Verificando se os valores numéricos envidos estão vazios
                if (TotalFrom == "")
                {
                    TotalFrom = "0";
                }
                if (TotalTo == "")
                {
                    TotalTo = "0";
                }

                listLogEmail = _logEmailService.SearchList(TitleCode, CardCode, DueDateFrom, DueDateTo, double.Parse(TotalFrom, CultureInfo.InvariantCulture), double.Parse(TotalTo, CultureInfo.InvariantCulture));
            }


            foreach (var item in listLogEmail)
            {
                item.Type = _typeModelService.GetByKey(item.TypeId);
            }

            return View(listLogEmail);
        }
        #endregion

        #region Cobranças
        public ActionResult Cobrancas(string TitleCode, string CardCode, string DueDate, string TotalFrom, string TotalTo)
        {
            if (((Login)Session["auth"]).Autorizacoes.RelatCobranças == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.RelatCobranças == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            List<CollectionReport> collectionReport = null;

            if (String.IsNullOrEmpty(TitleCode) && String.IsNullOrEmpty(CardCode)
                && String.IsNullOrEmpty(DueDate)
                && String.IsNullOrEmpty(TotalFrom) && String.IsNullOrEmpty(TotalTo))
            {
                collectionReport = _collectionReportService.List();
            }
            else
            {
                //Verificando se os valores numéricos envidos estão vazios
                if (TotalFrom == "")
                {
                    TotalFrom = "0";
                }
                if (TotalTo == "")
                {
                    TotalTo = "0";
                }

                collectionReport = _collectionReportService.Search(TitleCode, CardCode, DueDate, double.Parse(TotalFrom, CultureInfo.InvariantCulture), double.Parse(TotalTo, CultureInfo.InvariantCulture));
            }

            return View(collectionReport);
        }
        #endregion

    }
}