﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Services.AssistenteCobranca;
using PowerOne.Util;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class MailModelController : Controller
    {

        #region atributos
        private TypeModelService _typemodelService = new TypeModelService();
        private MailModelService _mailModelService = new MailModelService();
        private BPGroupService _bPGroupService = new BPGroupService();
        #endregion
        // GET: Email
        #region index
        public ActionResult Index()
        {
            if (((Login)Session["auth"]).Autorizacoes.ModeloEmail == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.ModeloEmail == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }
            List<MailModel> mailModel = new List<MailModel>();
            mailModel = _mailModelService.List();

            foreach (MailModel item in mailModel)
            {
                item.Type = _typemodelService.GetByKey(item.TypeId);
            }

            return View(mailModel);
        }
        #endregion

        #region detalhes
        public ActionResult Details(string id)
        {

            if (id == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }


            MailModel mailModel = _mailModelService.GetById(id);
            if (mailModel == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Código de Modelo de Email não encontrado!!" });
            }
            mailModel.Type = _typemodelService.GetByKey(mailModel.TypeId);

            return View(mailModel);
        }
        #endregion

        #region overview

        public ActionResult Overview()
        {

            List<OverviewViewModel> overviewList = new List<OverviewViewModel>();

            List<BPGroup> groups = _bPGroupService.List();

            foreach (var group in groups)
            {
                OverviewViewModel overview = new OverviewViewModel();
                overview.Group = group;

                Dictionary<string, MailModel> models = new Dictionary<string, MailModel>();
                MailModel mailModel = null;
                /*
                 * Pós-Faturamento 
                 */
                //Geral
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0001", "G");
                models.Add("G", mailModel);
                mailModel = null;

                //Boleto
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0001", "B");
                models.Add("B", mailModel);
                mailModel = null;

                //Transferencia
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0001", "T");
                models.Add("T", mailModel);
                mailModel = null;

                overview.PosFaturamento = models;
                models = new Dictionary<string, MailModel>();

                /*
                 * Alerta de Vencimento
                 */
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0002", "G");
                models.Add("G", mailModel);
                mailModel = null;

                //Boleto
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0002", "B");
                models.Add("B", mailModel);
                mailModel = null;

                //Transferencia
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0002", "T");
                models.Add("T", mailModel);
                mailModel = null;

                overview.AlertaVencimento = models;
                models = new Dictionary<string, MailModel>();

                /*
                 * Cobrança
                 */
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0003", "G");
                models.Add("G", mailModel);
                mailModel = null;

                //Boleto
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0003", "B");
                models.Add("B", mailModel);
                mailModel = null;

                //Transferencia
                mailModel = _mailModelService.GetByGroupAndType(group.CodeGroup, "T0003", "T");
                models.Add("T", mailModel);
                mailModel = null;

                overview.Cobranca = models;
                models = new Dictionary<string, MailModel>();

                overviewList.Add(overview);

            }

            return View(overviewList);

        }
        #endregion

        #region GET criar

        public ActionResult Create()
        {
            if (((Login)Session["auth"]).Autorizacoes.ModeloEmail == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.ModeloEmail == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            List<TypeModel> typesModel = _typemodelService.List();
            ViewData["Types"] = typesModel;

            return View();


        }
        #endregion

        #region POST criar
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(MailModel model)
        {
            if (((Login)Session["auth"]).Autorizacoes.CriarModeloEmail == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.CriarModeloEmail == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            //Verificando se existe pegando todas as categorias de modelo
            List<TypeModel> typesModel = _typemodelService.List();
            if (!ModelState.IsValid)
            {
                ViewData["Types"] = typesModel;
                return View(model);
            }

            //Verificando se existe
            if (_mailModelService.VerifyIfExist(model.Code))
            {
                string msg = "Código pertencente a um objeto já cadastrado.";
                Log.Gravar(msg, Log.TipoLog.Erro);
                ModelState.AddModelError("Code", msg);
                ViewData["Types"] = typesModel;
                return View(model);
            }

            //Verificando se nome já pertence a outro modelo
            if (_mailModelService.VerifyByName(model.Description))
            {
                string msg = "Descrição pertencente a um objeto já cadastrado.";
                Log.Gravar(msg, Log.TipoLog.Erro);
                ModelState.AddModelError("Description", msg);
                ViewData["Types"] = typesModel;
                return View(model);
            }

            string message = model.Message;

            _mailModelService.Create(model);
            TempData["Success"] = $"Modelo de Email {model.Code} cadastrado com sucesso!";

            return RedirectToAction("Index");
        }
        #endregion

        #region editar
        public ActionResult Edit(string id)
        {


            if (id == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }
            MailModel mailModel = _mailModelService.GetById(id);
            if (mailModel == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Código de Modelo de Email não encontrado!!" });
            }
            mailModel.Type = _typemodelService.GetByKey(mailModel.TypeId);

            if (mailModel == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Código de Modelo de Email não encontrado!!" });
            }

            List<TypeModel> typesModel = _typemodelService.List();
            ViewData["Types"] = typesModel;

            return View(mailModel);


        }
        #endregion

        [HttpPost]
        [ValidateInput(false)]

        #region editar
        public ActionResult Edit(MailModel model)
        {
            List<TypeModel> typesModel = _typemodelService.List();
            if (!ModelState.IsValid)
            {
                ViewData["Types"] = typesModel;
                return View(model);
            }

            _mailModelService.Edit(model);
            TempData["Success"] = $"Modelo de Email {model.Code} editado com sucesso!";
            return RedirectToAction("Index");
        }
        #endregion

        #region deletar
        public ActionResult Delete(string id)
        {


            if (id == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: Código de objeto não informado!" });
            }

            //Verificando se o modelo está vinculado a um grupo de parceiros de negócio
            if (_mailModelService.VerifyIfIsLinkedGroup(id))
            {
                TempData["Error"] = "O Modelo de Email já está vinculado a um Grupo de Parceiros de Negócio.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index");
            }

            //verificando se o modelo de email já foi utilizado para disparo de emails
            if (_mailModelService.VerifyIfWasSended(id))
            {
                TempData["Error"] = "Modelo de Email já foi utilizado em envios.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index");
            }

            MailModel mailModel = _mailModelService.GetById(id);
            if (mailModel == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Código de Modelo de Email não encontrado!!" });
            }
            mailModel.Type = _typemodelService.GetByKey(mailModel.TypeId);

            if (mailModel == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Código de Modelo de Email não encontrado!!" });
            }

            return View(mailModel);


        }
        #endregion

        #region delete

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteModel(string code)
        {
            _mailModelService.Delete(code);
            TempData["Success"] = $"Modelo de Email {code} excluído com sucesso!";

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region error

        public ActionResult Error(string message)
        {
            ErrorViewModel viewModel = new ErrorViewModel
            {
                Message = message
            };

            return View(viewModel);
        }
        #endregion
    }
}