﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Services.AssistenteCobranca;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class SendingWizardController : Controller
    {
        private TypeModelService _typeModelService = new TypeModelService();

        // GET: SendingWizard
        #region Index
        public ActionResult Index()
        {
            if (((Login)Session["auth"]).Autorizacoes.AssistEnvio == "N" && ((Login)Session["auth"]).Autorizacoes.Grupo.AssistEnvio == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }
            //Pegando os tipos de pós-faturamento e cobrança
            List<TypeModel> typesModel = new List<TypeModel>();
            typesModel.Add(_typeModelService.GetByKey("T0003"));
            typesModel.Add(_typeModelService.GetByKey("T0002"));

            ViewData["Types"] = typesModel;

            return View();

        }
        #endregion

    }
}