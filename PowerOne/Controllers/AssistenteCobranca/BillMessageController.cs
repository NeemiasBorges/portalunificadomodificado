﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.Enums;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Services;
using PowerOne.Services.AssistenteCobranca;
using PowerOne.Util;
using SAPbobsCOM;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class BillMessageController : Controller
    {

        #region Atributos
        //Serviços para consultas com o Banco de Dados
        private BillMessageService _billMessageService = new BillMessageService();
        private MailModelService _mailModelService = new MailModelService();
        private TypeModelService _typeModelService = new TypeModelService();
        private LogEmailService _logEmailService = new LogEmailService();
        private ContactEmployeeService _contactEmployee = new ContactEmployeeService();
        private BPGroupService _bpGroupService = new BPGroupService();
        private SendingWizardService _sendingWizardService = new SendingWizardService();
        private BusinessPartnerService _businessPartner = new BusinessPartnerService();
        private ConfiguracoesService _configuracoesEmail = new ConfiguracoesService();
        #endregion

        #region Utilitarios
        //Utilitários para tarefas específicas
        private MailModelUtil _mailModelUtil = new MailModelUtil();

        //Envio de Emails
        private Email _email = new Email();
        #endregion

        #region Index
        public ActionResult Index()
        {

            SendingWizardViewModel billParams = (SendingWizardViewModel)TempData["params"];

            if (billParams == null)
            {
                return RedirectToAction("Index", "SendingWizard");
            }


            List<BillMessage> billMessages = GetAllBillMessages(billParams, null, null);

            ViewData["typeMailView"] = billParams.MailTypeCode;

            foreach (var item in billMessages)
            {
                TimeSpan daysDifference = item.DocDueDate - DateTime.Now;
                item.DifferenceDueDateAndToday = daysDifference.Days;
            }


            return View(billMessages);
        }
        #endregion

        #region Filter
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Filter(SendingWizardViewModel billParams)
        {
            List<BPGroup> groupsLinkedBP = _bpGroupService.GetGropLikedBP();
            bool modelosConfigurados = true;

            foreach (var item in groupsLinkedBP)
            {
                if (!_sendingWizardService.VerificarGruposPossuiTodosModelos(item))
                {
                    modelosConfigurados = false;
                    break;
                }
            }

            if (_businessPartner.CountClientsWithouGroup() != 0)
            {
                TempData["Error"] = "Todos os Clientes devem pertercer a algum Grupo.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index", "SendingWizard");
            }

            if (modelosConfigurados == true)
            {
                //Passando parâmetros de filtragem para a página de exibir as contas a Receber
                TempData["params"] = billParams;
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Error"] = "Configure todos os Modelos de Email dos Grupos que estão vinculados aos Clientes";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index", "SendingWizard");
            }
        }
        #endregion

        #region SendMail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendMail(string[] billChecked, string[] contactChecked)
        {
            List<string> billsWithoutEmail = new List<string>();
            ConfiguracoesEmail configuracoes = _configuracoesEmail.GetConfiguracoesEmail();

            if (configuracoes == null)
            {
                TempData["Error"] = "Dados de email não configurados, acesse a seção de configurações!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                if (billChecked != null)
                {
                    //Pegando todas as Contas com os códigos contidos em billChecked
                    List<BillMessage> bills = GetAllBillMessages(null, billChecked, contactChecked);


                    //bills.Where(item => item.EmailAdress == "")
                    //.Select(item => item.CardName)
                    //.GroupBy(item => item.CardName)
                    //.ToList();

                    //Pegando os dados para registrar o Log de Email
                    foreach (var item in bills)
                    {
                        //Lista de emails para serem enviados cópias
                        List<string> emailsCC = new List<string>();
                        foreach (var itemEmail in item.EmailContactsCopyLists)
                        {
                            //verificando se o contato está selecionado no assistente de email
                            if (contactChecked != null)
                            {
                                if (contactChecked.Contains($"{item.DocCode}-{itemEmail.Id}"))
                                {
                                    emailsCC.Add(itemEmail.Email);
                                }
                            }

                        }

                        //Verificando se o cliente possui email cadastrado e disparando os emails
                        bool Enviado = false;
                        if (!String.IsNullOrEmpty(item.EmailAdress))
                        {
                            Enviado = _email.EnviaEmail(item, configuracoes);
                        }
                        else
                        {
                            billsWithoutEmail.Add(item.CardName);
                        }


                        LogEmail logEmail = new LogEmail()
                        {
                            BillCode = item.DocCode,
                            DueDate = item.DocDueDate,
                            BillTotal = item.DocTotal,
                            TypeId = item.MailModel.TypeId,
                            MailModelId = item.MailModelCode,
                            SendingDate = DateTime.Now,
                            EmailAdress = item.EmailAdress,
                            EmailContactsCopyLists = emailsCC.ToArray(),
                            Subject = item.MailModel.Subject,
                            Message = item.MailModel.Message,
                            CardCode = item.CardCode
                        };

                        if (Enviado)
                        {
                            logEmail.Status = "Success";
                            TempData["Success"] = "Mensagens enviadas com sucesso!";
                        }
                        else
                        {
                            logEmail.Status = "Failed";
                            TempData["Error"] = "Houve um erro ao enviar os emails, consulte os Registros de Envio!";
                            Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                        }

                        _logEmailService.Create(logEmail);

                    }
                }
                else
                {
                    TempData["Warning"] = "Nenhum título selecionado para o envio de emails!";
                }

                if (billsWithoutEmail.Count != 0)
                {
                    billsWithoutEmail.Distinct();

                    TempData["ClientesSemEmail"] = billsWithoutEmail;
                }
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region GetAllBillMessages
        /*
         * Método utilizado para pegar os modelos na base de dados e montar a mensagem
         * Ela pode receber parâmetros enviados pelo SendingWizard ou um conjunto de códigos 
         * selecionados na listagem das contas
         * 
         * 
         * Toda a complexidade de montagem dos dados para a listagem ou envio de emails se encontra aqui
         * Isso para que a complexidade fique somente em uma função, facilitando eventuais manutenções
         */
        public List<BillMessage> GetAllBillMessages(SendingWizardViewModel billParams, string[] codes, string[] contacts)
        {

            List<BillMessage> billMessages = null;

            //verificando se é para pegar de acordo com parâmetros ou por códigos específicos
            if (codes == null)
            {
                billMessages = _billMessageService.List(billParams);
            }
            else//Else para quando há filtros para consultar os títulos(Disparo de Emails)
            {
                billMessages = _billMessageService.GetByCollectionKeys(codes);
            }


            if (billMessages.Count != 0)
            {
                foreach (var billMessage in billMessages)
                {
                    int days = (int)billMessage.DocDueDate.Subtract(DateTime.Today).TotalDays;

                    //Adicionando os Contact Employees 
                    billMessage.EmailContactsCopyLists = _contactEmployee.List(billMessage.CardCode);

                    //verificando se há filtrao para contatos
                    if (contacts != null)
                    {
                        //Declarando uma lista para filtrar os contatos do email
                        List<ContactEmployee> listEmails = new List<ContactEmployee>(billMessage.EmailContactsCopyLists);

                        foreach (var item in billMessage.EmailContactsCopyLists)
                        {
                            if (!contacts.Contains($"{billMessage.DocCode}-{item.Id}"))
                            {
                                listEmails.Remove(item);
                            }
                        }

                        billMessage.EmailContactsCopyLists = listEmails;
                    }

                    if (days >= 0)
                    {
                        billMessage.MailModel = _mailModelService.GetByGroupAndType(billMessage.GroupCode, "T0002", billMessage.PayMethod);
                        billMessage.MailModelCode = billMessage.MailModel.Code;
                    }
                    else if (days < 0)
                    {
                        billMessage.MailModel = _mailModelService.GetByGroupAndType(billMessage.GroupCode, "T0003", billMessage.PayMethod);
                        billMessage.MailModelCode = billMessage.MailModel.Code;
                    }
                    billMessage.MailModel.Type = _typeModelService.GetByKey(billMessage.MailModel.TypeId);

                    //Capturando variáveis e montando a mensagem do Email Final
                    billMessage.MailModel.Message = _mailModelUtil.BuildMessage(billMessage, BuildTextMailType.MessageBody);
                    billMessage.MailModel.Subject = _mailModelUtil.BuildMessage(billMessage, BuildTextMailType.MessageSubject);
                }
            }

            return billMessages;
        }
        #endregion
    }
}