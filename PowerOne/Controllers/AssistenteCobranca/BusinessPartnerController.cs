﻿using PowerOne.Models;
using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.ViewModel;
using PowerOne.Services.AssistenteCobranca;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PowerOne.Controllers.AssistenteCobranca
{
    public class BusinessPartnerController : Controller
    {
        private BusinessPartnerService _businessPartnerService = new BusinessPartnerService();
        private BPGroupService _bpGroup = new BPGroupService();
        private ContactEmployeeService _contactEmployeeService = new ContactEmployeeService();

        // GET: BusinessPartner
        #region Index
        public ActionResult Index(string CardCode, string CardName, string CodeGroup)
        {

            List<BusinessPartner> bps;
            if (String.IsNullOrEmpty(CardCode) && String.IsNullOrEmpty(CardName) && String.IsNullOrEmpty(CodeGroup))
            {
                bps = _businessPartnerService.List();
            }
            else
            {
                //Para pegar todos os PN sem grupo
                if (CodeGroup == "null")
                {
                    CodeGroup = null;
                }
                BusinessPartner bpParams = new BusinessPartner() { CardCode = CardCode, CardName = CardName, CodeGroup = CodeGroup };
                bps = _businessPartnerService.Search(bpParams);
            }

            ViewData["BusinessPartner"] = bps;


            List<BPGroup> bpGroup = _bpGroup.List();
            //Para pegar todos os PN sem grupo
            bpGroup.Insert(0, new BPGroup()
            {
                CodeGroup = "null",
                DescGroup = ""
            });

            bpGroup.Insert(0, new BPGroup()
            {
                CodeGroup = "",
                DescGroup = "Selecione um Grupo"
            });
            ViewData["Groups"] = bpGroup;

            return View();
        }
        #endregion

        #region Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BusinessPartner bp)
        {
            _businessPartnerService.Edit(bp);
            TempData["Success"] = $"O Grupo {bp.CodeGroup} foi atribuído ao Cliente {bp.CardCode}.";

            List<BusinessPartner> bps = _businessPartnerService.List();
            List<BPGroup> bpGroup = _bpGroup.List();
            ViewData["Groups"] = bpGroup;

            return RedirectToAction("Index");
        }
        #endregion

        #region ContactEmployee
        public ActionResult ContactEmployee(string id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Error), new { message = "Página não encontrada: parceiro de negócios não especificado!" });
            }
            List<ContactEmployee> contactEmployee = _contactEmployeeService.List(id);

            if (contactEmployee.Count == 0)
            {
                TempData["Error"] = $"O Cliente {id} não possui pessoas de contato.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index");
            }

            return View(contactEmployee);
        }
        #endregion

        #region SetContacts
        [HttpPost]
        public ActionResult SetContacts(string[] contactChecked, string CardCode)
        {
            _contactEmployeeService.SetForUse(contactChecked, CardCode);

            return RedirectToAction("ContactEmployee", "BusinessPartner", new { id = CardCode });
        }
        #endregion

        #region Error
        public ActionResult Error(string message)
        {
            ErrorViewModel viewModel = new ErrorViewModel
            {
                Message = message
            };

            return View(viewModel);
        }
        #endregion
    }
}