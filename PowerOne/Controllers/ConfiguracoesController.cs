﻿using PowerOne.Models;
using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class ConfiguracoesController : Controller
    {
        #region Serviços
        private ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();
        #endregion

        #region Configurar Email
        public ActionResult Email()
        {
            if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            ConfiguracoesEmail configuracoesEmail = _configuracoesService.GetConfiguracoesEmail();

            return View(configuracoesEmail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Email(ConfiguracoesEmail configEmail)
        {
            string msg = _configuracoesService.ConfigurarEmail(configEmail);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Email Configurado com Sucesso!";
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Anexo(ConfiguracoesEmail confAnexo)
        {
            string msg = _configuracoesService.ConfiguraAnexo(confAnexo);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Anexos do Email Configurados com Sucesso!";
            }

            return RedirectToAction(nameof(Email));
        }
        #endregion

        #region Temas
        public ActionResult Temas()
        {
            return View();
        }
        #endregion

        #region Configurações de Documento
        public ActionResult DocConfig()
        {
            if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            TempData["DimensoesCtCusto"] = _configuracoesService.PegarConfiguracoesCentroCusto();

            return View();
        }
        #endregion

        #region Configurar Modelos RPT para impressao
        public ActionResult
            ConfigurarModelosImpressao(
            HttpPostedFileBase solicitacao,
            HttpPostedFileBase pedido,
            HttpPostedFileBase requerimentoEstoque,
            HttpPostedFileBase reembolso)
        {
            string msg = null;

            try
            {
                if (solicitacao != null)
                {
                    string fileName = Path.GetFileName(solicitacao.FileName);
                    string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

                    solicitacao.SaveAs(path);

                    msg = _configuracoesService.ConfigurarModeloImpressao("SC", "Modelo p/ Solicitação de Compras", path);
                }
                if (pedido != null)
                {
                    string fileName = Path.GetFileName(pedido.FileName);
                    string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

                    pedido.SaveAs(path);

                    msg = _configuracoesService.ConfigurarModeloImpressao("PC", "Modelo p/ Pedido de Compra", path);
                }
                if (requerimentoEstoque != null)
                {
                    string fileName = Path.GetFileName(requerimentoEstoque.FileName);
                    string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

                    requerimentoEstoque.SaveAs(path);

                    msg = _configuracoesService.ConfigurarModeloImpressao("RE", "Modelo p/ Requerimento de Estoque", path);
                }
                if (reembolso != null)
                {
                    string fileName = Path.GetFileName(reembolso.FileName);
                    string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

                    reembolso.SaveAs(path);

                    msg = _configuracoesService.ConfigurarModeloImpressao("RB", "Modelo p/ Reembolso", path);
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return RedirectToAction(nameof(DocConfig));
        }
        #endregion

        #region Trocar Icones
        [HttpPost]
        public ActionResult ChangeIcons(string icon)
        {                    
                if (icon != null)
                {
                    string msg = _configuracoesService.ConfigurarTema(((Models.Login)Session["auth"]).Id.ToString(), null, icon);

                    if (msg != null)
                    {
                        TempData["Error"] = msg;
                        Log.Gravar(msg, Log.TipoLog.Erro);
                    }
                    else
                    {
                        ((Models.Login)Session["auth"]).Tema = _configuracoesService.GetTema(((Models.Login)Session["auth"]).Id.ToString());
                        TempData["Success"] = "icones alterados com sucesso!";
                    }
                }
                else
                {
                    TempData["Error"] = "Selecione um icone!";
                    Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                }

                return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Upload de Logo
        [HttpPost]
        public ActionResult UploadLogo(HttpPostedFileBase logo)
        {
            string msg = null;
            try
            {
                if (logo != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Content/img"), "logo-company.png");
                    logo.SaveAs(path);
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Logo do site alterada com sucesso!";
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Configurar Cor do Menu
        [HttpPost]
        public ActionResult TrocarCorMenu(string color)
        {
            if (color != null)
            {
                string msg = _configuracoesService.ConfigurarTema(((Models.Login)Session["auth"]).Id.ToString(), color, null);

                if (msg != null)
                {
                    TempData["Error"] = msg;
                    Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                }
                else
                {
                    ((Models.Login)Session["auth"]).Tema = _configuracoesService.GetTema(((Models.Login)Session["auth"]).Id.ToString());
                    TempData["Success"] = "Cor do menu alterado com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione uma cor!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Configura Centros de Custo
        [HttpPost]
        public ActionResult CtCustoConfig(int[] ctCusto)
        {
            string msg = _configuracoesService.ConfigurarCentroCusto(ctCusto);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Configurações de dimensões de centro de custo alterada com sucesso!";
            }

            return RedirectToAction(nameof(DocConfig));
        }
        #endregion

        #region Autorizações
        public ActionResult Autorizacoes()
        {
            if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            TempData["autorizacoes"] = _autorizacoesService.List();
            TempData["autorizacoesGrupo"] = _autorizacoesService.ListGrupo();

            return View();
        }

        [HttpPost]
        public ActionResult Autorizacoes(Autorizacoes autorizacoes)
        {
            string msg = _autorizacoesService.Update(autorizacoes);
            
            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = $"Autorizações de {autorizacoes.UserName} atualizadas com sucesso.";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
            }

            return RedirectToAction(nameof(Autorizacoes));
        }
        #endregion

        #region Autorizacoes para Grupo
        public ActionResult AtualizarAutorizacoesGrupo(AutorizacoesGrupo autorizacoes)
        {
            string msg = _autorizacoesService.AtualizarAutorizacoesGrupos(autorizacoes);

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Autorização atualizada com sucesso!";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
            }

            return RedirectToAction("Autorizacoes");
        }
        #endregion

        #region Adicionar/Atualizar o grupo de um usuário
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AtualizarGrupoUsuario(string idUser, string idGrupo)
        {
            string msg = null;
            if (String.IsNullOrEmpty(idGrupo))
            {
                msg = "Selecione um grupo.";
            }
            else
            {
                msg = _autorizacoesService.AtualizarGrupoDoUsuario(idUser, idGrupo);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Grupo de usuário adicionado com sucesso.";
            }


            return RedirectToAction(nameof(Autorizacoes));
        }

        #endregion
    }
}