﻿using PowerOne.Services.AssistenteCobranca;
using PowerOne.Services.Compras;
using PowerOne.Services.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region services
        PedidoCompraService _pedidoCompraService = new PedidoCompraService();
        SolicitacaoCompraService __solicitacaoService = new SolicitacaoCompraService();
        RequerimentoEstoqueService __estoqueService = new RequerimentoEstoqueService();
        MailModelService _mailModelService = new MailModelService();
        BPGroupService _BPGroupService = new BPGroupService();
        LogEmailService _logEmailService = new LogEmailService();

        #endregion

        #region overview
        public ActionResult Index()
        {
            if (((PowerOne.Models.Login)Session["auth"]) == null)
            {
                return RedirectToAction("Index", "LoginController");
            }

            TempData["consultaModelos"] = _mailModelService.Contar();
            TempData["grupos"] = _BPGroupService.Contar();
            TempData["registrosMes"] = _logEmailService.ContarRegistroMes();
            TempData["consultasPedido"] = _pedidoCompraService.contarRegistrosPedido();
            TempData["consultasSolicitacao"] = __solicitacaoService.contarRegistrosSolicita();
            TempData["estoque"] = __estoqueService.contEstoque();

            return View();
        }
        #endregion

        #region assistente de cobranca
        public ActionResult AssistenteCobranca()
        {
            TempData["consultaModelos"] = _mailModelService.Contar();
            TempData["grupos"] = _BPGroupService.Contar();
            TempData["registrosMes"] = _logEmailService.ContarRegistroMes();

            return View();
        }
        #endregion

        #region estoque
        public ActionResult Estoque()
        {
            TempData["estoque"] = __estoqueService.contEstoque();
            return View();
        }
        #endregion

        #region compras
        public ActionResult Compras()
        {
            TempData["consultasPedido"] = _pedidoCompraService.contarRegistrosPedido();
            TempData["vencimentosPedidos"] = _pedidoCompraService.vencimentos();
            TempData["consultasSolicitacao"] = __solicitacaoService.contarRegistrosSolicita();
            TempData["vencimentosSolicitacoes"] = __solicitacaoService.vencimentos();
            return View();
        }
        #endregion

        #region Timesheet
        public ActionResult Timesheet()
        {
            return View();
        }
        #endregion

        #region Financeiro

        public ActionResult Reembolso()
        {
            TempData["consultasPedido"] = _pedidoCompraService.contarRegistrosPedido();
            return View();
        }

        #endregion

        #region Configurações
        public ActionResult Configuracoes()
        {
            return View();
        }

        #endregion

        #region Não Autorizado
        public ActionResult NaoAutorizado()
        {
            return View();
        }
        #endregion

    }
}