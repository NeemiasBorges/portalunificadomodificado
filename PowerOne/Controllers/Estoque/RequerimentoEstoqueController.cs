﻿using Microsoft.Ajax.Utilities;
using PowerOne.Models;
using PowerOne.Models.Enums;
using PowerOne.Models.Estoque;
using PowerOne.Services;
using PowerOne.Services.Estoque;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PowerOne.Controllers.Estoque
{
    [Authorize]
    public class RequerimentoEstoqueController : Controller
    {
        #region Serviços (Consultas ao Banco de Dados)
        RequerimentoEstoqueService _requerimentoEstoqueService = new RequerimentoEstoqueService();
        ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();
        DepositoService _depositoService = new DepositoService();
        FiliaisService _filiaisService = new FiliaisService();
        ImpressaoService _impressaoService = new ImpressaoService();
        ConfiguracoesService __configuracoesService = new ConfiguracoesService();
        #endregion

        #region Exibir Requerimentos com Filtro
        public ActionResult Index(string dtInicio, string dtFim, string status)
        {
            if (((Login)Session["Auth"]).Autorizacoes.ReqEstoque == "N" 
                && ((Login)Session["Auth"]).Autorizacoes.Grupo.ReqEstoque == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            if (String.IsNullOrEmpty(dtInicio) && String.IsNullOrEmpty(dtFim))
            {
                if (status == null)
                {
                    status = "T";
                }
                List<RequerimentoEstoque> requerimentos = _requerimentoEstoqueService.List((Login)Session["auth"], null, null, status);
                return View(requerimentos);
            }
            else
            {
                List<RequerimentoEstoque> requerimentos = _requerimentoEstoqueService.List((Login)Session["auth"], dtInicio, dtFim, status);
                return View(requerimentos);
            }
        }
        #endregion

        #region Detalhes do Requerimento de Estoque
        public ActionResult Details(int DocEntry)
        {
            if (((Login)Session["Auth"]).Autorizacoes.ReqEstoque == "N" 
                && ((Login)Session["Auth"]).Autorizacoes.Grupo.ReqEstoque == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            RequerimentoEstoque requerimento = _requerimentoEstoqueService.GetByKey(DocEntry);
            /* (Id do Requerimento,lista de linhas que se deseja capturar) */
            List<RequerimentoEstoqueLinhas> requerimentoEstoqueLinhas = _requerimentoEstoqueService.ListarLinhas(DocEntry, null);

            requerimento.Itens = requerimentoEstoqueLinhas;

            return View(requerimento);
        }
        #endregion

        #region Aprovar Requisição de Estoque
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AprovarRequisicao(string[] linesChecked, string observacoes, int DocEntry)
        {
            string msgErro = _requerimentoEstoqueService.AprovarRequerimento((Login)Session["auth"], linesChecked, observacoes, DocEntry);

            if (msgErro != null)
            {
                TempData["Error"] = msgErro;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = $"Requerimento de saída de estoque {DocEntry} aprovada com sucesso";
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Fechar Requerimento de Estoque
        public ActionResult Fechar(int DocEntry)
        {
            RequerimentoEstoque requerimentoEstoque = _requerimentoEstoqueService.GetByKey(DocEntry);

            return View(requerimentoEstoque);
        }

        [HttpPost]
        public ActionResult FecharRequerimento(int DocEntry)
        {
            string msg = _requerimentoEstoqueService.FecharRequerimento(DocEntry, null);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Requerimento de Estoque fechado com sucesso!";
            }

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Criar Requisição de Estoque
        public ActionResult Create()
        {
            if (((Login)Session["Auth"]).Autorizacoes.CriarReqEstoque == "N" 
                && ((Login)Session["Auth"]).Autorizacoes.Grupo.CriarReqEstoque == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }
            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<Filiais> filiais = _filiaisService.List();

            TempData["filiais"] = filiais;
            TempData["ItensEstoque"] = itens;

            return View();
        }

        [HttpPost]
        public ActionResult Add(RequerimentoEstoque ReqEstoque)
        {
            string msg = null;
            var resultado = new { msg = "", type = "" };

            if (ReqEstoque.Itens == null)
            {
                msg = "O Requerimento de Estoque deve ter pelo menos um item!";
                Log.Gravar(msg, Log.TipoLog.Erro);
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

            msg = _requerimentoEstoqueService.Create(ReqEstoque,(Login)Session["auth"]);

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };
            }
            else
            {
                resultado = new
                {
                    msg = "Requerimento de Estoque adicionado com sucesso",
                    type = "success"
                };
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Imprimir Requerimento de Estoque
        public ActionResult Imprimir(int DocEntry)
        {
            string path = __configuracoesService.GetDiretorioModeloImpressao("RE");

            if (!System.IO.File.Exists(path))
            {
                TempData["Error"] = "Arquivo não existente! Configure o Modelo de Impressão novamente!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }

            if (path != null)
            {
                string file = _impressaoService.Imprimir(DocEntry, path, TipoDocumento.RequerimentoEstoque);
                string content_type = "application/pdf";
                string nameFile = $"REQUERIMENTO_ESTOQUE_{DocEntry}.pdf";


                return File(file, content_type, nameFile);
            }
            else
            {
                TempData["Error"] = "Modelo Crystal Reports não configurado, acesse as configurações de documentos.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }
        }
        #endregion
    }
}