﻿using PowerOne.Models;
using PowerOne.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    public class DepositoController : Controller
    {
        #region Serviços (Consultas ao Banco de Dados)

        DepositoService _depositoService = new DepositoService();

        #endregion

        #region Index
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Listar Depósitos
        public JsonResult ListDepositos(int Filial)
        {
            List<Deposito> depositos = _depositoService.List(Filial);

            return Json(depositos, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}