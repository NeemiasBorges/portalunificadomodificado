﻿using PowerOne.Models;
using PowerOne.Models.Enums;
using PowerOne.Models.Reembolso;
using PowerOne.Services;
using PowerOne.Services.Reembolso;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PowerOne.Controllers.Reembolso
{
    [Authorize]
    public class SolicitacaoReembolsoController : Controller
    {

        #region Serviços (Consultas ao Banco de Dados)
        SolicitacaoReembolsoService _solicitacaoReembolsoService = new SolicitacaoReembolsoService();

        ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();

        ListBPService _bussinesPartner = new ListBPService();

        FiliaisService _filiaisService = new FiliaisService();

        DepositoService _depositoService = new DepositoService();

        ProjetosService _projetosService = new ProjetosService();

        CentroCustoService _centroCustoService = new CentroCustoService();

        ImpressaoService _impressaoService = new ImpressaoService();

        ConfiguracoesService _configuracoesService = new ConfiguracoesService();

        AnexosService _anexosService = new AnexosService();
        #endregion

        #region Exibir Reembolsos
        public ActionResult Index(string DataInicio, string DataFim, string Status)
        {
            if (((Login)Session["auth"]).Autorizacoes.Reembolso == "N" 
                && ((Login)Session["auth"]).Autorizacoes.Grupo.Reembolso == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            if (String.IsNullOrEmpty(DataInicio) && String.IsNullOrEmpty(DataFim))
            {
                if (Status == null)
                {
                    Status = "T";
                }
                List<SolicitacaoReembolso> reembolsos = _solicitacaoReembolsoService.SearchList((Login)Session["auth"], null, null, Status);
                return View(reembolsos);
            }
            else
            {
                List<SolicitacaoReembolso> solicitacaos = _solicitacaoReembolsoService.SearchList((Login)Session["auth"], DataInicio, DataFim, Status);

                return View(solicitacaos);
            }
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if (((Login)Session["auth"]).Autorizacoes.CriarReembolso == "N" 
                && ((Login)Session["auth"]).Autorizacoes.Grupo.CriarReembolso == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<ListBP> bps = _bussinesPartner.List();
            List<Filiais> filiais = _filiaisService.List();
            List<Deposito> depositos = _depositoService.List(1);
            List<Projetos> projetos = _projetosService.List();

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            TempData["ItensEstoque"] = itens;
            TempData["ListBP"] = bps;
            TempData["filiais"] = filiais;
            TempData["depositos"] = depositos;
            TempData["projetos"] = projetos;

            TempData["DimensoesCtCusto"] = dimensoesCtCusto;

            return View();
        }

        [HttpPost]
        public JsonResult Add(SolicitacaoReembolso reembolso)
        {
            var resultado = new { msg = "", type = "" };

            if (reembolso.Itens == null)
            {
                string errMsg = "O Reembolso deve ter pelo menos um item!";
                Log.Gravar(errMsg, Log.TipoLog.Erro);
                resultado = new
                {
                    msg = errMsg,
                    type = "error"
                };

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

            string msg = _solicitacaoReembolsoService.Create(reembolso, (Login)Session["auth"]);

            if (msg != null)
            {
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };
            }
            else
            {
                resultado = new
                {
                    msg = "Documento adicionado com sucesso!",
                    type = "success"
                };
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Aprovar Reembolso
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Anexo(string observacoes, int DocEntry)
        {

            bool confirmaReembolso = _solicitacaoReembolsoService.AddAnexo(observacoes, DocEntry, (Login)Session["auth"]);
            if (confirmaReembolso == true)
            {
                TempData["Success"] = "O Reembolso foi aprovado!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Error"] = "Ocorreu uma falha no processo";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Detalhes do Reembolso
        public ActionResult Details(int DocEntry)
        {
            if (((Login)Session["auth"]).Autorizacoes.Reembolso == "N" 
                && ((Login)Session["auth"]).Autorizacoes.Grupo.Reembolso == "N")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            SolicitacaoReembolso solicitacaoReembolso = _solicitacaoReembolsoService.GetByKey(DocEntry);
            solicitacaoReembolso.ListaAnexos = _anexosService.GetAnexo(DocEntry, TipoDocumento.Reembolso);

            return View(solicitacaoReembolso);
        }
        #endregion

        #region Fechar Status do Reembolso
        public ActionResult FecharStatus(int DocEntry)
        {
            string msg = _solicitacaoReembolsoService.FecharStatus(DocEntry);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Status do Documento Alterado com sucesso!";
            }

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Deletar Linhas Reembolso
        public ActionResult DeleteLinha(int id, int line)
        {
            return RedirectToAction("Details", new { DocEntry = id });

            //return RedirectToAction("Details", new { DocEntry = id });
        }
        #endregion

        #region Imprimir Pedido de Compra
        public ActionResult Imprimir(int DocEntry)
        {
            string path = _configuracoesService.GetDiretorioModeloImpressao("RB");

            if (!System.IO.File.Exists(path))
            {
                TempData["Error"] = "Arquivo não existente! Configure o Modelo de Impressão novamente!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }

            if (path != null)
            {
                string file = _impressaoService.Imprimir(DocEntry, path, TipoDocumento.Reembolso);
                string content_type = "application/pdf";
                string nameFile = $"REEMBOLSO_{DocEntry}.pdf";


                return File(file, content_type, nameFile);
            }
            else
            {
                TempData["Error"] = "Modelo Crystal Reports não configurado, acesse as configurações de documentos.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }
        }
        #endregion

        #region Editar Anexo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarAnexo(HttpPostedFileBase file, int DocEntry, int idAnexos)
        {
            string msg = null;
            string fileName = Path.GetFileName(file.FileName);
            string ext = Path.GetExtension(file.FileName).Substring(1);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Files/Anexos"), fileName);
            file.SaveAs(path);

            List<Arquivo> Anexos = new List<Arquivo>();
            Anexos.Add(new Arquivo() { Nome = name, Extensao = ext, Diretorio = path });

            if (idAnexos == 0)
            {
                msg = _anexosService.InserirAnexos(Anexos, DocEntry, Models.Enums.TipoDocumento.Reembolso);
            }
            else
            {
                msg = _anexosService.EditarAdicionandoAnexos(Anexos, DocEntry, idAnexos, Models.Enums.TipoDocumento.Reembolso);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Anexo adicionado com sucesso!";
            }

            return RedirectToAction(nameof(Details), new { DocEntry = DocEntry });
        }
        #endregion
    }
}