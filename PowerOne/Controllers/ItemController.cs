﻿using PowerOne.Models;
using PowerOne.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    public class ItemController : Controller
    {
        #region Serviços de Consultas ao Banco de Dados
        ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();
        #endregion

        public ActionResult Search(string itemName)
        {
            List<ItemEstoque> items = _itemEstoqueService.Search(itemName);

            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }
}