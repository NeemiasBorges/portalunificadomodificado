﻿using PowerOne.Services.Compras;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using PowerOne.Models;
using PowerOne.Models.Compras;
using PowerOne.Services;
using PowerOne.Models.Compras.Enums;
using PowerOne.Models.Enums;
using System.Linq;
using PowerOne.Util;
using System.Web;
using System.IO;

namespace PowerOne.Controllers.Compras
{
    [Authorize]
    public class PedidoComprasController : Controller
    {
        #region Serviços (Consultas ao Banco de Dados)
        PedidoCompraService _pedidoCompraService = new PedidoCompraService();

        ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();

        ListBPService _bussinesPartner = new ListBPService();

        FiliaisService _filiaisService = new FiliaisService();

        DepositoService _depositoService = new DepositoService();

        CentroCustoService _centroCustoService = new CentroCustoService();

        ImpressaoService _impressaoService = new ImpressaoService();

        ConfiguracoesService _configuracoesService = new ConfiguracoesService();

        AnexosService _anexosService = new AnexosService();
        #endregion

        #region Exibir Pedidos de Compra
        public ActionResult Index(string DataInicio, string DataFim, string Status)
        {
            if ((((Login)Session["auth"]).Autorizacoes.PedCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.PedCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            if (String.IsNullOrEmpty(DataInicio) && String.IsNullOrEmpty(DataFim))
            {
                if (Status == null)
                {
                    Status = "T";
                }
                List<PedidoCompra> pedidos = _pedidoCompraService.SearchListPedidos((Login)Session["auth"], null, null, Status);
                return View(pedidos);
            }
            else
            {

                List<PedidoCompra> pedidos = _pedidoCompraService.SearchListPedidos((Login)Session["auth"], DataInicio, DataFim, Status);

                return View(pedidos);
            }

        }
        #endregion

        #region Detalhes do Pedido
        public ActionResult Details(int DocEntry, int BPL_IDAssignedToInvoice)
        {
            if ((((Login)Session["auth"]).Autorizacoes.PedCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.PedCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<Deposito> depositos = _depositoService.List(BPL_IDAssignedToInvoice);
            TempData["ItensEstoque"] = itens;
            TempData["depositos"] = depositos;

            PedidoCompra pedidoCompra = _pedidoCompraService.GetByKey(DocEntry);
            pedidoCompra.BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice;
            pedidoCompra.ListaAnexos = _anexosService.GetAnexo(DocEntry,TipoDocumento.PedidoCompra);


            return View(pedidoCompra);
        }
        #endregion

        #region Conferimento Linha
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AprovaLinha(double U_CONFPEDIDO, string U_ObsConf, int DocEntry, int VisOrder, int BPL_IDAssignedToInvoice)
        {
            string msg = _pedidoCompraService.AprovarLinha(U_CONFPEDIDO, U_ObsConf, DocEntry, VisOrder, (Login)Session["auth"]);

            var resultado = new { msg = "", type = "" };

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };
            }
            else
            {
                resultado = new
                {
                    msg = "Conferência feita com sucesso!",
                    type = "success"
                };
            }

            //return RedirectToAction("Details", new { DocEntry, BPL_IDAssignedToInvoice });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if ((((Login)Session["auth"]).Autorizacoes.CriarPedCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.CriarPedCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<ListBP> bps = _bussinesPartner.List();
            List<Filiais> filiais = _filiaisService.List();
            List<Deposito> depositos = _depositoService.List(1);

            TempData["ItensEstoque"] = itens;
            TempData["ListBP"] = bps;
            TempData["filiais"] = filiais;
            TempData["depositos"] = depositos;

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            return View();
        }

        [HttpPost]
        public JsonResult Add(PedidoCompra Pedido)
        {
            var resultado = new { msg = "", type = "" };

            if (Pedido.Itens == null)
            {
                resultado = new
                {
                    msg = "O Pedido deve ter pelo menos um item!",
                    type = "error"
                };

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

            string msg = _pedidoCompraService.Create(Pedido, (Login)Session["auth"]);

            if (msg != null)
            {
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };
            }
            else
            {
                resultado = new
                {
                    msg = "Pedido de Compra adicionado com sucesso!",
                    type = "success"
                };
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Fechar Status do Pedido
        public ActionResult FecharStatus(int DocEntry)
        {
            string msg = _pedidoCompraService.FecharStatus(DocEntry);

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Status do Documento Alterado com sucesso!";
            }

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Fechar Status Linhas
        public ActionResult FecharStatusLinha(int id, int line, int BPL_IDAssignedToInvoice)
        {
            string msg = _pedidoCompraService.FecharStatusLinha(id, line);

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Status da Linha Fechado com sucesso!";
            }

            return RedirectToAction("Details", new { DocEntry = id, BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice });
        }
        #endregion

        #region Editar Linhas
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarLinha(PedidoCompraLinhas pedidoCompraLinha, int paramType, int BPL_IDAssignedToInvoice)
        {
            if (pedidoCompraLinha.ShipDate < DateTime.Today)
            {
                TempData["Error"] = "A Data necessária não pode ser menor do que o dia atual e não pode ser vazia!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Details), new { DocEntry = pedidoCompraLinha.DocEntry, BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice });
            }
            if (pedidoCompraLinha.Quantity <= 0)
            {
                TempData["Error"] = "A Quantidade tem que ser maior do que 0";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Details), new { DocEntry = pedidoCompraLinha.DocEntry, BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice });
            }
            if (pedidoCompraLinha.PriceBefDi <= 0)
            {
                TempData["Error"] = "A o Preço tem que ser maior do que 0";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Details), new { DocEntry = pedidoCompraLinha.DocEntry, BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice });
            }

            AddLinha type;
            if (paramType == 2)
            {
                type = AddLinha.Add;
            }
            else
            {
                type = AddLinha.Update;
            }

            string msg = _pedidoCompraService.AlterarLinha(pedidoCompraLinha, type);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Documento Alterado com sucesso!";
            }

            return RedirectToAction("Details", new { DocEntry = pedidoCompraLinha.DocEntry, BPL_IDAssignedToInvoice = BPL_IDAssignedToInvoice });
        }
        #endregion

        #region Imprimir Pedido de Compra
        public ActionResult Imprimir(int DocEntry)
        {
            string path = _configuracoesService.GetDiretorioModeloImpressao("PC");

            if (!System.IO.File.Exists(path))
            {
                TempData["Error"] = "Arquivo não existente! Configure o Modelo de Impressão novamente!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }

            if (path != null)
            {
                string file = _impressaoService.Imprimir(DocEntry, path, TipoDocumento.PedidoCompra);
                string content_type = "application/pdf";
                string nameFile = $"PEDIDO_COMPRA_{DocEntry}.pdf";


                return File(file, content_type, nameFile);
            }
            else
            {
                TempData["Error"] = "Modelo Crystal Reports não configurado, acesse as configurações de documentos.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }
        }
        #endregion

        #region Editar Anexo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarAnexo(HttpPostedFileBase file, int DocEntry, int Filial, int idAnexos)
        {
            string msg = null;
            string fileName = Path.GetFileName(file.FileName);
            string ext = Path.GetExtension(file.FileName).Substring(1);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Files/Anexos"), fileName);
            file.SaveAs(path);

            List<Arquivo> Anexos = new List<Arquivo>();
            Anexos.Add(new Arquivo() { Nome = name, Extensao = ext, Diretorio = path });

            if (idAnexos == 0)
            {
                 msg = _anexosService.InserirAnexos(Anexos, DocEntry, Models.Enums.TipoDocumento.PedidoCompra);
            }
            else
            {
                 msg = _anexosService.EditarAdicionandoAnexos(Anexos, DocEntry, idAnexos, Models.Enums.TipoDocumento.PedidoCompra);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Anexo adicionado com sucesso!";
            }

            return RedirectToAction(nameof(Details), new { DocEntry = DocEntry, BPL_IDAssignedToInvoice = Filial });
        }
        #endregion
    }

}