﻿using PowerOne.Services.Compras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PowerOne.Services;
using System.Web.Mvc;
using PowerOne.Models;
using PowerOne.Models.Compras;
using PowerOne.Models.Compras.Enums;
using SAPbouiCOM;
using PowerOne.Models.Enums;
using System.IO;
using PowerOne.Util;

namespace PowerOne.Controllers.Compras
{
    [Authorize]
    public class SolicitacaoComprasController : Controller
    {

        #region Serviços (Consultas ao Banco de Dados)
        SolicitacaoCompraService _solicitacaoCompraService = new SolicitacaoCompraService();
        ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();
        FiliaisService _filiaisService = new FiliaisService();
        DepositoService _depositoService = new DepositoService();
        CentroCustoService _centroCustoService = new CentroCustoService();
        ImpressaoService _impressaoService = new ImpressaoService();
        ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        AnexosService _anexosService = new AnexosService();
        #endregion

        #region Exibir Solicitação de Compras
        public ActionResult Index(string DataInicio, string DataFim, string Status)
        {
            if((((Login)Session["auth"]).Autorizacoes.SolCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.SolCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado","Home");
            }

            if (String.IsNullOrEmpty(DataInicio) && String.IsNullOrEmpty(DataFim))
            {
                if (Status == null)
                {
                    Status = "T";
                }
                List<SolicitacaoCompra> pedidos = _solicitacaoCompraService.SearchList((Login)Session["auth"], null, null, Status);
                return View(pedidos);
            }
            else
            {
                List<SolicitacaoCompra> solicitacaos = _solicitacaoCompraService.SearchList((Login)Session["auth"], DataInicio, DataFim, Status);

                return View(solicitacaos);
            }
        }
        #endregion

        #region Detalhes da solicitação de compra
        public ActionResult Details(int DocEntry, int Filial)
        {
            if ((((Login)Session["auth"]).Autorizacoes.SolCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.SolCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<Deposito> depositos = _depositoService.List(Filial);
            TempData["ItensEstoque"] = itens;
            TempData["depositos"] = depositos;

            SolicitacaoCompra solicitacao = _solicitacaoCompraService.GetByKey(DocEntry);
            solicitacao.Filial = Filial;
            solicitacao.ListaAnexos = _anexosService.GetAnexo(DocEntry,TipoDocumento.SolicitacaoCompra);

            return View(solicitacao);

        }
        #endregion

        #region Editar Linhas de Solicitação
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarLinha(SolicitacaoCompraLinha solicitacaoCompraLinha, int paramType, int Filial)
        {
            if (solicitacaoCompraLinha.ReqDate < DateTime.Today)
            {
                TempData["Error"] = "A Data necessária não pode ser menor do que o dia atual e não pode ser vazia!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Details), new { DocEntry = solicitacaoCompraLinha.DocEntry, Filial = Filial });
            }
            if (solicitacaoCompraLinha.Quantidade <= 0)
            {
                TempData["Error"] = "A Quantidade tem que ser maior do que 0";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Details), new { DocEntry = solicitacaoCompraLinha.DocEntry, Filial = Filial });
            }
            //if (solicitacaoCompraLinha.PriceBefDi <= 0)
            //{
            //    TempData["Error"] = "A o Preço tem que ser maior do que 0";
            //    return RedirectToAction(nameof(Details), new { DocEntry = solicitacaoCompraLinha.DocEntry, Filial = Filial });
            //}

            AddLinha type;
            if (paramType == 2)
            {
                type = AddLinha.Add;
            }
            else
            {
                type = AddLinha.Update;
            }

            string msg = _solicitacaoCompraService.AlterarLinha(solicitacaoCompraLinha, type);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Documento Alterado com sucesso!";
            }

            return RedirectToAction("Details", new { DocEntry = solicitacaoCompraLinha.DocEntry, Filial = Filial });
        }
        #endregion

        #region create
        public ActionResult Create()
        {
            if ((((Login)Session["auth"]).Autorizacoes.CriarSolCompra == "N") && (((Login)Session["auth"]).Autorizacoes.Grupo.CriarSolCompra == "N"))
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            int[] dimensoesCtCusto = _configuracoesService.PegarConfiguracoesCentroCusto();

            List<ItemEstoque> itens = _itemEstoqueService.List();
            List<Filiais> filiais = _filiaisService.List();
            List<Deposito> depositos = _depositoService.List(1);

            TempData["ItensEstoque"] = itens;
            TempData["filiais"] = filiais;
            TempData["depositos"] = depositos;

            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoService.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoService.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoService.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoService.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoService.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            return View();
        }

        [HttpPost]

        public JsonResult Add(SolicitacaoCompra solicitacaoCompra)
        {
            var resultado = new { msg = "", type = "" };

            if (solicitacaoCompra.Itens == null)
            {
                resultado = new
                {
                    msg = "A solicitação deve ter pelo menos um item!",
                    type = "error"
                };

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

            string msg = _solicitacaoCompraService.Create(solicitacaoCompra, (Login)Session["auth"]);

            if (msg != null)
            {
                resultado = new
                {
                    msg = msg,
                    type = "error"
                };
            }
            else
            {
                resultado = new
                {
                    msg = "Solicitação de Compra adicionada com sucesso!",
                    type = "success"
                };
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Fechar Status Linhas
        public ActionResult FecharStatusLinha(int id, int line, int Filial)
        {
            string msg = _solicitacaoCompraService.FecharStatusLinha(id, line);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Status da Linha Fechado com sucesso!";
            }

            return RedirectToAction("Details", new { DocEntry = id, Filial =  Filial });
        }
        #endregion

        #region Fechar Status da Solicitação
        public ActionResult FecharStatus(int DocEntry)
        {
            string msg = _solicitacaoCompraService.FecharStatus(DocEntry);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Status do Documento Fechado com sucesso!";
            }

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Imprimir Solicitacao de Compras
        public ActionResult Imprimir(int DocEntry)
        {
            string path = _configuracoesService.GetDiretorioModeloImpressao("SC");

            if (!System.IO.File.Exists(path))
            {
                TempData["Error"] = "Arquivo não existente! Configure o Modelo de Impressão novamente!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }

            if (path != null)
            {
                string file = _impressaoService.Imprimir(DocEntry, path, TipoDocumento.SolicitacaoCompra);
                string content_type = "application/pdf";
                string nameFile = $"SOLICITACAO_COMPRA_{DocEntry}.pdf";


                return File(file, content_type, nameFile);
            }
            else
            {
                TempData["Error"] = "Modelo Crystal Reports não configurado, acesse as configurações de documentos.";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                return RedirectToAction(nameof(Index));
            }
        }
        #endregion

        #region Editar Anexo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarAnexo(HttpPostedFileBase file, int DocEntry, int Filial, int idAnexos)
        {
            string msg = null;

            string fileName = Path.GetFileName(file.FileName);
            string ext = Path.GetExtension(file.FileName).Substring(1);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Files/Anexos"), fileName);
            file.SaveAs(path);

            List<Arquivo> Anexos = new List<Arquivo>();
            Anexos.Add(new Arquivo() { Nome = name, Extensao = ext, Diretorio = path });

            if (idAnexos == 0)
            {
                msg = _anexosService.InserirAnexos(Anexos, DocEntry, Models.Enums.TipoDocumento.SolicitacaoCompra);
            }
            else
            {
                msg = _anexosService.EditarAdicionandoAnexos(Anexos, DocEntry, idAnexos, Models.Enums.TipoDocumento.SolicitacaoCompra);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Anexo adicionado com sucesso!";
            }

            return RedirectToAction(nameof(Details), new { DocEntry = DocEntry, Filial = Filial });
        }
        #endregion
    }
}