﻿using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class UploadArquivoController : Controller
    {
        #region Upload de Arquivo com Action Result
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase file)
        {
            string fileName = Path.GetFileName(file.FileName);
            string ext = Path.GetExtension(file.FileName).Substring(1);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Files/Anexos"),fileName);
            file.SaveAs(path);

            return Json(new Arquivo() { Nome = name, Extensao = ext, Diretorio = path },JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}