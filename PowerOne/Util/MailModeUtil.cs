﻿using PowerOne.Models.AssistenteCobranca;
using PowerOne.Models.AssistenteCobranca.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Util
{
    public class MailModelUtil
    {
        public string BuildMessage(BillMessage billMessage, BuildTextMailType typeText)
        {
            string CardName = billMessage.CardName;
            string SerialNumber = billMessage.DocCode;
            string DocDate = billMessage.DocDate.ToString("dd/MM/yyyy");
            string DocDueDate = billMessage.DocDueDate.ToString("dd/MM/yyyy");
            string DocTotal = billMessage.DocTotal.ToString("F2");

            int DaysToDueDate = billMessage.DifferenceDueDateAndToday;

            string message = "";
            string[] faturedMessage = null;

            //Verificando se estamos construindo o assunto ou o corpo do email
            //Dividindo a messagem, separando as variáveis
            if (typeText == BuildTextMailType.MessageBody)
            {
                faturedMessage = billMessage.MailModel.Message.Split('%');
            }
            else if (typeText == BuildTextMailType.MessageSubject)
            {
                faturedMessage = billMessage.MailModel.Subject.Split('%');
            }


            //Separando as Variáveis
            for (int i = 0; i < faturedMessage.Length; i++)
            {
                switch (faturedMessage[i])
                {
                    case "CardName":
                        faturedMessage[i] = CardName;
                        break;
                    case "SerialNumber":
                        //Pegando o número de série do código
                        faturedMessage[i] = SerialNumber.Split('-')[0].Substring(3, SerialNumber.Split('-')[0].Length - 3);
                        break;
                    case "InstallmentNumber":
                        //Pegando o número da parcela do código
                        if (SerialNumber.Split('-').Length > 1)
                        {
                            faturedMessage[i] = SerialNumber.Split('-')[1].Substring(2, SerialNumber.Split('-')[1].Length - 2);
                        }
                        break;
                    case "DocDate":
                        faturedMessage[i] = DocDate;
                        break;
                    case "DocDueDate":
                        faturedMessage[i] = DocDueDate;
                        break;
                    case "DocTotal":
                        faturedMessage[i] = "R$" + DocTotal;
                        break;
                    case "DaysToDueDate":
                        TimeSpan daysDifference = billMessage.DocDueDate - DateTime.Now;
                        faturedMessage[i] = daysDifference.Days.ToString();
                        break;
                    case "InvoiceModel":
                        faturedMessage[i] = billMessage.InvoiceModel;
                        break;
                }
                message += faturedMessage[i];
            }

            return message;

        }
    }
}