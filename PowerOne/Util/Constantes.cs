﻿using System.Configuration;

namespace PowerOne.Util
{
    public class Constantes
    {
        public static string SAP_USUARIO = ConfigurationManager.AppSettings["UsuarioSap"].ToString();
        public static string SAP_SENHA = ConfigurationManager.AppSettings["SenhaSap"].ToString();
        public static string SAP_BASE = ConfigurationManager.AppSettings["SapBD"].ToString();
        public static string SenhaSQL = ConfigurationManager.AppSettings["SenhaBD"].ToString();
        public static string UsuarioSQL = ConfigurationManager.AppSettings["UsuarioBD"].ToString();
        public static string Servidor = ConfigurationManager.AppSettings["Servidor"].ToString();
        public static string VersaoBancoDados = ConfigurationManager.AppSettings["VersaoBD"].ToString();
        public static string ServidorLicenca = ConfigurationManager.AppSettings["ServidorLicenca"].ToString();


        //Diretório onde estará armazenado os arquivos PDF das Notas Fiscais de Saída
        public static string DiretorioNotasFiscais = ConfigurationManager.AppSettings["DiretorioNotasFiscais"].ToString();
        public static string DiretorioBoletos = ConfigurationManager.AppSettings["DiretorioBoletos"].ToString();

        //Dados para o envio de emails
        public static string DiretorioLogoLayoutEmail = ConfigurationManager.AppSettings["DiretorioLogoLayoutEmail"].ToString();
    }
}