﻿using PowerOne.Services;
using PowerOne.Services.AssistenteCobranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Util
{
    public class Seeds
    {
        private TypeModelService _typeModelService = new TypeModelService();
        private LoginService _loginService = new LoginService();

        public void TypeMailSeeds()
        {
            if (!_typeModelService.VerifyIfExist("T0001"))
            {
                _typeModelService.Create("T0001", "Pós-Faturamento");
            }
            if (!_typeModelService.VerifyIfExist("T0002"))
            {
                _typeModelService.Create("T0002", "Alerta de Vencimento");
            }
            if (!_typeModelService.VerifyIfExist("T0003"))
            {
                _typeModelService.Create("T0003", "Cobrança");
            }
        }

        public void EmployeeSeeds()
        {
            _loginService.ToFillTheEmptyPassword();
        }
    }
}