﻿using PowerOne.Database;
using PowerOne.Database.AssistenteCobranca;
using PowerOne.Database.Compras;
using PowerOne.Database.Estoque;

namespace PowerOne.Util
{
    public class CreateTablesAndFields
    {
        #region Main
        public static void Main()
        {
            /*Assistente Cobranca*/
            AssistenteCobranca();

            /*Configurações*/
            ConfiguracoesEmail();

            /*Requerimento Estoque*/
            RequerimentoEstoque();

            /*Documento de Marketing*/
            DocumentosMarketing();
            DocumentoMarketingLinha();


            /*Configuracoes Documentos de Impressao*/
            ConfigImpressao();

            /*Configuracoes de Tema do Site*/
            ConfigTema();

            /*Centro de Custo*/
            CentroCusto();

            /*Autorizações*/
            Autorizacoes();

            /*Autorizações Grupo*/
            AutorizacoesGrupo();

            /*Log de Login*/
            LogLogin();
        }
        #endregion

        #region Log de Login
        public static void LogLogin()
        {
            LogLoginDB logLoginDB = new LogLoginDB();
            //logLoginDB.CreateTable();
            logLoginDB.AddFields();
        }
        #endregion

        #region Autorizacoes de Grupo
        public static void AutorizacoesGrupo()
        {
            AutorizacoesGrupoDB autorizacoesGrupoDB = new AutorizacoesGrupoDB();
            autorizacoesGrupoDB.CreateTable();
            autorizacoesGrupoDB.AddFields();
            autorizacoesGrupoDB.Seeds();
        }
        #endregion

        #region Autorizações
        public static void Autorizacoes()
        {
            AutorizacoesDB autorizacoesDB = new AutorizacoesDB();
            autorizacoesDB.CreateTable();
            autorizacoesDB.AddFields();
            autorizacoesDB.Seeds();
        }
        #endregion

        #region Centros de Custo
        public static void CentroCusto()
        {
            NiveisCentrosCustoDB niveisCentrosCustoDB = new NiveisCentrosCustoDB();
            niveisCentrosCustoDB.AddFields();
        }
        #endregion

        #region Configuracoes de Temas
        public static void ConfigTema()
        {
            ConfiguracoesTemaDB configuracoesTemaDB = new ConfiguracoesTemaDB();
            configuracoesTemaDB.CreateTable();
            configuracoesTemaDB.AddFields();
        }
        #endregion

        #region Configuracoes Impressao Documentos
        public static void ConfigImpressao()
        {
            ConfiguracoesImpressaoDB configuracoesImpressaoDB = new ConfiguracoesImpressaoDB();
            configuracoesImpressaoDB.CreateTable();
            configuracoesImpressaoDB.AddFields();
        }
        #endregion

        #region Documentos de Marketing
        public static void DocumentosMarketing()
        {
            DocumentoMarketingDB documentoMarketingDB = new DocumentoMarketingDB();
            documentoMarketingDB.AddFields();
        }

        public static void DocumentoMarketingLinha()
        {
            DocumentoMarketingLinhaDB documentoMarketingLinhaDB = new DocumentoMarketingLinhaDB();
            documentoMarketingLinhaDB.AddFields();
        }

        #endregion

        #region Requerimento Estoque
        public static void RequerimentoEstoque()
        {
            RequerimentoEstoqueDB requerimentoEstoqueDB = new RequerimentoEstoqueDB();
            RequerimentoEstoqueLinhasDB requerimentoEstoqueLinhasDB = new RequerimentoEstoqueLinhasDB();

            requerimentoEstoqueDB.CreateTable();
            requerimentoEstoqueDB.AddField();

            requerimentoEstoqueLinhasDB.CreateTable();
            requerimentoEstoqueLinhasDB.AddField();
        }
        #endregion

        #region Configuracoes 
        public static void ConfiguracoesEmail()
        {
            ConfiguracoesEmailDB configuracoesEmailDB = new ConfiguracoesEmailDB();

            configuracoesEmailDB.CreateTable();
            configuracoesEmailDB.AddFields();
        }
        #endregion

        #region AssistenteCobranca
        public static void AssistenteCobranca()
        {
            Seeds seed = new Seeds();

            EmployeeDB colaboradorDB = new EmployeeDB();
            colaboradorDB.AddFields();

            BusinessPartnerDB businessPartnerDB = new BusinessPartnerDB();
            businessPartnerDB.AddFields();

            TypeMailDB typeMailDB = new TypeMailDB();
            typeMailDB.CreateTable();

            BPGroupDB bpGroupDB = new BPGroupDB();
            bpGroupDB.CreateTable();
            bpGroupDB.AddFields();

            MailModelDB mailModelDB = new MailModelDB();
            mailModelDB.CreateTable();
            mailModelDB.AddField();

            SendingScheduleDB sendingScheduleDB = new SendingScheduleDB();
            sendingScheduleDB.CreateTable();
            sendingScheduleDB.AddField();

            BPGroupTypeMailModelDB bPGroupTypeMailModelDB = new BPGroupTypeMailModelDB();
            bPGroupTypeMailModelDB.CreateTable();
            bPGroupTypeMailModelDB.AddField();

            LogEmailDB logEmailDB = new LogEmailDB();
            logEmailDB.CreateTable();
            logEmailDB.AddField();

            ContactEmployeeDB contactEmployeeDB = new ContactEmployeeDB();
            contactEmployeeDB.AddFields();

            /**********************
             * Criando Seeds
             **********************/
            seed.TypeMailSeeds();
            seed.EmployeeSeeds();
        }
        #endregion
    }
}