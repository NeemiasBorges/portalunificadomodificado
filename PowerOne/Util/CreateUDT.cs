﻿using PowerOne.Database;
using System;

namespace PowerOne.Util
{
    public class CreateUDT
    {

        #region AddUDT
        public bool AddUDT(string TableName, string TableDescription, SAPbobsCOM.BoUTBTableType TableType)
        {
            SAPbobsCOM.Company oConnection = Conexao.Company;
            SAPbobsCOM.UserTablesMD oUserTable = (SAPbobsCOM.UserTablesMD)oConnection.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);

            bool bCriou = false;
            int iRetCode = -1;

            try
            {
                oUserTable.TableName = TableName;
                oUserTable.TableDescription = TableDescription;
                oUserTable.TableType = TableType;

                iRetCode = oUserTable.Add();

                //Verifica se a tabela já existe
                if (iRetCode == -2035)
                {
                    bCriou = true;
                }
                else if (iRetCode == 0)
                {
                    bCriou = true;
                }
                else if (iRetCode != 0)
                {
                    bCriou = false;
                    string msg = oConnection.GetLastErrorDescription();
                    Log.Gravar("Table: " + TableName + "-" +  msg, Log.TipoLog.Erro);
                }
            }
            catch (Exception e)
            {
                bCriou = false;
                Log.Gravar("Table: " + TableName + "-" +  e.Message, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTable);
            oUserTable = null;

            return bCriou;
        }
        #endregion
    }
}