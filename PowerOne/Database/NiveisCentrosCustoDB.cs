﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class NiveisCentrosCustoDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDF _createUDF = new CreateUDF();

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Ativado", "ODIM"))
            {
                List<ValoresValidos> valoresValidos = new List<ValoresValidos>();
                valoresValidos.Add(new ValoresValidos { sValor = "Y", sDescricao = "Ativado" });
                valoresValidos.Add(new ValoresValidos { sValor = "N", sDescricao = "Desativado" });
                bCriou = _createUDF.AddUDF("P1_Ativado", "ODIM", "Exibir nos Documentos", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "Y", null, null);
            }
        }
    }
}