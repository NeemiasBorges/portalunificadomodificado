﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database
{
    public class ConfiguracoesEmailDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_EMAILCONF".ToUpper()))
            {
                _createUDT.AddUDT("P1_EMAILCONF", "P1 - Configurações de Email", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Email", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_Email", "P1_EMAILCONF", "Endereço de Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, null, null, null);
            }
            if (!_consultaGeral.VerifyField("P1_Senha", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_Senha", "P1_EMAILCONF", "Senha", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, null, null, null);
            }
            if (!_consultaGeral.VerifyField("P1_Servidor", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_Servidor", "P1_EMAILCONF", "Servidor SMTP", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, null, null, null);
            }
            if (!_consultaGeral.VerifyField("P1_Porta", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_Porta", "P1_EMAILCONF", "Porta", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10, null, null, null);
            }
            if (!_consultaGeral.VerifyField("P1_DrBoleto", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_DrBoleto", "P1_EMAILCONF", "Dir. Boleto", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, null, null, null);
            }
            if (!_consultaGeral.VerifyField("P1_DrNtFisc", "@P1_EMAILCONF"))
            {
                bCriou = _createUDF.AddUDF("P1_DrNtFisc", "P1_EMAILCONF", "Dir. N. Fiscal", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, null, null, null);
            }
        }
    }
}