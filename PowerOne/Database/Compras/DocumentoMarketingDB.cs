﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database.Compras
{
    public class DocumentoMarketingDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        public void AddFields()
        {
            if (!_consultaGeral.VerifyField("P1_OWNER", "OPOR"))
            {
                _createUDF.AddUDF("P1_OWNER", "OPOR", "Autor do Documento", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, "", null, null);
            }

            if (!_consultaGeral.VerifyField("P1_TYPE", "OPOR"))
            {
                List<ValoresValidos> oValValidos = new List<ValoresValidos>();
                oValValidos.Add(new ValoresValidos { sValor = "P", sDescricao = "Pedido" });
                oValValidos.Add(new ValoresValidos { sValor = "R", sDescricao = "Reembolso" });
                _createUDF.AddUDF("P1_TYPE", "OPOR", "Tipo do documento", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "P", null, oValValidos);
            }
        }
    }
}