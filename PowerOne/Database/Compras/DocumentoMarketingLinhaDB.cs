﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database.Compras
{
    public class DocumentoMarketingLinhaDB
    {
        #region Serviços
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        #endregion

        #region AddFilds
        public void AddFields()
        {
            if (!_consultaGeral.VerifyField("P1_CONFPEDIDO", "POR1"))
            {
                _createUDF.AddUDF("P1_CONFPEDIDO", "POR1", "Conferência de Pedido", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 15, "", null, null);
            }

            if (!_consultaGeral.VerifyField("P1_ObsConf", "POR1"))
            {
                _createUDF.AddUDF("P1_ObsConf", "POR1", "Obs Conferência", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null, null);
            }

            if (!_consultaGeral.VerifyField("P1_Conferente", "POR1"))
            {
                _createUDF.AddUDF("P1_Conferente", "POR1", "Conferente", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 75, "", null, null);
            }

            if (!_consultaGeral.VerifyField("P1_CONFREC", "POR1"))
            {
                List<ValoresValidos> oValValidos = new List<ValoresValidos>();
                oValValidos.Add(new ValoresValidos { sValor = "N", sDescricao = "Não" });
                oValValidos.Add(new ValoresValidos { sValor = "Y", sDescricao = "Sim" });
                _createUDF.AddUDF("P1_CONFREC", "POR1", "Status Conferimento", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValValidos);
            }

            if (!_consultaGeral.VerifyField("P1_DataConf", "POR1"))
            {
                _createUDF.AddUDF("P1_DataConf", "POR1", "Data Conferência", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null, null);
            }
        }
        #endregion
    }
}