﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database.Estoque
{
    public class RequerimentoEstoqueLinhasDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_IGE1".ToUpper()))
            {
                _createUDT.AddUDT("P1_IGE1", "P1 - Req. Saida LINHAS ", SAPbobsCOM.BoUTBTableType.bott_DocumentLines);
            }
        }
        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_ID", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_ID", "P1_IGE1", "ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_LinStatus", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_LinStatus", "P1_IGE1", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_xPed", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_xPed", "P1_IGE1", "Pedido", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_ItemCode", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_ItemCode", "P1_IGE1", "Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_PN", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_PN", "P1_IGE1", "CODIGO PN", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_DTEFET", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_DTEFET", "P1_IGE1", "DATA EFETIVAÇÃO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Quantity", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_Quantity", "P1_IGE1", "Quantidade", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 3, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Comments", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_Comments", "P1_IGE1", "Observações", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null,null);
            }
            if (!_consultaGeral.VerifyField("P1_WhsCode", "@P1_IGE1"))
            {
                _createUDF.AddUDF("P1_WhsCode", "P1_IGE1", "Depósito", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null, null);
            }
        }
    }
}