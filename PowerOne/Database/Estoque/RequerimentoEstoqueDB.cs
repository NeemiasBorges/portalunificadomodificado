﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database.Estoque
{
    public class RequerimentoEstoqueDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_OIGE".ToUpper()))
            {
                _createUDT.AddUDT("P1_OIGE", "P1 - Req.Saida de Mercadoria", SAPbobsCOM.BoUTBTableType.bott_Document);
            }
        }

        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_STATUS", "@P1_OIGE"))
            {
                _createUDF.AddUDF("P1_STATUS", "P1_OIGE", "STATUS", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, "", null,null);
            }

            if (!_consultaGeral.VerifyField("P1_DTLANC", "@P1_OIGE"))
            {
                _createUDF.AddUDF("P1_DTLANC", "P1_OIGE", "DATA LANÇAMENTO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null,null);
            }

            if (!_consultaGeral.VerifyField("P1_DTEFET", "@P1_OIGE"))
            {
                _createUDF.AddUDF("P1_DTEFET", "P1_OIGE", "DATA EFETIVAÇÃO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null,null);
            }

            if (!_consultaGeral.VerifyField("P1_Owner", "@P1_OIGE"))
            {
                _createUDF.AddUDF("P1_Owner", "P1_OIGE", "Solicitante", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null,null);
            }

            if (!_consultaGeral.VerifyField("P1_Comments", "@P1_OIGE"))
            {
                _createUDF.AddUDF("P1_Comments", "P1_OIGE", "Observações", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null,null);
            }
        }
    }
}