﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database
{
    public class ConfiguracoesImpressaoDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_RPTDOC".ToUpper()))
            {
                _createUDT.AddUDT("P1_RPTDOC", "P1 - Modelo p/ Impressão", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Diretorio", "@P1_RPTDOC"))
            {
                bCriou = _createUDF.AddUDF("P1_Diretorio", "P1_RPTDOC", "Caminho do Arquivo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, null, null, null);
            }
        }
    }
}