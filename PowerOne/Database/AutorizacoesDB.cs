﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class AutorizacoesDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_Access".ToUpper()))
            {
                _createUDT.AddUDT("P1_Access", "P1 - Autorizações", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();

            oValoresValidos.Add(new ValoresValidos { sValor = "N", sDescricao = "Sem Autorização" });
            oValoresValidos.Add(new ValoresValidos { sValor = "R", sDescricao = "Acesso Restrito" });
            oValoresValidos.Add(new ValoresValidos { sValor = "T", sDescricao = "Acesso Total" });

            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_EMPId", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_EMPId", "@P1_Access", "EmpId", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, "", null, null);
            }
            if (!_consultaGeral.VerifyField("P1_Type", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_Type", "@P1_Access", "Tipo de Usuário", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, "User", null, null);
            }
            if (!_consultaGeral.VerifyField("P1_ConsSol", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_ConsSol", "@P1_Access", "Consulta Solicitação", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewSol", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewSol", "@P1_Access", "Solicitação de Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_PedComp", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_PedComp", "@P1_Access", "Pedido de Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewPedC", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewPedC", "@P1_Access", "Criar Pedido Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_ReqS", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_ReqS", "@P1_Access", "Consulta Req Saida", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewReqS", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewReqS", "@P1_Access", "Nova Requisição", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewReem", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewReem", "@P1_Access", "Novo Reembolso", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_Reemb", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_Reemb", "@P1_Access", "Novo Reembolso", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewMailM", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewMailM", "@P1_Access", "Novo Modelo Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_MailMod", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_MailMod", "@P1_Access", "Exibir Modelo Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_AstEnvio", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_AstEnvio", "@P1_Access", "Assistente Envio", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_Client", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_Client", "@P1_Access", "Listar Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewGPCnt", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_NewGPCnt", "@P1_Access", "Criar Grupo Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_GPCnt", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_GPCnt", "@P1_Access", "Exibir Grupos Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_LogEmail", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_LogEmail", "@P1_Access", "Registro Envios Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_RTCobran", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_RTCobran", "@P1_Access", "Relatório Cobranças", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_Grupo", "@P1_Access"))
            {
                bCriou = _createUDF.AddUDF("P1_Grupo", "@P1_Access", "Grupo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "", "P1_AccGrp", null);
            }
        }

        public void Seeds()
        {
            _autorizacoesService.CriarAutorizacoes();
        }
    }
}