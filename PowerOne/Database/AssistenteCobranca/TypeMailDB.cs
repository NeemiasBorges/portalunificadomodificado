﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class TypeMailDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_TYPEMAIL".ToUpper()))
            {
                _createUDT.AddUDT("P1_TYPEMAIL", "P1 - Tipo de Modelo de Email", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }
    }
}