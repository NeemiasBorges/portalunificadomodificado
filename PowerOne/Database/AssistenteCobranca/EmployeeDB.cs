﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class EmployeeDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDF _createUDF = new CreateUDF();

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Password", "OHEM"))
            {
                bCriou = _createUDF.AddUDF("P1_Password", "OHEM", "Password", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "NOVO", null, null);
            }
        }

        
    }
}