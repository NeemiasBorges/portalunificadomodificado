﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class SendingScheduleDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_SCHEDULE".ToUpper()))
            {
                _createUDT.AddUDT("P1_SCHEDULE", "P1 - Agenda envio de emails", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_CodeType", "@P1_SCHEDULE"))
            {
                _createUDF.AddUDF("P1_CodeType", "P1_SCHEDULE", "Código do Tipo de Modelo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_TYPEMAIL", null);
            }
            if (!_consultaGeral.VerifyField("P1_Days", "@P1_SCHEDULE"))
            {
                _createUDF.AddUDF("P1_Days", "P1_SCHEDULE", "Dias antes/depois do vencimento para envio do email", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10, null, null,null);
            }
        }

    }
}