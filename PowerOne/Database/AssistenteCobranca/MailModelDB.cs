﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class MailModelDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_MAILMODEL".ToUpper()))
            {
                _createUDT.AddUDT("P1_MAILMODEL", "P1 - Modelo de Email", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_CodeType", "@P1_MAILMODEL"))
            {
                _createUDF.AddUDF("P1_CodeType", "P1_MAILMODEL", "Código do Tipo de Modelo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_TYPEMAIL", null);
            }
            if (!_consultaGeral.VerifyField("P1_Subject", "@P1_MAILMODEL"))
            {
                _createUDF.AddUDF("P1_Subject", "P1_MAILMODEL", "Assunto do Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None,254, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Message", "@P1_MAILMODEL"))
            {
                _createUDF.AddUDF("P1_Message", "P1_MAILMODEL", "Mensagem do Email", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10000, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_PayMethod", "@P1_MAILMODEL"))
            {
                _createUDF.AddUDF("P1_PayMethod", "P1_MAILMODEL", "Forma de Pagamento", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, null, null,null);
            }
        }
    }
}