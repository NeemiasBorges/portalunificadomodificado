﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class BPGroupDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_BPGROUP".ToUpper()))
            {
                _createUDT.AddUDT("P1_BPGROUP", "P1 - Grupo de PN", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Ativado", "@P1_BPGROUP"))
            {
                bCriou = _createUDF.AddUDF("P1_Ativado", "P1_BPGROUP", "Ativado", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "Y", null, null);
            }
        }
    }
}