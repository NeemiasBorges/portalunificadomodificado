﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class BPGroupTypeMailModelDB
    {
        #region propriedades
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        #endregion

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_GRTPMAIL".ToUpper()))
            {
                _createUDT.AddUDT("P1_GRTPMAIL", "P1 - Modelos do Grupo de PN", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_GroupCode", "@P1_GRTPMAIL"))
            {
                _createUDF.AddUDF("P1_GroupCode", "P1_GRTPMAIL", "Código do Grupo de Parceiros de Negócio", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_BPGROUP",null);
            }
            if (!_consultaGeral.VerifyField("P1_CodeType", "@P1_GRTPMAIL"))
            {
                _createUDF.AddUDF("P1_CodeType", "P1_GRTPMAIL", "Código do Tipo de Modelo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_TYPEMAIL", null);
            }
            if (!_consultaGeral.VerifyField("P1_CodeMail", "@P1_GRTPMAIL"))
            {
                _createUDF.AddUDF("P1_CodeMail", "P1_GRTPMAIL", "Código do Modelo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_MAILMODEL", null);
            }

            if (!_consultaGeral.VerifyField("P1_PayMethod", "@P1_GRTPMAIL"))
            {
                _createUDF.AddUDF("P1_PayMethod", "P1_GRTPMAIL", "Forma de Pagamento", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, null, null,null);
            }
        }
    }
}