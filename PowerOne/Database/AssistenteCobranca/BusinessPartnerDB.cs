﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class BusinessPartnerDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDF _createUDF = new CreateUDF();

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_BPGroup", "OCRD"))
            {
                bCriou = _createUDF.AddUDF("P1_BPGroup", "OCRD", "Grupo de clientes p/ Emails", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_BPGROUP", null);
            }

            if (!_consultaGeral.VerifyField("P1_EmailList", "OCRD"))
            {
                bCriou = _createUDF.AddUDF("P1_EmailList", "OCRD", "Lista de Emails para contato", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 500, null, null, null);
            }
        }
    }
}