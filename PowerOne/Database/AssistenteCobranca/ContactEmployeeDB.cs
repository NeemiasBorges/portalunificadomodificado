﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class ContactEmployeeDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDF _createUDF = new CreateUDF();

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_UseForMail", "OCPR"))
            {
                bCriou = _createUDF.AddUDF("P1_UseForMail", "OCPR", "Utilizar p/ Envio de Emails", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null,null);
            }
        }
    }
}