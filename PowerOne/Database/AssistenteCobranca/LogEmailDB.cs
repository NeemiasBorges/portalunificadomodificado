﻿using PowerOne.Services;
using PowerOne.Util;

namespace PowerOne.Database.AssistenteCobranca
{
    public class LogEmailDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_LOGEMAIL".ToUpper()))
            {
                _createUDT.AddUDT("P1_LOGEMAIL", "P1 - Log Emails enviados", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddField()
        {
            if (!_consultaGeral.VerifyField("P1_CardCode", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_CardCode", "P1_LOGEMAIL", "Código do Parceiro de Negócio", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 17, null, "P1_OCRD", null);
            }
            if (!_consultaGeral.VerifyField("P1_CRCode", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_CRCode", "P1_LOGEMAIL", "Código do título", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Date", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_Date", "P1_LOGEMAIL", "Data de envio do email", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_DueDate", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_DueDate", "P1_LOGEMAIL", "Data de vencimento do título", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_TpMailCode", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_TpMailCode", "P1_LOGEMAIL", "Típo de Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_TYPEMAIL", null);
            }
            if (!_consultaGeral.VerifyField("P1_ModelMail", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_ModelMail", "P1_LOGEMAIL", "Modelo de Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_MailModel", null);
            }
            if (!_consultaGeral.VerifyField("P1_CardCode", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_CardCode", "P1_LOGEMAIL", "Código do Cliente", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, "P1_MailModel", null);
            }
            if (!_consultaGeral.VerifyField("P1_EmailTxt", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_EmailTxt", "P1_LOGEMAIL", "Mensagem do Email", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10000, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_CrTotal", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_CrTotal", "P1_LOGEMAIL", "Valor do Contas a Receber", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 0, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Subject", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_Subject", "P1_LOGEMAIL", "Assunto do Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_EmlAdress", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_EmlAdress", "P1_LOGEMAIL", "Endereço do Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_ListEmails", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_ListEmails", "P1_LOGEMAIL", "Lista de Endereços do Emails", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, null, null,null);
            }
            if (!_consultaGeral.VerifyField("P1_Status", "@P1_LOGEMAIL"))
            {
                _createUDF.AddUDF("P1_Status", "P1_LOGEMAIL", "Endereço do Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, null, null,null);
            }

        }
    }
}