﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database.Compras
{
    public class DocumentoMarketingDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        public void AddFields()
        {
            if (!_consultaGeral.VerifyField("G2_CONFREC", "OINV"))
            {
                List<ValoresValidos> oValValidos = new List<ValoresValidos>();
                oValValidos.Add(new ValoresValidos { sValor = "N", sDescricao = "Não" });
                oValValidos.Add(new ValoresValidos { sValor = "Y", sDescricao = "Sim" });
                _createUDF.AddUDF("G2_CONFREC", "OINV", "Recebimento Conferido", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N",null, oValValidos);
            }

            if (!_consultaGeral.VerifyField("G2_DataConf", "OINV"))
            {
                _createUDF.AddUDF("G2_DataConf", "OINV", "Data da Conferencia", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null, null);
            }

            if (!_consultaGeral.VerifyField("G2_ObsConf", "OINV"))
            {
                _createUDF.AddUDF("G2_ObsConf", "OINV", "Observação da Conferencia", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null, null);
            }

            if (!_consultaGeral.VerifyField("G2_Conferente", "OINV"))
            {
                _createUDF.AddUDF("G2_Conferente", "OINV", "Nome Conferente", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null, null);
            }
        }
    }
}