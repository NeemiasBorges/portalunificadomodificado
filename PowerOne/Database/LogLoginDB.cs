﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class LogLoginDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();

        //public void CreateTable()
        //{
        //    if (!_consultaGeral.VerifyTable("EB_LogLogin".ToUpper()))
        //    {
        //        _createUDT.AddUDT("EB_LogLogin", "LogLogin", SAPbobsCOM.BoUTBTableType.bott_NoObject);
        //    }
        //}

        public void AddFields()
        {
            List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();

            oValoresValidos.Add(new ValoresValidos { sValor = "L", sDescricao = "Logado" });
            oValoresValidos.Add(new ValoresValidos { sValor = "D", sDescricao = "Deslogado" });

            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Status", "@OHEM"))
            {
                bCriou = _createUDF.AddUDF("P1_Status", "OHEM", "Status de Login", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "D", null, oValoresValidos);
            }
        }
    }
}