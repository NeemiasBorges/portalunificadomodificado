﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class ConfiguracoesCentroCustoDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void AddFields()
        {
            bool bCriou = false;

            List<ValoresValidos> valoresValidos = new List<ValoresValidos>();
            valoresValidos.Add(new ValoresValidos() { sValor = "Y", sDescricao = "Utilizar" });
            valoresValidos.Add(new ValoresValidos() { sValor = "N", sDescricao = "Não Utilizar" });
        }

    }
}