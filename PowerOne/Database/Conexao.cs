﻿using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database
{
    public class Conexao
    {
        private static SAPbobsCOM.Company oCompany;

        public static void Connect()
        {
            oCompany = new SAPbobsCOM.Company();
            oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(Constantes.VersaoBancoDados));
            oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Portuguese_Br;
            oCompany.Disconnect();
            oCompany.Server = Constantes.Servidor;
            oCompany.CompanyDB = Constantes.SAP_BASE;
            oCompany.UserName = Constantes.SAP_USUARIO;
            oCompany.Password = Constantes.SAP_SENHA;
            oCompany.LicenseServer = Constantes.ServidorLicenca;
            oCompany.DbUserName = Constantes.UsuarioSQL;
            oCompany.DbPassword = Constantes.SenhaSQL;
            oCompany.UseTrusted = false;
            oCompany.Connect();
        }

        public static SAPbobsCOM.Company Company 
        { 
            get { return oCompany; } 
        }

    }
}